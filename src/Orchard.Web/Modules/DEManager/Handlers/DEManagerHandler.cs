﻿using DEManager.Models;
using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DEManager.Handlers
{
    public class DEManagerHandler: ContentHandler 
    {
        public DEManagerHandler(IRepository<DEManagerPartRecord> repository)
        {
         Filters.Add(StorageFilter.For(repository));

         OnRemoved<DEManagerPart>((context, part) =>
         {
             repository.Delete(part.Record);
         });

        }
    } 
}