﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Records;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DEManager.Models
{
    public class DEManagerPartRecord : ContentPartRecord
    {
        public virtual string ExternalKey { get; set; }
        public virtual int ItemsPerPage { get; set; }
        public virtual string FieldsFilter { get; set; }

        public DEManagerPartRecord() { ItemsPerPage = 50; }
    }

    public class DEManagerPart : ContentPart<DEManagerPartRecord>
    {
        [Required]
        public string ExternalKey
        {
            get { return Retrieve(r => r.ExternalKey); }
            set { Store(r => r.ExternalKey, value); }
        }

        [Required]
        [DefaultValue(50)]
        public int ItemsPerPage
        {
            get { return Retrieve(r => r.ItemsPerPage); }
            set { Store(r => r.ItemsPerPage, value); }
        }

        public string FieldsFilter
        {
            get { return Retrieve(r => r.FieldsFilter); }
            set { Store(r => r.FieldsFilter, value); }
        }
    }
}