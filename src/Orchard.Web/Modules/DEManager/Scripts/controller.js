﻿/*
* Main Data Extension Manager JavaScript file
* Combines AMPScript SOAP API calls and JS to build a grid to edit a certain Data Extension
*
* Author: Patricio Sapir - 5/10/2013
*/

var apiUrl = 'https://fanatics-api.azurewebsites.net/api';

$(function () {
    $('#modalLoading').modal();

    if (getToken())
        initialize();
    else
    {
        var loginURL = apiUrl + "/auth/token";

        var data = "grant_type=password";
        data += "&token=" + oauthToken;
        data += "&userId=" + userID;

        $.post(loginURL, data)
            .done(function (response) {

                // set access token
                $.ajaxSetup({
                    headers: { 'Authorization': "Bearer " + response.access_token }
                });

                sessionStorage.setItem("apiToken", response.access_token);
                initialize();
            })
            .error(function (response) {

            })
            .always(function () {

            });
    }
    
    $("#frmAddEdit").validate(
         {
             highlight: function (label) {
                 $(label).closest('.control-group').addClass('error');
             },
             success: function (label) {
                 label
                   .text('OK!').addClass('valid')
                   .closest('.control-group').addClass('success');
             }
         });

    $('#btnSubmit').click(function () {
        if ($("#frmAddEdit").validate().form()) {

            var url = apiUrl + "/dataextension/update";

            var formData = {};
            formData.Row = "{";
            $.each($(".de-field"), function (index, item) {
                formData.Row += "\"" + item.id.replace('txt', '') + "\":\"" + item.value + "\"";
                if (index < $(".de-field").length - 1)
                    formData.Row += ",";
            });
            //formData.Row = JSON.stringify(formData.Row);
            formData.Row += "}";
            formData.ExternalKey = externalKey;

            $('#btnSubmit').attr('disabled', true);
            
            $.post(url, formData).done(function (data) {
                if (data.resultStatus == "OK") {
                    $("#pnlFormStatus").addClass('alert-success').removeClass('alert-error');
                    $('#lblFormTitle').text('Sucess!');
                    $('#lblFormMessage').text('Your information was succesfully submitted, refreshing grid...');

                    $("#pnlFormStatus").fadeIn(300).delay(3000).fadeOut(300).queue(function (next) {
                        $('#pnlAddEdit').modal('hide');
                        next();
                    });

                    rawData = null;
                    $('#grDataExtension').datagrid('reload');
                }
                else {
                    $("#pnlFormStatus").addClass('alert-error').removeClass('alert-success');
                    $('#lblFormTitle').text('Error!');
                    $('#lblFormMessage').text('An error has ocurred, please try again or contact the system administrator');
                    $("#pnlFormStatus").fadeIn(300).delay(3000).fadeOut(300);
                }
            })
                .error(function (response) {
                    $("#pnlFormStatus").addClass('alert-error').removeClass('alert-success');
                    $('#lblFormTitle').text('Error!');
                    $('#lblFormMessage').text('An error has ocurred, please try again or contact the system administrator');
                    $("#pnlFormStatus").fadeIn(300).delay(3000).fadeOut(300);
                })
                .always(function (response) {

                $('#btnSubmit').removeAttr('disabled');

            });
        }
    });

});

function getToken() {
    if (typeof (Storage) !== "undefined") {
        var apiToken = sessionStorage.getItem("apiToken");
        if (apiToken != null)
        {
            $.ajaxSetup({
                headers: { 'Authorization': "Bearer " + apiToken }
            });
            return true;
        }
    } 
   return false;
}
function initialize() {
    // Initialize bootstrap tooltips
    $('.ttip').tooltip();

    firstLoad = true;

    buildGrid();
}

function buildGrid() {
    var deURL = apiUrl + "/dataextension/getfields?externalkey=" + externalKey;
    var columns = [];
    var hasPK = null;
    $.get(deURL)
           .done(function (fields) {
               for (var i = 0; i < fields.length; i++) {
                   var item = {};
                   var field = fields[i];
                   switch (field.fieldType) {
                       case "EmailAddress":
                           item.inputType = "email";
                           break;
                       case "Number":
                           item.inputType = "number";
                           break;
                       default:
                           item.inputType = "text";
                           break;
                   }

                   item.property = field.name;
                   item.label = field.name;
                   item.ordinal = "'" + field.ordinal+"'";
                   item.isPK = field.isPrimaryKey;
                   item.maxLength = field.maxLength;

                   if (item.maxLength == 0)
                       item.maxLength = 8000;

                   if(field.isRequired)
                       item.required = 'required';
                   else
                       item.required = '';

                   item.sortable = true;

                   if (hasPK == null && field.isPrimaryKey)
                       hasPK = true;

                   columns.push(item);
               }

               if (hasPK == null)
                   hasPK = false;

               if (hasPK) {
                   var item = {
                       property: 'Actions',
                       label: 'Actions',
                       ordinal: '999',
                       sortable: false
                   };
               }

               columns.push(item);

               dataSource = new DataExtensionDataSource({
                   columns: columns,
                   formatter: function (items) {
                       if(hasPK){
                           dataItems = items;
                           $.each(items, function (index, item) {
                               item.Actions = "<a href='javascript:void(0)' onclick='EditItem("+index+");'><i class='icon-edit'></i></a>&nbsp;<a href='javascript:void(0)' onclick='RemoveItem("+index+");'><i class='icon-trash'></i></a>";
                           });
                        }
                       },
                   delay: 250
               });

               // Associate data source to data grid.
               $('#grDataExtension').datagrid({
                   stretchHeight: false,
                   dataSource: dataSource,
                   renderData: true
               }).on('loaded', function() {

                   // Set Form Fields
                   if (firstLoad){

                       $('#frmAddEdit').empty();
                       $.each(dataSource.columns(), function(index, value) {
                           if(index < dataSource.columns().length -1)
                               $('#frmAddEdit').append('<div class="control-group"><label class="control-label">'+value.label+'</label><div class="controls"><input type="'+value.inputType+'" maxlength="'+value.maxLength+'" id="txt'+value.label+'" class="form-control de-field" '+value.required+'></div></div>');
                       });

                       var thIndex; 

                       if(fieldsFilter != "")
                       {
                           var fieldsToShow = fieldsFilter.split(",");
                           var fieldsToHide = [];
                           var found = false;

                           for (var j = 0; j < columns.length; j++) {
                               for (var i = 0; i < fieldsToShow.length; i++) {
                                   if (fieldsToShow[i].toLowerCase() == columns[j].label.toLowerCase() || columns[j].label.toLowerCase() == "actions") {
                                       found = true;
                                       break;
                                   }
                               }
                               if (!found)
                                   fieldsToHide.push(columns[j].label);

                               found = false;
                           }

                           for (var i = 0; i < fieldsToHide.length; i++) {
                               thIndex = $('th[data-property="' + fieldsToHide[i] + '"]').index() + 1;
                               $('th[data-property="' + fieldsToHide[i] + '"]').hide();
                               $('td:nth-child('+thIndex+')').hide();
                           }
                       }
		               
                       if(!hasPK)
                       {
                           thIndex = $('th[data-property="Actions"]').index() + 1;
                           $('th[data-property="Actions"]').remove();
                           $('td:nth-child(' + thIndex + ')').remove();
                           $('#btnAdd').remove();
                       }
                       $('#modalLoading').modal('hide');
                  }
               });
           })
           .error(function (response) {
             
           })
           .always(function () {
             
           });
}

/*
	Removes an item from a data Extension
*/
function RemoveItem(index){
    if(confirm('Are you sure you would like to delete this item?')){

        var item = dataItems[index];

        var url = apiUrl + "/dataextension/remove";
        delete item.Actions;

        var formData = {};
        formData.Row = [];
        formData.Row.push(JSON.stringify(item));
        formData.ExternalKey = externalKey;

        $.post(url, formData).done(function(data) {

            if(data.resultStatus == "OK"){
                $("#pnlStatus").addClass('alert-success').removeClass('alert-error');
                $('#lblTitle').text('Record removed!');
            }
            else{
                $("#pnlStatus").addClass('alert-error').removeClass('alert-success');
                $('#lblTitle').text('There has been an error when trying to remove this record.');
            }
        })
        .error(function(data){
            $("#pnlStatus").addClass('alert-error').removeClass('alert-success');
            $('#lblTitle').text('There has been an error when trying to remove this record.');
        })
        .always(function(data){
            $('#lblMessage').text(data.message);
            $("#pnlStatus").fadeIn(300).delay(3000).fadeOut(300);
            rawData = null;
            $('#grDataExtension').datagrid('reload');
        });
    }
}

/*
	Edits an specific item from a data extension.
*/
function EditItem(index){

    $(".de-field").val('');

    selectedItem = dataItems[index];
	
    $.each(dataSource._columns, function (index2, item) {
        if(item.property != 'Actions')
            $('#txt'+item.property).val(dataItems[index][item.property]);

        // Disable PK items for edit
        if(item.isPK)
            $('#txt'+item.property).attr('disabled',true);			
    });

    $("#frmAddEdit").validate().resetForm();
	
    $('#lblModalTitle').text('Edit Record');
	
    $('#pnlAddEdit').modal();
}

function AddItem(){

    $(".de-field").val('');
    $(".de-field").removeAttr('disabled');
	
    $('#lblModalTitle').text('Add Record');
	
    $('#pnlAddEdit').modal();
}

function formatNumber(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}