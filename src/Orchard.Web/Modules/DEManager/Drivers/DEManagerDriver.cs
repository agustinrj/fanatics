﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DEManager.Models;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement;

namespace DEManager.Drivers
{
    public class DEManagerDriver : ContentPartDriver<DEManagerPart>
    {
        protected override DriverResult Display(
        DEManagerPart part, string displayType, dynamic shapeHelper)
        {
            return ContentShape("Parts_DEManager",
                () => shapeHelper.Parts_DEManager(
                    ExternalKey: part.ExternalKey,
                    ItemsPerPage: part.ItemsPerPage,
                    FieldsFilter: part.FieldsFilter));
        }

        //GET
        protected override DriverResult Editor(DEManagerPart part, dynamic shapeHelper)
        {
            return ContentShape("Parts_DEManager_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts/DEManager",
                    Model: part,
                    Prefix: Prefix));
        }

        //POST
        protected override DriverResult Editor(
            DEManagerPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            updater.TryUpdateModel(part, Prefix, null, null);
            return Editor(part, shapeHelper);
        }
    }
}