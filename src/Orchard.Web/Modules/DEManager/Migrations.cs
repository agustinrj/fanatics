using System;
using System.Collections.Generic;
using System.Data;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.MetaData;
using Orchard.ContentManagement.MetaData.Builders;
using Orchard.Core.Contents.Extensions;
using Orchard.Data.Migration;
using Orchard.Indexing;

namespace DEManager {
    public class Migrations : DataMigrationImpl {

        public int Create() {
			// Creating table DEManagerPartRecord
			SchemaBuilder.CreateTable("DEManagerPartRecord", table => table
				.ContentPartRecord()
                .Column<string>("ExternalKey", c => c.Unique())
				.Column("ItemsPerPage", DbType.Int32)
				.Column("FieldsFilter", DbType.String)
			);
            return 1;
        }

        public int UpdateFrom1() {

            ContentDefinitionManager.AlterTypeDefinition("Data Extension Manager", cfg => cfg
             .WithPart("TitlePart")
             .WithPart("CommonPart")
             .WithPart("AutoroutePart")
             .WithPart("DEManagerPart")
             .WithPart("LocalizationPart")
             .WithPart("PublishLaterPart")
             .Creatable()
             .Draftable()
             .Indexed());

            ContentDefinitionManager.AlterPartDefinition("DEManagerPart",
              builder => builder.Attachable());

            return 2;
        }

        public int UpdateFrom2() { 
            ContentDefinitionManager.AlterTypeDefinition("Data Extension Manager", cfg => cfg
             .WithPart("ContentPermissionsPart"));
            return 3;
        }

    }
}