﻿using System;
using Orchard.ContentManagement;
using Orchard.ContentManagement.FieldStorage;
using Orchard.Environment.Extensions;

namespace InputFieldWithDefault.CustomFields
{
    [OrchardFeature("InputFieldWithDefault")]
    public class InputFieldWithDefault : ContentField
    {
       public InputFieldWithDefault()
        {
        }
    }
}