﻿using System;
using JetBrains.Annotations;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Localization;
using Orchard.Fields.Fields;
using InputFieldWithDefault.Settings;

namespace InputFieldWithDefault.Drivers
{
    [UsedImplicitly]
    public class InputFieldWithDefaultDriver : ContentFieldDriver<InputFieldWithDefault.CustomFields.InputFieldWithDefault>
    {
        public IOrchardServices Services { get; set; }
        private const string TemplateName = "Fields/InputFieldWithDefault";

        public InputFieldWithDefaultDriver(IOrchardServices services)
        {
            Services = services;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        private static string GetPrefix(ContentField field, ContentPart part)
        {
            return part.PartDefinition.Name + "." + field.Name;
        }

        private static string GetDifferentiator(InputFieldWithDefault.CustomFields.InputFieldWithDefault field, ContentPart part)
        {
            return field.Name;
        }

        protected override DriverResult Display(ContentPart part, InputFieldWithDefault.CustomFields.InputFieldWithDefault field, string displayType, dynamic shapeHelper)
        {
            return ContentShape("InputFieldWithDefault", GetDifferentiator(field, part), () => {
                var settings = field.PartFieldDefinition.Settings.GetModel<InputFieldWithDefaultSettings>();
                return shapeHelper.Fields_Input().Settings(settings);
            });
        }

        protected override DriverResult Editor(ContentPart part, InputFieldWithDefault.CustomFields.InputFieldWithDefault field, dynamic shapeHelper)
        {
            return ContentShape("InputFieldWithDefault_Edit", GetDifferentiator(field, part),
                () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: field, Prefix: GetPrefix(field, part)));
        }

        protected override DriverResult Editor(ContentPart part, InputFieldWithDefault.CustomFields.InputFieldWithDefault field, IUpdateModel updater, dynamic shapeHelper)
        {
            if (updater.TryUpdateModel(field, GetPrefix(field, part), null, null))
            {
                var settings = field.PartFieldDefinition.Settings.GetModel<InputFieldWithDefaultSettings>();

            }

            return Editor(part, field, shapeHelper);
        }

        protected override void Importing(ContentPart part, InputFieldWithDefault.CustomFields.InputFieldWithDefault field, ImportContentContext context)
        {
           
        }

        protected override void Exporting(ContentPart part, InputFieldWithDefault.CustomFields.InputFieldWithDefault field, ExportContentContext context)
        {
           
        }

        protected override void Describe(DescribeMembersContext context)
        {
            context
                .Member(null, typeof(string), T("Value"), T("The value of the field."))
                .Enumerate<InputField>(() => field => new[] { field.Value });
        }
    }
}