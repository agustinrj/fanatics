﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.MetaData;
using Orchard.ContentManagement.MetaData.Builders;
using Orchard.ContentManagement.MetaData.Models;
using Orchard.ContentManagement.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InputFieldWithDefault.Settings
{
    public class InputFieldWithDefaultEditorEvents : ContentDefinitionEditorEventsBase
    {

        public override IEnumerable<TemplateViewModel> PartFieldEditor(ContentPartFieldDefinition definition)
        {
            if (definition.FieldDefinition.Name == "InputFieldWithDefault")
            {
                var model = definition.Settings.GetModel<InputFieldWithDefaultSettings>();
                yield return DefinitionTemplate(model);
            }
        }

        public override IEnumerable<TemplateViewModel> PartFieldEditorUpdate(ContentPartFieldDefinitionBuilder builder, IUpdateModel updateModel)
        {
            if (builder.FieldType != "InputFieldWithDefault")
            {
                yield break;
            }

            var model = new InputFieldWithDefaultSettings();
            if (updateModel.TryUpdateModel(model, "InputFieldWithDefaultSettings", null, null))
            {
                builder.WithSetting("InputFieldWithDefaultSettings.DefaultValue", model.DefaultValue.ToString());
            }

            yield return DefinitionTemplate(model);
        }
    }
}