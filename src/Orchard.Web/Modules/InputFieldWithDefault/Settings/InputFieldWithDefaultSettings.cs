﻿using Orchard.Fields.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InputFieldWithDefault.Settings
{
    public class InputFieldWithDefaultSettings
    {

        public string DefaultValue { get; set; }

    }
}