﻿define(['jquery', 'handlebars', 'bootstrap', 'fuelux', 'moment', 'query-builder','bootstrap-select'], function ($) {

    $('#filter').queryBuilder({
        display_errors:false,
        plugins: { 'bt-selectpicker': { liveSearch: true, multiple: true, maxOptions:false } },
        filters: [{
            id: "FirstName",
            type: "string",
            label: "First Name",
            operators: ['equal','not_equal'],
        },
        {
            id: "PSID",
            type: "integer",
            label: "PSID",
            input: "select",
            operators: ['equal'],
            multiple: true,
            validation: { callback: function (value, rule) { return value.length > 0 } },
            values: [1, 2, 3, 4, 5],
            plugin : 'selectpicker',
            plugin_config: {
                multiple: true,
                liveSearch: true,
                maxOptions:false
            }
        }]
    });
});

// $('#filter').queryBuilder('getAmpscript', false) replace = with == and add IF and THEN and ENDIF at the end as well as %%[]%% and setting the variable