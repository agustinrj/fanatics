﻿requirejs.config({
    paths: {
        'jquery': '//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min',
        'main': '/Modules/Fanatics.CMSTool/Scripts/main.js?v=1',
        'create-schedules': '/Modules/Fanatics.CMSTool/Scripts/schedules/create.js?v=1',
        'create-content': '/Modules/Fanatics.CMSTool/Scripts/content/create.js?v=1',
        'handlebars': '/Modules/Fanatics.CMSTool/Scripts/vendor/handlebars/handlebars',
        'bootstrap': '//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min',
        'moment': '/Modules/Fanatics.CMSTool/Scripts/vendor/moment/moment',
        'fuelux': '//www.fuelcdn.com/fuelux/3.6.3/js/fuelux.min',
        'validate': '//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min',
        'underscore': '/Modules/Fanatics.CMSTool/Scripts/vendor/underscore/underscore',
        'daterangepicker': '/Modules/Fanatics.CMSTool/Scripts/vendor/daterangepicker/daterangepicker',
        'query-builder': '/Modules/Fanatics.CMSTool/Scripts/vendor/query-builder/query-builder',
        'bootstrap-select': '/Modules/Fanatics.CMSTool/Scripts/vendor/bootstrap-select/bootstrap-select'
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'bootstrap': {
            deps: ['jquery'],
            exports: '$.fn.popover'
        },
        'validate': {
            deps: ['jquery']
        },
        'fuelux': {
            deps: ['jquery']
        }
    }
});