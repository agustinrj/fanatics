﻿var contentTypes = [];
define(['jquery', 'handlebars', 'bootstrap', 'fuelux', 'moment', 'query-builder', 'bootstrap-select'], function ($) {
    registerHelpers();
    buildDisplayCondition();
    loadContentTypes();

    //events
    $('#content-type').on('changed.fu.selectlist', function (e, data) {
        if (data.value != "")
            loadContentOptions(data.index);
    });
});

function loadContentOptions(index) {
    $('#content-options').hide();
    var content = contentTypes[index];
    if(content.Fields.length > 0)
    {
        $('#content-options .panel-body').empty();
        var source = $('#content-options-template').html();
        var template = Handlebars.compile(source);
        var html = template({ fields: content.Fields });
        $('#content-options .panel-body').append(html);
        $('#content-options').show();
    }
}

function registerHelpers() {
    Handlebars.registerHelper('ifvalue', function (conditional, options) {
        if (options.hash.value === conditional) {
            return options.fn(this)
        } else {
            return options.inverse(this);
        }
    });
}

function loadContentTypes() {
    // check if campaign objects exist first.
    var post = {};
    post.__RequestVerificationToken = antiForgeryToken;

    $.post("/cms/content/get-types", post, function () { })
        .done(function (result) {
            contentTypes = result;
            $('#content-type').empty();
            var source = $('#content-type-template').html();
            var template = Handlebars.compile(source);
            var html = template({ contentTypes: result });
            $('#content-type').append(html);
            $('#content-type').selectlist();
        })
        .fail(function () {
            alert("Unexpected error, please contact the system administrator");
        });
}

function buildDisplayCondition() {
    $('#filter').queryBuilder({
        display_errors: false,
        plugins: { 'bt-selectpicker': { liveSearch: true, multiple: true, maxOptions: false } },
        filters: [{
            id: "FirstName",
            type: "string",
            label: "First Name",
            operators: ['equal', 'not_equal'],
        },
        {
            id: "PSID",
            type: "integer",
            label: "PSID",
            input: "select",
            operators: ['equal'],
            multiple: true,
            validation: { callback: function (value, rule) { return value.length > 0 } },
            values: [1, 2, 3, 4, 5],
            plugin: 'selectpicker',
            plugin_config: {
                multiple: true,
                liveSearch: true,
                maxOptions: false
            }
        }]
    });
}