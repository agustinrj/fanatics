﻿define(['jquery', 'handlebars', 'bootstrap', 'fuelux', 'moment'], function ($) {
        $('#start-date').datepicker({ allowPastDates: false });
        $('#end-date').datepicker({ allowPastDates: false });

        var contents = [
            {
                Ordinal: 1,
                ContentID: 1,
                ContentType:'Banner'
            },
            {
                Ordinal: 2,
                ContentID: 2,
                ContentType: 'Banner'
            },
            {
                Ordinal: 3,
                ContentID: 3,
                ContentType: 'Content'
            },
            {
                Ordinal: 4,
                ContentID: 4,
                ContentType: 'PI Hash'
            }
        ];

        LoadContentList(contents);

        // Handles content sorting
        $(document).on('click', '.content-item .down-btn', function () {

            var itemsCount = $('.content-item').length;
            var pos = 0;
            pos = $(this).data('pos');

            if (pos < itemsCount) {
                var currentItem = "[data-item='" + pos + "']";
                var nextItem = "[data-item='" + (pos + 1) + "']";

                $(currentItem).fadeOut(500, function () {
                    $(currentItem).detach().insertAfter($(nextItem)).fadeIn(500, function () {
                        SortContents();
                    });
                });
            }
            else
                return false;
        });

        $(document).on('click', '.content-item .up-btn', function () {
            var pos = 0;
            pos = $(this).data('pos');

            if (pos > 1) {
                var currentItem = "[data-item='" + pos + "']";
                var prevItem = "[data-item='" + (pos - 1) + "']";

                $(currentItem).fadeOut(500, function () {
                    $(currentItem).detach().insertBefore($(prevItem)).fadeIn(500, function () {
                        SortContents();
                    });
                });
            }
            else
                return false;
        });

        $(document).on('click', '.delete-content', function () {
            var pos = 0;
            pos = $(this).data('pos');
            var currentItem = "[data-item='" + pos + "']";
            $(currentItem).fadeOut(500, function () {
                $(currentItem).remove();
                SortContents();
            });
        });

});

// Sets the order of the contents
function SortContents() {
    if ($('.content-item').length == 0)
        $('#no-contents').fadeIn();
    else
        $('#no-contents').fadeOut();

    $('.content-item').each(function (index, value) {
        $(this).data('item', index + 1);
        $(this).attr('data-item', index + 1);
        $(this).find('.up-btn').data('pos', index + 1);
        $(this).find('.up-btn').attr('data-pos', index + 1);
        $(this).find('.up-btn span').css("color", "");
        $(this).find('.down-btn').data('pos', index + 1);
        $(this).find('.down-btn').attr('data-pos', index + 1);
        $(this).find('.delete-content').data('pos', index + 1);
        $(this).find('.delete-content').attr('data-pos', index + 1);
        $(this).find('.down-btn span').css("color", "");
        $(this).find('.content-position').text(index + 1);

        // set button styles
        if (index == 0)
            $(this).find('.up-btn span').css("color", "#999");

        if (index == $('.content-item').length - 1)
            $(this).find('.down-btn span').css("color", "#999");
    });
}

function LoadContentList(contents) {
    $.each(contents, function (index, content) {
        var source = $("#content-item-template").html();
        var template = Handlebars.compile(source);
        var html = template(content);
        var el = $('#content-list').append(html);
    });

    SortContents();
}