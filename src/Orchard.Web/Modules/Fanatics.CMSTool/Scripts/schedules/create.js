﻿define(['jquery', 'handlebars', 'bootstrap', 'fuelux', 'moment', 'bootstrap-select'], function ($) {
        $('#start-date').datepicker({ allowPastDates: false });
        $('#end-date').datepicker({ allowPastDates: false });

        var contents = [
            {
                Ordinal: 1,
                ContentID: -1,
                ContentType:'Banner'
            },
            {
                Ordinal: 2,
                ContentID: -1,
                ContentType: 'Banner'
            },
            {
                Ordinal: 3,
                ContentID: -1,
                ContentType: 'Content'
            },
            {
                Ordinal: 4,
                ContentID: -1,
                ContentType: 'PI Hash'
            }
        ];

        LoadContentList(contents);

        // Handles content sorting
        $(document).on('click', '.content-item .down-btn', function () {

            var itemsCount = $('.content-item').length;
            var pos = 0;
            pos = $(this).data('pos');

            if (pos < itemsCount) {
                var currentItem = "[data-item='" + pos + "']";
                var nextItem = "[data-item='" + (pos + 1) + "']";

                $(currentItem).fadeOut(500, function () {
                    $(currentItem).detach().insertAfter($(nextItem)).fadeIn(500, function () {
                        SortContents();
                    });
                });
            }
            else
                return false;
        });

        $(document).on('click', '.content-item .up-btn', function () {
            var pos = 0;
            pos = $(this).data('pos');

            if (pos > 1) {
                var currentItem = "[data-item='" + pos + "']";
                var prevItem = "[data-item='" + (pos - 1) + "']";

                $(currentItem).fadeOut(500, function () {
                    $(currentItem).detach().insertBefore($(prevItem)).fadeIn(500, function () {
                        SortContents();
                    });
                });
            }
            else
                return false;
        });

        $(document).on('click', '.delete-content', function () {
            var pos = 0;
            pos = $(this).data('pos');
            var currentItem = "[data-item='" + pos + "']";
            $(currentItem).fadeOut(500, function () {
                $(currentItem).remove();
                SortContents();
            });
        });

    // handles schedule save
        $('#btn-save').click(function () {

            var $btn = $(this).button('loading');
            var hasErrors = false;

            if (!hasErrors) {
                $.post("/cms/schedules/save", GetScheduleObject(), function () { })
                .done(function (result) {
                    if (result.Status.toLowerCase() == "ok") {
                        showModal({ title: "Campaign Succesfully Saved!", message: "Page will refresh in 1 second...", readonly: true });
                    }
                    else {
                        if (result.Message != null)
                            showModal({ title: "Campaign Submission Error", message: "A Campaign with the same name already exists", readonly: false });
                        else
                            showModal({ title: "Campaign Submission Error", message: "There has been an error submitting your campaign. Please contact the system administrator.", readonly: false });
                    }
                })
                .fail(function () {
                    showModal({ title: "Campaign Submission Error", message: "There has been an error submitting your campaign. Please contact the system administrator.", readonly: false });
                })
                .always(function () {
                    $btn.button('reset');
                });
            }
            else
                $btn.button('reset');

        });
});

// Sets the order of the contents
function SortContents() {
    if ($('.content-item').length == 0)
        $('#no-contents').fadeIn();
    else
        $('#no-contents').fadeOut();

    $('.content-item').each(function (index, value) {
        $(this).data('item', index + 1);
        $(this).attr('data-item', index + 1);
        $(this).find('.up-btn').data('pos', index + 1);
        $(this).find('.up-btn').attr('data-pos', index + 1);
        $(this).find('.up-btn span').css("color", "");
        $(this).find('.down-btn').data('pos', index + 1);
        $(this).find('.down-btn').attr('data-pos', index + 1);
        $(this).find('.delete-content').data('pos', index + 1);
        $(this).find('.delete-content').attr('data-pos', index + 1);
        $(this).find('.down-btn span').css("color", "");
        $(this).find('.content-position').text(index + 1);

        // set button styles
        if (index == 0)
            $(this).find('.up-btn span').css("color", "#999");

        if (index == $('.content-item').length - 1)
            $(this).find('.down-btn span').css("color", "#999");
    });
}

function LoadContentList(contents) {
    $.each(contents, function (index, content) {
        var source = $("#content-item-template").html();
        var template = Handlebars.compile(source);
        var html = template(content);
        var el = $('#content-list').append(html);
    });

    SortContents();
}

// returns the schedule object for save operation
function GetScheduleObject() {
    var post = {};
    var sched = {};
    var startDate = new Date($('#start-date').datepicker('getDate'));
    var endDate = new Date($('#end-date').datepicker('getDate'));

    post.__RequestVerificationToken = antiForgeryToken
    
    sched.Id = scheduleID ? scheduleID : -1;
    sched.Name = $('#name').val();
    sched.StartDate = startDate.toJSON();
    sched.ZoneID = $('#zone').selectlist('selectedItem').value;
    sched.PSIDs = $('#psids').val()  ? $('#psids').val().join(",") : "";

    if ($('#chk-no-end-date input:checked').length <= 0)
        sched.EndDate = endDate.toJSON();
    else
        sched.EndDate = null;

    sched.Contents = [];

    // Contents
    $.each($('.content-item'), function (i, content) {
        var id = $(this).data('content-id');
        var ordinal = $(this).data('item');
        sched.Contents.push({ Id: id, Ordinal: ordinal });
    });

    post.Schedule = JSON.stringify(sched);

    return post;
}