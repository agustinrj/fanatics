﻿using Fanatics.CMSTool.Models;
using Orchard.ContentManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fanatics.CMSTool.Services
{
    public class ContentService
    {
        /// <summary>
        /// Gets the content types that are part of the CMS Tool
        /// </summary>
        /// <param name="contentManager"></param>
        /// <returns></returns>
        public List<ContentType> GetContentTypes(IContentManager contentManager)
        {
            List<ContentType> contentTypes = new List<ContentType>();

            //Get all content types with the CMSTool prefix in them
            var contentList = contentManager.GetContentTypeDefinitions().Where(p => p.Name.Contains("CMSTool")).ToList();

            foreach (var contentType in contentList)
            {
                ContentType cType = new ContentType();
                cType.DisplayName = contentType.DisplayName.Replace("CMSTool_", "");
                cType.TechnicalName = contentType.Name;

                // list the fields that are not part of the default content type
                var contentParts = contentType.Parts.Where(p => !p.PartDefinition.Name.Contains("Common")).ToList();

                foreach (var part in contentParts)
                {
                    // explore the fields and add them to our POCO 
                    foreach (var field in part.PartDefinition.Fields)
                    {
                        Fanatics.CMSTool.Models.ContentField cField = new Fanatics.CMSTool.Models.ContentField();
                        bool required = false;
                        bool.TryParse(field.Settings.Where(s => s.Key.Contains("Required")).SingleOrDefault().Value,out required);

                        cField.Name = field.DisplayName;
                        cField.Type = field.Settings.Where(s => s.Key.Contains("Type")).SingleOrDefault().Value;
                        cField.Required = required;
                        cType.Fields.Add(cField);
                    }
                }

                contentTypes.Add(cType);
            }

            return contentTypes;
        }
    }
}