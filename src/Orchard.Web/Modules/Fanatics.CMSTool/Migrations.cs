﻿using System;
using System.Collections.Generic;
using System.Data;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.MetaData;
using Orchard.ContentManagement.MetaData.Builders;
using Orchard.Core.Contents.Extensions;
using Orchard.Data.Migration;
using Orchard.Indexing;

namespace Fanatics.CMSTool
{
    public class Migrations : DataMigrationImpl
    {
        public int Create()
        {
            // Creating table CMSTool_Schedules
            SchemaBuilder.CreateTable("Schedule", table => table
                .Column<int>("Id", column => column.PrimaryKey().Identity())
                .Column<string>("Name", column=>column.NotNull())
                .Column<int>("ZoneID", column => column.NotNull())
                .Column<DateTime>("StartDate", column => column.NotNull())
                .Column<DateTime>("EndDate")
                .Column<string>("PSIDs")
            );

            SchemaBuilder.CreateTable("ScheduleContent",table => table
            .Column<int>("Id", column => column.PrimaryKey().Identity())
            .Column<int>("ContentID", column => column.NotNull())
            .Column<int>("Ordinal", column => column.NotNull())
            .Column<int>("Schedule_Id")
        );
            
            return 1;
        }

        public int UpdateFrom1()
        {
            SchemaBuilder.CreateTable("ScheduleContentField", table => table
            .Column<int>("Id", column => column.PrimaryKey().Identity())
            .Column<int>("FieldName", column => column.NotNull())
            .Column<int>("Value", column => column.NotNull())
            .Column<int>("Schedule_Contents_Id"));

            SchemaBuilder.AlterTable("ScheduleContent", table => table
             .AddColumn<string>("Rules"));

            SchemaBuilder.AlterTable("ScheduleContent", table => table
            .AddColumn<string>("HTMLContent"));

            SchemaBuilder.AlterTable("ScheduleContent", table => table
            .DropColumn("ContentID"));

            return 2;
        }
    }
}