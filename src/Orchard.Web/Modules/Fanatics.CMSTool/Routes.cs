﻿using Orchard.Mvc.Routes;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;

namespace Fanatics.CMSTool
{
    public class Routes : IRouteProvider
    {
        public void GetRoutes(ICollection<RouteDescriptor> routes)
        {
            foreach (var routeDescriptor in GetRoutes())
                routes.Add(routeDescriptor);
        }

        public IEnumerable<RouteDescriptor> GetRoutes()
        {
            return new[] {
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "cms", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "Fanatics.CMSTool"}, // this is the name of your module
                            {"controller", "Home"},
                            {"action", "Index"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.CMSTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "cms/schedules/create", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "Fanatics.CMSTool"}, // this is the name of your module
                            {"controller", "Schedules"},
                            {"action", "Create"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.CMSTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                }
                ,
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "cms/schedules/save", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "Fanatics.CMSTool"}, // this is the name of your module
                            {"controller", "Schedules"},
                            {"action", "Save"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.CMSTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "cms/content/create", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "Fanatics.CMSTool"}, // this is the name of your module
                            {"controller", "Content"},
                            {"action", "Create"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.CMSTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "cms/content/get-types", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "Fanatics.CMSTool"}, // this is the name of your module
                            {"controller", "Content"},
                            {"action", "GetContentTypes"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.CMSTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                }
            };
        }
    }
}