﻿using Fanatics.CMSTool.Services;
using Orchard.ContentManagement;
using Orchard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fanatics.CMSTool.Controllers
{
    public class ContentController : Controller
    {
        public ISessionLocator SessionLocator { get; set; }

        public IContentManager ContentManager { get; set; }

    
        public ContentController(ISessionLocator sessionLocator,
            IContentManager contentManager,
            IRepository<Models.Schedule> schedulesRepository)
        {
            SessionLocator = sessionLocator;
            ContentManager = contentManager;
        }

        // GET: Content
        public ActionResult Index()
        {
            return View();
        }

        // GET: Content
        public ActionResult Create()
        {
            // get content definition
            // get fields
            // put fields with values?
           
            /*
            var blog = _contentManager.New("Blog");
            blog.As<ICommonPart>().Owner = owner;
            blog.As<TitlePart>().Title = Title;
            if (!String.IsNullOrEmpty(Description)) {
                blog.As<BlogPart>().Description = Description;
            }

            if (Homepage || !String.IsNullOrWhiteSpace(Slug)) {
                dynamic dblog = blog;
                if (dblog.AutoroutePart != null) {
                    dblog.AutoroutePart.UseCustomPattern = true;
                    dblog.AutoroutePart.CustomPattern = Homepage ? "/" : Slug;
                }
            }
            
            _contentManager.Create(blog);
            */

            return View();
        }

        // POST: get-types
        [HttpPost]
        public JsonResult GetContentTypes()
        {
            ContentService contentService = new ContentService();
            return Json(contentService.GetContentTypes(ContentManager));
        }

    }
}