﻿using Fanatics.CMSTool.Models;
using Fanatics.CMSTool.Services;
using Orchard.ContentManagement;
using Orchard.Data;
using Orchard.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Fanatics.CMSTool.Controllers
{
    public class SchedulesController : Controller
    {
        public ILogger Logger { get; set; }

        public ISessionLocator SessionLocator { get; set; }

        public IContentManager ContentManager { get; set; }

        IRepository<Models.Schedule> SchedulesRepository { get; set; }

        public SchedulesController(ISessionLocator sessionLocator,
            IContentManager contentManager,
            IRepository<Models.Schedule> schedulesRepository)
        {
            SessionLocator = sessionLocator;
            ContentManager = contentManager;
            SchedulesRepository = schedulesRepository;
            Logger = NullLogger.Instance;
        }

        // GET: Schedules
        public ActionResult Index()
        {
            return View();
        }

        // GET: Schedules
        public ActionResult Create()
        {
            return View();
        }

        // GET: Schedules
        [ValidateInput(false)]
        public JsonResult Save(SchedulePost post)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                ScheduleService scheduleService = new ScheduleService();
                var s = serializer.Deserialize<Models.Schedule>(post.Schedule);
                return Json(scheduleService.Save(s, SchedulesRepository));
            }
            catch (Exception ex)
            {
                var s = serializer.Deserialize<Models.Schedule>(post.Schedule);
                Logger.Error(string.Format("CMS Tool Error - saving schedule ID {0} - {1}:<br/><strong>Object Submitted<strong><br/><br/>{2}<br/><br/><strong>Exception:</strong><br/>:<br/>{3}", s.Id, s.Name,post.Schedule, ex.ToString()));
                throw;
            }
            
        }
    }
}