﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Records;
using Orchard.Data.Conventions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fanatics.CMSTool.Models
{
    public class Schedule 
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int ZoneID { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime ? EndDate { get; set; }
        public virtual string PSIDs { get; set; }

        [CascadeAllDeleteOrphan]
        public virtual IList<ScheduleContent> Contents { get; set; }

        public Schedule() {
            Contents = new List<ScheduleContent>();
        }
    }
}