﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fanatics.CMSTool.Models
{
    public class ScheduleContentField
    {
        public virtual int Id { get; set; }
        public virtual string FieldName { get; set; }
        public virtual string Value { get; set; }

        public ScheduleContentField() { }
    }
}