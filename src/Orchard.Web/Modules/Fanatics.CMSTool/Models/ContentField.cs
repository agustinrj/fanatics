﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fanatics.CMSTool.Models
{
    public class ContentField
    {   
        public string Name { get; set; }
        public string Type { get; set; }
        public bool Required { get; set; }
    }
}