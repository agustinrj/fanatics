﻿using Orchard.Data.Conventions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fanatics.CMSTool.Models
{
    public class ScheduleContent
    {
        public virtual int Id { get; set; }
        public virtual int Ordinal { get; set; }
        public virtual string Rules { get; set; }
        public virtual string HTMLContent { get; set; }
        [CascadeAllDeleteOrphan]
        public virtual IList<ScheduleContentField> Fields { get; set; }

        public ScheduleContent() {
            Fields = new List<ScheduleContentField>();
        }
    }
}