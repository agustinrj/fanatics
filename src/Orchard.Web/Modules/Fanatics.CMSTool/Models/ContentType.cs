﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fanatics.CMSTool.Models
{
    public class ContentType
    {
        public string TechnicalName { get; set; }
        public string DisplayName { get; set; }
        public List<ContentField> Fields { get; set; }

        public ContentType()
        {
            Fields = new List<ContentField>();
        }
    }
}