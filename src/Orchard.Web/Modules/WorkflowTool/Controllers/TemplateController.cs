﻿using Orchard;
using Orchard.ContentManagement;
using Orchard.Localization;
using Orchard.MediaLibrary.Models;
using Orchard.Security;
using Orchard.Taxonomies.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkflowTool.Models;


namespace WorkflowTool.Controllers
{
    public class TemplateController : BaseController
    {
        public TemplateController(
            IOrchardServices services,
            ITaxonomyService taxonomies) {
            Services = services;
            T = NullLocalizer.Instance;
            Taxonomies = taxonomies;
        }

        public JsonResult GetByBU(int buID)
        {
            var templates = WorkflowServices.Template.GetByBU(buID, Services.ContentManager,Taxonomies);
            return Json(new { Status = "success", Data = templates });
        }

        public JsonResult GetDetails(string sfmcName)
        {
            var template = WorkflowServices.Template.GetDetails(sfmcName);
            return Json(new { Status = "success", Data = template});
        }

        public JsonResult GetByUser()
        {  
            List<dynamic> templatesData = new List<dynamic>();
            List<int> addedTemplates = new List<int>();

            // get businessUnit for logged user
            var user = Services.WorkContext.CurrentUser;
            var businessUnits = Taxonomies.GetTermsForContentItem(user.ContentItem.Id);

            foreach (Orchard.Taxonomies.Models.TermPart bu in businessUnits)
            {
                string buID = bu.FullPath.Replace("/","");

                // get templates for each bu assigned     

                var templates = Services.ContentManager.Query<WorkflowToolTemplatePart, WorkflowToolTemplatePartRecord>().Where(t => t.BusinessUnits.Contains(buID)).List();

                foreach (var template in templates)
                {
                    // get template images
                    var thumbnail = Services.ContentManager.Query<MediaPart, MediaPartRecord>().Where(m => m.Id == template.ThumbnailID).List().FirstOrDefault();
                    var map = Services.ContentManager.Query<MediaPart, MediaPartRecord>().Where(m => m.Id == template.MapImageID).List().FirstOrDefault();

                    if (!addedTemplates.Contains(template.Id))
                    {
                        addedTemplates.Add(template.Id);
                        templatesData.Add(new
                        {
                            ID = template.Id,
                            Thumbnail = thumbnail != null ? Path.Combine(thumbnail.FolderPath, thumbnail.FileName) : "",
                            MapImage = map != null ? Path.Combine(map.FolderPath, map.FileName) : "",
                            BusinesUnits = template.BusinessUnits,
                            SFMCName = template.SFMCName
                        });
                    }
                }
            }

            return Json(new { Status = "success", Data = templatesData });
        }
    }
}