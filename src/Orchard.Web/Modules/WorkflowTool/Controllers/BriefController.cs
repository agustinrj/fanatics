﻿using Orchard;
using Orchard.ContentManagement;
using Orchard.Localization;
using Orchard.Taxonomies.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WorkflowTool.Controllers
{
    public class BriefController : BaseController
    {

        public BriefController(
            IOrchardServices services,
            ITaxonomyService taxonomies)
        {
            Services = services;
            T = NullLocalizer.Instance;
            Taxonomies = taxonomies;
        }

        // GET: Brief
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ViewBrief(string campaignID)
        {
            return View("Index", WorkflowServices.CampaignService.GetDetails(campaignID,Services.ContentManager));
        }
    }
}