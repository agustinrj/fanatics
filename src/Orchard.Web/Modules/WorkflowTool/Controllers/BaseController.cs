﻿using Orchard;
using Orchard.Localization;
using Orchard.Taxonomies.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkflowTool.Services;

namespace WorkflowTool.Controllers
{
    public class BaseController:Controller
    {
        public ServiceFactory WorkflowServices { get; set; }
        public IOrchardServices Services { get; set; }
        public Localizer T { get; set; }
        public ITaxonomyService Taxonomies { get; set; }

        public BaseController() {
            WorkflowServices = new ServiceFactory();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!Services.Authorizer.Authorize(Permissions.AccessWorkflowTool, T("Not authorized to access the Workflow Tool")))
                filterContext.Result = new HttpUnauthorizedResult();
          
             base.OnActionExecuting(filterContext);
        }
    }
}