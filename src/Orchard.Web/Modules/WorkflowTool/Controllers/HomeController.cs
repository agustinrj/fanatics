﻿using Orchard;
using Orchard.ContentManagement;
using Orchard.Localization;
using Orchard.MediaLibrary.Models;
using Orchard.Security;
using Orchard.Taxonomies.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkflowTool.Models;

namespace WorkflowTool.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(
            IOrchardServices services,
            ITaxonomyService taxonomies) {
            Services = services;
            T = NullLocalizer.Instance;
            Taxonomies = taxonomies;
        }

        public ActionResult Index()
        {  
            return View();
        }

        public ActionResult Help()
        {
            return View();
        }
    }
}