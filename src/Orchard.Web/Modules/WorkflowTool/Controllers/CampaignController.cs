﻿using Orchard;
using Orchard.Data;
using Orchard.Localization;
using Orchard.Logging;
using Orchard.Security;
using Orchard.Taxonomies.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WorkflowTool.Models;

namespace WorkflowTool.Controllers
{
    public class CampaignController : BaseController
    {
        public ILogger Logger { get; set; }

        public CampaignController(
            IOrchardServices services,
            ITaxonomyService taxonomies) {
            Services = services;
            T = NullLocalizer.Instance;
            Taxonomies = taxonomies;
            Logger = NullLogger.Instance;
        }

        // GET: Campaign
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Get(int pageIndex, int pageSize, string sortDirection, string sortBy, string filterBy, string searchBy, string startDate, string endDate)
        {
            var campaigns = WorkflowServices.CampaignService.Get(pageIndex, pageSize, sortDirection, sortBy, filterBy, searchBy, startDate, endDate);

            int count = campaigns.Count;

            var data = new { total = count, items = campaigns.Skip(pageIndex * pageSize).Take(pageSize).ToList() };

            return Json(new { Status = "success", Data = data },JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public JsonResult Save(CampaignPost post)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                var c = serializer.Deserialize<Fanatics.Email.Model.Campaign>(post.Campaign);
                return Json(WorkflowServices.CampaignService.Save(c));
            }
            catch (Exception ex)
            {
                var c = serializer.Deserialize<Fanatics.Email.Model.Campaign>(post.Campaign);
                Logger.Error(string.Format("Error saving campaign {0}:<br/><strong>Object Submitted<strong><br/><br/>{1}<br/><br/><strong>Exception:</strong><br/>:<br/>{2}", c.EmailName, post.Campaign,ex.ToString()));
                throw;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult CreateUISend(Fanatics.Email.Model.EmailSendCreateRequest post)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                var c = serializer.Deserialize<Fanatics.Email.Model.Campaign>(post.CampaignString);
                post.Campaign = c;
                return Json(WorkflowServices.CampaignService.CreateUISend(post));
            }
            catch (Exception ex)
            {
                var c = serializer.Deserialize<Fanatics.Email.Model.Campaign>(post.CampaignString);
                Logger.Error(string.Format("Error creating UI Send for ID {0}:<br/><br/><strong>Exception:</strong><br/>:<br/>{1}", c.Id, ex.ToString()));
                return Json(new {Status="Error",Message=ex.Message });
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult CreateEmail(Fanatics.Email.Model.EmailSendCreateRequest post)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try
            {
                var c = serializer.Deserialize<Fanatics.Email.Model.Campaign>(post.CampaignString);
                post.Campaign = c;
                return Json(WorkflowServices.CampaignService.CreateEmail(post));
            }
            catch (Exception ex)
            {
                var c = serializer.Deserialize<Fanatics.Email.Model.Campaign>(post.CampaignString);
                Logger.Error(string.Format("Error creating Email for ID {0}:<br/><br/><strong>Exception:</strong><br/>:<br/>{1}", c.Id, ex.ToString()));
                return Json(new { Status = "Error", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult UISendExists(string campaignID)
        {
            try
            {
                return Json(WorkflowServices.CampaignService.UISendExists(campaignID));
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("Error on UISendExists method for ID {0}:<br/><br/><strong>Exception:</strong><br/>:<br/>{1}", campaignID, ex.ToString()));
                throw;
            }
        }

        [HttpPost]
        public JsonResult EmailExists(string campaignID)
        {
            try
            {
                return Json(WorkflowServices.CampaignService.EmailExists(campaignID));
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("Error on EmailExists method for ID {0}:<br/><br/><strong>Exception:</strong><br/>:<br/>{1}", campaignID, ex.ToString()));
                throw;
            }
        }

        [ValidateInput(false)]
        [HttpPost]
        public JsonResult AddFeedback(CampaignFeedbackPost post)
        {
            try
            {
                return Json(WorkflowServices.CampaignService.AddFeedback(post.CampaignID, post.Feedback, Services));
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("Error adding campaign feedback for ID {0}:<br/><strong>Feedback<strong><br/><br/>{1}<br/><br/><strong>Exception:</strong><br/>:<br/>{2}", post.CampaignID, post.Feedback, ex.ToString()));
                throw;
            }
        }

        [HttpPost]
        public JsonResult CopyCampaign(CopyCampaignPost post)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                return Json(WorkflowServices.CampaignService.CopyCampaign(post.CampaignID, post.CampaignName));
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("Error copying campaign ID {0}:<br/><br/><strong>Exception:</strong><br/>:<br/>{2}", post.CampaignID, ex.ToString()));
                throw;
            }
        }

        [HttpPost]
        public JsonResult ChangeStatus(ChangeStatusPost post)
        {
            return Json(WorkflowServices.CampaignService.ChangeStatus(post.CampaignID,post.Status));
        }

        [HttpPost]
        public JsonResult ChangeOwner(ChangeOwnerPost post)
        {
            return Json(WorkflowServices.CampaignService.ChangeOwner(post.CampaignID, post.Owner,post.OwnerEmail));
        }

        [HttpPost]
        public JsonResult ChangeJobIDs(ChangeJobIDPost post)
        {
            return Json(WorkflowServices.CampaignService.ChangeJobIDs(post.CampaignID, post.JobIDs));
        }

        [HttpPost]
        public JsonResult Delete(string campaignID)
        {
            return Json(WorkflowServices.CampaignService.Delete(campaignID,Services.WorkContext.CurrentUser.UserName));
        }

        public ActionResult Edit(string campaignID)
        {
            return View("Index", WorkflowServices.CampaignService.GetDetails(campaignID,Services.ContentManager));
        }

        
    }
}