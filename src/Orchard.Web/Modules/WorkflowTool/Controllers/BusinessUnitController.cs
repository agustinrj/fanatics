﻿using Orchard;
using Orchard.ContentManagement;
using Orchard.Data;
using Orchard.Fields.Fields;
using Orchard.Localization;
using Orchard.MediaLibrary.Models;
using Orchard.Security;
using Orchard.Taxonomies.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkflowTool.Models;
namespace WorkflowTool.Controllers
{
    public class BusinessUnitController:BaseController
    {
        public ISessionLocator SessionLocator { get; set; }

        public BusinessUnitController(
            IOrchardServices services,
            ITaxonomyService taxonomies,
            ISessionLocator sessionLocator) {
            Services = services;
            T = NullLocalizer.Instance;
            Taxonomies = taxonomies;
            SessionLocator = sessionLocator;
        }

        /// <summary>
        /// Gets all available business units
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Get() {
            var taxononomies = WorkflowServices.BusinessUnit.Get(Taxonomies,SessionLocator,Services);
            
            return Json(new { Status = "success", Data = taxononomies });
        }

        /// <summary>
        /// Gets all available business units
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAll()
        {
            var taxononomies = WorkflowServices.BusinessUnit.Get(Taxonomies, null, Services);

            return Json(new { Status = "success", Data = taxononomies });
        }
    }
}