﻿using Orchard;
using Orchard.ContentManagement;
using Orchard.Data;
using Orchard.Localization;
using Orchard.Taxonomies.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WorkflowTool.Controllers
{
    public class ImportController : BaseController
    {
        public ISessionLocator SessionLocator { get; set; }

        public ImportController(
           IOrchardServices services,
           ITaxonomyService taxonomies,
           ISessionLocator sessionLocator)
        {
            Services = services;
            T = NullLocalizer.Instance;
            Taxonomies = taxonomies;
            SessionLocator = sessionLocator;
        }

        // GET: Brief
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ProcessImport(HttpPostedFileBase file)
        {
            var importResults = WorkflowServices.CampaignService.Import(file, Taxonomies, Services, SessionLocator);
            return View("Results",importResults);
        }
    }
}