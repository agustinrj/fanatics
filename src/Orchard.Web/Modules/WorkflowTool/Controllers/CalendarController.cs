﻿using Orchard;
using Orchard.Data;
using Orchard.Localization;
using Orchard.Taxonomies.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WorkflowTool.Controllers
{
    public class CalendarController : BaseController
    {

        public ISessionLocator SessionLocator { get; set; }

        public CalendarController(
            IOrchardServices services,
            ITaxonomyService taxonomies,
            ISessionLocator sessionLocator)
        {
            Services = services;
            T = NullLocalizer.Instance;
            Taxonomies = taxonomies;
            SessionLocator = sessionLocator;
        }


        // GET: Calendar
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetEvents()
        {
            var events = WorkflowServices.CalendarService.GetEvents(Taxonomies, null, Services);
            
            return Json(new { Status = "success", Data = events }, JsonRequestBehavior.AllowGet);
        }

    }
}