﻿using Orchard;
using Orchard.Localization;
using Orchard.Taxonomies.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WorkflowTool.Controllers
{
    public class AudienceController:BaseController
    {
        public AudienceController(
            IOrchardServices services,
            ITaxonomyService taxonomies) {
            Services = services;
            T = NullLocalizer.Instance;
            Taxonomies = taxonomies;
        }

        [HttpPost]
        public JsonResult GetByBusinessUnit(string buName, bool IsNewBU)
        {
            var audiences = WorkflowServices.AudienceService.GetByBusinessUnit(buName,IsNewBU);
            var deAudiences = audiences.Where(a => a.AudienceType.ToLower() == "deaudience").ToList();
            var psidAudiences = audiences.Where(a => a.AudienceType.ToLower() == "psid").ToList();
            var teamidAudiences = audiences.Where(a => a.AudienceType.ToLower() == "teamid").ToList();
            var supressionAudiences = audiences.Where(a => a.AudienceType.ToLower() == "supression").ToList();

            var teamidAudiencesGrouped = teamidAudiences.GroupBy( p => p.AudienceGroup,(key, g) => new { 
                                                 AudienceGroup = key, 
                                                 Items = g.ToList() 
                                                }).OrderBy(a => a.AudienceGroup).ToList();         
   
            var psidAudiencesGrouped = psidAudiences.GroupBy(p => p.AudienceGroup, (key, g) => new
            {
                AudienceGroup = key,
                Items = g.ToList()
            }).OrderBy(a => a.AudienceGroup).ToList();

            var deAudiencesGrouped = deAudiences.GroupBy(p => p.AudienceGroup, (key, g) => new
            {
                AudienceGroup = key,
                Items = g.ToList()
            }).OrderBy(a => a.AudienceGroup).ToList();

            var supressionAudiencesGrouped = supressionAudiences.GroupBy(p => p.AudienceGroup, (key, g) => new
            {
                AudienceGroup = key,
                Items = g.ToList()
            }).OrderBy(a => a.AudienceGroup).ToList();

            var data = new
            {
                DEAudiences = deAudiencesGrouped,
                hasDEAudiences = deAudiencesGrouped.Count > 0,
                PSIDAudiences = psidAudiencesGrouped,
                hasPSIDAudiences = psidAudiencesGrouped.Count > 0,
                teamIDAudiences = teamidAudiencesGrouped,
                hasTeamIDAudiences = teamidAudiencesGrouped.Count > 0,
                hasSupressions = supressionAudiencesGrouped.Count > 0,
                Supressions = supressionAudiencesGrouped
            };

            return Json(new { Status = "success", Data = data });
        }
    }
}