﻿using Orchard;
using Orchard.ContentManagement;
using Orchard.Data;
using Orchard.Localization;
using Orchard.MediaLibrary.Models;
using Orchard.Security;
using Orchard.Taxonomies.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkflowTool.Models;
namespace WorkflowTool.Controllers
{
    public class UserController:BaseController
    {
        public ISessionLocator SessionLocator { get; set; }

        public UserController(
            IOrchardServices services,
            ITaxonomyService taxonomies,
            ISessionLocator sessionLocator) {
            Services = services;
            T = NullLocalizer.Instance;
            Taxonomies = taxonomies;
            SessionLocator = sessionLocator;
        }

        [HttpPost]
        public JsonResult GetByBusinessUnit(int buID)
        {
            var users = WorkflowServices.User.GetByBusinessUnit(buID,Services,SessionLocator);
            return Json(new { Status = "success", Data = users });
        }

        [HttpPost]
        public JsonResult GetOpsUsers()
        {
            var users = WorkflowServices.User.GetOpsUsers(SessionLocator,Services);
            return Json(new { Status = "success", Data = users });
        }
    }
}