﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Records;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WorkflowTool.Models
{
    public class WorkflowToolTemplatePartRecord: ContentPartRecord
    {
        public virtual string SFMCName { get; set; }
        public virtual int ThumbnailID { get; set; }
        public virtual int MapImageID { get; set; }
        public virtual string BusinessUnits { get; set; }
        public virtual string Toggles { get; set; }
        public virtual string Description { get; set; }
    }

    public class WorkflowToolTemplatePart : ContentPart<WorkflowToolTemplatePartRecord>
    {
        [Required]
        public string SFMCName
        {
            get { return Retrieve(r => r.SFMCName); }
            set { Store(r => r.SFMCName, value); }
        }

        [Required]
        public int ThumbnailID
        {
            get { return Retrieve(r => r.ThumbnailID); }
            set { Store(r => r.ThumbnailID, value); }
        }

        [Required]
        public int MapImageID
        {
            get { return Retrieve(r => r.MapImageID); }
            set { Store(r => r.MapImageID, value); }
        }

        public string BusinessUnits
        {
            get { return Retrieve(r => r.BusinessUnits); }
            set { Store(r => r.BusinessUnits, value); }
        }

        public string Toggles
        {
            get { return Retrieve(r => r.Toggles); }
            set { Store(r => r.Toggles, value); }
        }

        public string Description
        {
            get { return Retrieve(r => r.Description); }
            set { Store(r => r.Description, value); }
        }
    }
}