﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkflowTool.Models
{
    public class CopyCampaignPost
    {
        public string CampaignID { get; set; }
        public string CampaignName { get; set; }
    }
}