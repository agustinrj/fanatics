﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkflowTool.Models
{
    public class ChangeStatusPost
    {
        public string CampaignID { get; set; }
        public string Status { get; set; }
    }
}