﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkflowTool.Models
{
    public class CampaignImportResult
    {
        public int ProcessedCount { get; set; }
        public string OverallStatus { get; set; }
        public List<String> Errors { get; set; }

        public CampaignImportResult() {
            Errors = new List<string>();
        }

    }
}