﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkflowTool.Models
{
    public class ChangeOwnerPost
    {
        public string CampaignID { get; set; }
        public string Owner { get; set; }
        public string OwnerEmail { get; set; }
    }
}