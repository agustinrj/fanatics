﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkflowTool.Models
{
    public class CampaignFeedbackPost
    {
        public string CampaignID { get; set; }
        public string Feedback { get; set; }
    }
}