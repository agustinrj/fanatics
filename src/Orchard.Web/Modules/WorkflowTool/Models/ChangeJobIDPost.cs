﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkflowTool.Models
{
    public class ChangeJobIDPost
    {
        public string CampaignID { get; set; }
        public string JobIDs { get; set; }
    }
}