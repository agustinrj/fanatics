﻿using Orchard.Mvc.Routes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WorkflowTool
{
    public class Routes : IRouteProvider
    {
        public void GetRoutes(ICollection<RouteDescriptor> routes)
        {
            foreach (var routeDescriptor in GetRoutes())
                routes.Add(routeDescriptor);
        }

        public IEnumerable<RouteDescriptor> GetRoutes()
        {
            return new[] {
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "workflow", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Home"},
                            {"action", "Index"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "workflow/template/getbyuser", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Template"},
                            {"action", "GetByUser"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "workflow/help", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Home"},
                            {"action", "Help"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "workflow/businessunit/{buID}/gettemplates", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Template"},
                            {"action", "GetByBU"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                 new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "workflow/template/{sfmcName}/getdetails", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Template"},
                            {"action", "GetDetails"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "workflow/campaign", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Campaign"},
                            {"action", "Index"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "workflow/campaign/get", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Campaign"},
                            {"action", "Get"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "workflow/campaign/save", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Campaign"},
                            {"action", "Save"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                 new RouteDescriptor {
                    Route = new Route(
                        "workflow/campaign/{campaignID}", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Campaign"},
                            {"action", "Edit"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "workflow/campaign/email-exists", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Campaign"},
                            {"action", "EmailExists"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "workflow/campaign/uisend-exists", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Campaign"},
                            {"action", "UISendExists"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                }
                ,
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "workflow/campaign/create-email", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Campaign"},
                            {"action", "CreateEmail"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Priority = 1,
                    Route = new Route(
                        "workflow/campaign/create-uisend", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Campaign"},
                            {"action", "CreateUISend"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                }
                ,
                 new RouteDescriptor {
                    Route = new Route(
                        "workflow/campaign-changestatus", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Campaign"},
                            {"action", "ChangeStatus"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Route = new Route(
                        "workflow/campaign-changeowner", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Campaign"},
                            {"action", "ChangeOwner"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Route = new Route(
                        "workflow/campaign-changejobids", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Campaign"},
                            {"action", "ChangeJobIDs"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Route = new Route(
                        "workflow/campaign-copy", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Campaign"},
                            {"action", "CopyCampaign"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                 new RouteDescriptor {
                    Route = new Route(
                        "workflow/brief/{campaignID}", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Brief"},
                            {"action", "ViewBrief"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                 new RouteDescriptor {
                    Route = new Route(
                        "workflow/campaign/{campaignID}", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Campaign"},
                            {"action", "Edit"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                 new RouteDescriptor {
                    Route = new Route(
                        "workflow/import", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Import"},
                            {"action", "Index"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                }
                 ,
                 new RouteDescriptor {
                    Route = new Route(
                        "workflow/ProcessImport", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Import"},
                            {"action", "ProcessImport"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                 new RouteDescriptor {
                    Route = new Route(
                        "workflow/calendar", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Calendar"},
                            {"action", "Index"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                 new RouteDescriptor {
                    Route = new Route(
                        "workflow/calendar/get-events", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Calendar"},
                            {"action", "GetEvents"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Route = new Route(
                        "workflow/campaign-feedback", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Campaign"},
                            {"action", "AddFeedback"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                }
                ,
                new RouteDescriptor {
                    Route = new Route(
                        "workflow/campaign-delete", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Campaign"},
                            {"action", "Delete"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Route = new Route(
                        "workflow/user/get-ops", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "User"},
                            {"action", "GetOpsUsers"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                 new RouteDescriptor {
                    Route = new Route(
                        "workflow/businessunit", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "BusinessUnit"},
                            {"action", "Get"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                }
                 ,
                 new RouteDescriptor {
                    Route = new Route(
                        "workflow/businessunit/getall", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "BusinessUnit"},
                            {"action", "GetAll"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                 new RouteDescriptor {
                    Route = new Route(
                        "workflow/businessunit/{buID}/getusers", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "User"},
                            {"action", "GetByBusinessUnit"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                },
                 new RouteDescriptor {
                    Route = new Route(
                        "workflow/businessunit/{buName}/getaudiences", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"}, // this is the name of your module
                            {"controller", "Audience"},
                            {"action", "GetByBusinessUnit"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "WorkflowTool"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                }
            };
        }
    }
}