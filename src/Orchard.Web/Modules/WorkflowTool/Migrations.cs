﻿using System;
using System.Collections.Generic;
using System.Data;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.MetaData;
using Orchard.ContentManagement.MetaData.Builders;
using Orchard.Core.Contents.Extensions;
using Orchard.Data.Migration;
using Orchard.Indexing;

namespace WorkflowTool
{
    public class Migrations : DataMigrationImpl
    {
        public int Create()
        {
            // Creating table DEManagerPartRecord
            SchemaBuilder.CreateTable("WorkflowToolTemplatePartRecord", table => table
                .ContentPartRecord()
                .Column("SFMCName", DbType.String)
                .Column("ThumbnailID", DbType.Int32)
                .Column("MapImageID", DbType.Int32)
                .Column("BusinessUnits", DbType.String)
            );
            return 1;
        }

        public int UpdateFrom1()
        {
            ContentDefinitionManager.AlterTypeDefinition("Workflow Tool Template", cfg => cfg
             .WithPart("TitlePart")
             .WithPart("CommonPart")
             .WithPart("WorkflowToolTemplatePart")
             .WithPart("PublishLaterPart")
             .Creatable()
             .Draftable()
             .Indexed());

            ContentDefinitionManager.AlterPartDefinition("WorkflowToolTemplatePart",
              builder => builder.Attachable());

            ContentDefinitionManager.AlterPartDefinition("WorkflowToolTemplatePart",
             builder => builder.WithField("ThumbnailImage",
                 fieldBuilder => fieldBuilder
                .OfType("MediaLibraryPickerField").WithDisplayName("Thumbnail Image")
                    .WithSetting("MediaLibraryPickerField.Hint", "Thumbnail to be display during template selection").WithSetting("MediaLibraryPickerFieldSettings.Required", "True")
                    .WithSetting("MediaLibraryPickerFieldSettings.Multiple", "False")));

            ContentDefinitionManager.AlterPartDefinition("WorkflowToolTemplatePart",
             builder => builder.WithField("MapImage",
                 fieldBuilder => fieldBuilder
                .OfType("MediaLibraryPickerField").WithDisplayName("Map Image")
                    .WithSetting("MediaLibraryPickerField.Hint", "Map Image to be display during template configuration").WithSetting("MediaLibraryPickerFieldSettings.Required", "True")
                    .WithSetting("MediaLibraryPickerFieldSettings.Multiple", "False")));

            return 2;
        }

        public int UpdateFrom2()
        {
            ContentDefinitionManager.AlterPartDefinition("WorkflowToolTemplatePart",
             builder => builder.WithField("BusinessUnit",
                 fieldBuilder => fieldBuilder
                .OfType("TaxonomyField").WithDisplayName("Business Unit(s)")
                    .WithSetting("TaxonomyFieldSettings.Hint", "Business Unit(s) that the template relates to")
                    .WithSetting("TaxonomyFieldSettings.Required", "True")
                    .WithSetting("TaxonomyFieldSettings.Taxonomy", "Fanatics Business Units")
                    .WithSetting("TaxonomyFieldSettings.LeavesOnly", "False")
                    .WithSetting("TaxonomyFieldSettings.SingleChoice", "False")
                    .WithSetting("TaxonomyFieldSettings.Autocomplete", "True")
                    .WithSetting("TaxonomyFieldSettings.AllowCustomTerms", "False")
                    ));


            return 3;
        }

        public int UpdateFrom3()
        {
            SchemaBuilder.AlterTable("WorkflowToolTemplatePartRecord",t=>t
                .AddColumn("Description",DbType.String));

            SchemaBuilder.AlterTable("WorkflowToolTemplatePartRecord",t=>t
                .AddColumn("Toggles",DbType.String));

            ContentDefinitionManager.AlterPartDefinition("WorkflowToolTemplatePart",
             builder => builder.WithField("AvailableToggles",
                 fieldBuilder => fieldBuilder
                .OfType("EnumerationField").WithDisplayName("Available Toggles")
                    .WithSetting("EnumerationFieldSettings.Hint", "Toggles that the user can choose from when selecting this template.")
                    .WithSetting("EnumerationFieldSettings.Required", "False")
                    .WithSetting("EnumerationFieldSettings.ListMode", "Checkbox")
                    .WithSetting("EnumerationFieldSettings.Options", "Picks for You&#xD;&#xA;Email Reqs 1&#xD;&#xA;Trending Products&#xD;&#xA;Dynamic Banners")
                    ));

            ContentDefinitionManager.AlterPartDefinition("WorkflowToolTemplatePart",
            builder => builder.WithField("TemplateDescription",
                fieldBuilder => fieldBuilder
               .OfType("TextField").WithDisplayName("Template Description")
                   .WithSetting("TextFieldSettings.Hint", "Description that will appear under the preview of the template")
                   .WithSetting("TextFieldSettings.Flavor", "Html")
                   .WithSetting("TextFieldSettings.Required", "False")
                   ));
            return 4;
        }

        public int UpdateFrom4() {
          

            return 5;
        }
    }
}