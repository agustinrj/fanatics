﻿using Orchard.ContentManagement;
using Orchard.Data;
using Orchard.Users.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkflowTool.Services
{
    public class UserService
    {
        public List<dynamic> GetByBusinessUnit(int buID, Orchard.IOrchardServices Services, ISessionLocator SessionLocator)
        {
            List<dynamic> users = new List<dynamic>();

            var session = SessionLocator.For(typeof(UserPartRecord));
            
            var r = session.CreateQuery("SELECT u.UserName, u.Id, u.Email " +
                            "FROM Orchard.Users.Models.UserPartRecord AS u, " +
                            "Orchard.Taxonomies.Models.TermContentItem AS t, " +
                            "Orchard.Roles.Models.UserRolesPartRecord AS r " +
                            "WHERE u.Id = t.TermsPartRecord.Id AND u.Id = r.UserId AND r.Role.Name = 'Workflow Campaign Owner' " +
                            "AND t.TermRecord.Id = "+buID).List();
            bool hasCurrent = false;
            foreach (var u in r)
            {
                if (hasCurrent == false && Services.WorkContext.CurrentUser.UserName == ((object[])(u))[0].ToString())
                    hasCurrent = true;

                users.Add(new { ID = ((object[])(u))[1], UserName = ((object[])(u))[0], UserEmail = ((object[])(u))[2], IsCurrent = Services.WorkContext.CurrentUser.UserName == ((object[])(u))[0].ToString() });
            }

            if (!hasCurrent)
                users.Add(new { ID = Services.WorkContext.CurrentUser.Id, UserName = Services.WorkContext.CurrentUser.UserName, UserEmail = Services.WorkContext.CurrentUser.Email, IsCurrent = true });

            return users;
        }

        public List<dynamic> GetOpsUsers(ISessionLocator SessionLocator, Orchard.IOrchardServices Services)
        {
            List<dynamic> users = new List<dynamic>();

            var session = SessionLocator.For(typeof(UserPartRecord));

            var r = session.CreateQuery("SELECT a.UserName, a.Email " +
                                        "FROM Orchard.Users.Models.UserPartRecord AS a, " +
                                        "Orchard.Roles.Models.UserRolesPartRecord AS b " +
                                        "WHERE a.Id = b.UserId AND b.Role.Name = 'CRM Ops'").List();
            foreach (var u in r)
                users.Add(new { UserName = ((object[])(u))[0], UserEmail = ((object[])(u))[1], CanCreateObjects = Services.WorkContext.CurrentUser.UserName == ((object[])(u))[0].ToString() });

            return users;
        }

        public bool UserHasAccessToBusinessUnit(int buID, string userName, ISessionLocator SessionLocator)
        {
            var session = SessionLocator.For(typeof(UserPartRecord));

            var r = session.CreateQuery("SELECT u.UserName, u.Id, u.Email " +
                            "FROM Orchard.Users.Models.UserPartRecord AS u, " +
                            "Orchard.Taxonomies.Models.TermContentItem AS t " +
                            "WHERE u.Id = t.TermsPartRecord.Id " +
                            "AND t.TermRecord.Id = " + buID + "AND u.UserName = '" + userName +"'").List();

            return r.Count > 0;
        }
    }
}