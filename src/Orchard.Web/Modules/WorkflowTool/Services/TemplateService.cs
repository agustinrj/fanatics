﻿using Orchard.ContentManagement;
using Orchard.MediaLibrary.Models;
using Orchard.Taxonomies.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WorkflowTool.Models;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using Newtonsoft.Json.Linq;


namespace WorkflowTool.Services
{
    public class TemplateService
    {
        public List<dynamic> GetByBU(int buID, IContentManager contentManager, ITaxonomyService Taxonomies)
        {
            List<dynamic> templatesData = new List<dynamic>();
            List<int> addedTemplates = new List<int>();

            var templates = contentManager.Query<WorkflowToolTemplatePart, WorkflowToolTemplatePartRecord>().Where(t => t.BusinessUnits.Contains(buID.ToString())).List();

            foreach (var template in templates)
            {
                // get template images
                var thumbnail = contentManager.Query<MediaPart, MediaPartRecord>().Where(m => m.Id == template.ThumbnailID).List().FirstOrDefault();
                var map = contentManager.Query<MediaPart, MediaPartRecord>().Where(m => m.Id == template.MapImageID).List().FirstOrDefault();

                if (!addedTemplates.Contains(template.Id))
                {
                    addedTemplates.Add(template.Id);
                    var temp = new
                    {
                        Name = template.ContentItem.Parts.Where(t=>t.GetType() == typeof(Orchard.Core.Title.Models.TitlePart)).FirstOrDefault().As<Orchard.Core.Title.Models.TitlePart>().Title,
                        ID = template.Id,
                        Thumbnail = thumbnail != null ? Path.Combine("/Media/Default",thumbnail.FolderPath, thumbnail.FileName) : "",
                        MapImage = map != null ? Path.Combine("/Media/Default", map.FolderPath, map.FileName) : "",
                        BusinesUnits = template.BusinessUnits,
                        SFMCName = template.SFMCName,
                        Description = template.Description,
                        Toggles = new List<dynamic>()
                    };
                    
                    foreach (var toggle in template.Toggles.Split(','))
                    {
                        if (!string.IsNullOrEmpty(toggle))
                            temp.Toggles.Add(new{Name=toggle});
                    }

                    templatesData.Add(temp);
                }
            }

            return templatesData;
        }

        public string GetMapByName(string templateName,IContentManager contentManager)
        {
            string mapPath = "";
            var templates = contentManager.Query<WorkflowToolTemplatePart, WorkflowToolTemplatePartRecord>().List();
            var template = templates.Where(temp => temp.ContentItem.Parts.Where(t => t.GetType() == typeof(Orchard.Core.Title.Models.TitlePart)).FirstOrDefault().As<Orchard.Core.Title.Models.TitlePart>().Title == templateName).FirstOrDefault();
            if (template != null)
            {
                var map = contentManager.Query<MediaPart, MediaPartRecord>().Where(m => m.Id == template.MapImageID).List().FirstOrDefault();
                mapPath = map != null ? Path.Combine("/Media/Default", map.FolderPath, map.FileName) : "";
            }
            return mapPath;
        }

        public dynamic GetDetails(string sfmcName)
        {
            Fanatics.Email.Controller.Email emailController = new Fanatics.Email.Controller.Email();
            var email = emailController.GetEmailByName(sfmcName,0,false);

            // parse body for content areas.
            var r = new Regex(@"<contents>[\s\S]*?<\/contents>");
            var xml = r.Match(email.HTMLBody).Value;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            string json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(doc).Replace("@","");
            
            return new { Name = email.Name, Subject = email.Subject, PreHeader = email.PreHeader, HTMLBody = email.HTMLBody.Replace(json,""),Spots = json };
        }
    }
}