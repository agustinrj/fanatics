﻿using Orchard.ContentManagement;
using Orchard.Data;
using Orchard.Taxonomies.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using WorkflowTool.Models;

namespace WorkflowTool.Services
{
    public class CampaignService
    {
        public List<Fanatics.Email.Model.Campaign> Get(int pageIndex, int pageSize, string sortDirection, string sortBy, string filterBy, string searchBy, string startDate, string endDate)
        {
           
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);
            string campaignsDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignsDE"].ToString();

            Fanatics.Email.Controller.Campaign campaignController = new Fanatics.Email.Controller.Campaign();
            return campaignController.Get(campaignsDE,mid, pageIndex, pageSize, sortDirection, sortBy, filterBy, searchBy, startDate, endDate);
        }

        public dynamic Save(Fanatics.Email.Model.Campaign campaign)
        {
           
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            Fanatics.Email.Controller.Campaign campaignController = new Fanatics.Email.Controller.Campaign();
            return campaignController.Save(mid,campaign);
        }

        public dynamic Delete(string campaignID, string lastModifiedBy)
        {
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            Fanatics.Email.Controller.Campaign campaignController = new Fanatics.Email.Controller.Campaign();
            return campaignController.Delete(mid, campaignID,lastModifiedBy);
        }

        public dynamic CopyCampaign(string campaignID, string campaignName)
        {
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            Fanatics.Email.Controller.Campaign campaignController = new Fanatics.Email.Controller.Campaign();
            return campaignController.Copy(mid,campaignID,campaignName);
        }

        public dynamic ChangeStatus(string campaignID, string status)
        {
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            Fanatics.Email.Controller.Campaign campaignController = new Fanatics.Email.Controller.Campaign();
            return campaignController.ChangeStatus(mid, campaignID,status);
        }

        public dynamic ChangeOwner(string campaignID, string owner,string ownerEmail)
        {
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            Fanatics.Email.Controller.Campaign campaignController = new Fanatics.Email.Controller.Campaign();
            return campaignController.ChangeOwner(mid, campaignID, owner,ownerEmail);
        }

        public dynamic ChangeJobIDs(string campaignID, string jobids)
        {
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            Fanatics.Email.Controller.Campaign campaignController = new Fanatics.Email.Controller.Campaign();
            return campaignController.ChangeJobIDs(mid, campaignID, jobids);
        }

        public Fanatics.Email.Model.Campaign GetDetails(string campaignID,IContentManager contentManager)
        {
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            Fanatics.Email.Controller.Campaign campaignController = new Fanatics.Email.Controller.Campaign();
            var c = campaignController.GetDetails(mid, campaignID);

            TemplateService templateService = new TemplateService();
            c.TemplateMap = templateService.GetMapByName(c.Template,contentManager);

            return c;
        }

        public dynamic AddFeedback(string campaignID,string feedback, Orchard.IOrchardServices services)
        {
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            Fanatics.Email.Controller.Campaign campaignController = new Fanatics.Email.Controller.Campaign();

            return campaignController.AddFeedback(mid, campaignID, services.WorkContext.CurrentUser.UserName, feedback);
        }

        /// <summary>
        /// returns whether the campaign has an associated SFMC object to it or not
        /// </summary>
        /// <param name="campaignID"></param>
        /// <returns></returns>
        public bool EmailExists(string campaignID)
        {
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            int newMID = 0;
            int.TryParse(ConfigurationManager.AppSettings["NewETClient"], out newMID);

            Fanatics.Email.Controller.Campaign campaignController = new Fanatics.Email.Controller.Campaign();

            return campaignController.HasSFMCObjects(mid, campaignID, "email");
        }

        /// <summary>
        /// returns whether the campaign has an associated SFMC object to it or not
        /// </summary>
        /// <param name="campaignID"></param>
        /// <returns></returns>
        public bool UISendExists(string campaignID)
        {
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            Fanatics.Email.Controller.Campaign campaignController = new Fanatics.Email.Controller.Campaign();

            return campaignController.HasSFMCObjects(mid, campaignID, "uisend");
        }

        /// <summary>
        /// Creates a User Initiated Send based on a request object
        /// </summary>
        /// <param name="request"></param>
        /// <returns>{Status = "OK"} or an execption</returns>
        public dynamic CreateUISend(Fanatics.Email.Model.EmailSendCreateRequest request)
        {
            Fanatics.Email.Controller.EmailSendDefinition uiSendController = new Fanatics.Email.Controller.EmailSendDefinition();
            return uiSendController.CreateFromCampaignRequest(request);
        }

        /// <summary>
        /// Creates an Email based on a request object
        /// </summary>
        /// <param name="request"></param>
        /// <returns>{Status = "OK"} or an execption</returns>
        public dynamic CreateEmail(Fanatics.Email.Model.EmailSendCreateRequest request)
        {
            Fanatics.Email.Controller.Email emailController = new Fanatics.Email.Controller.Email();
            return emailController.CreateFromCampaignRequest(request);
        }

        /// <summary>
        /// Main Import Service
        /// </summary>
        /// <param name="file"></param>
        /// <param name="taxonomies"></param>
        /// <param name="services"></param>
        /// <param name="sessionLocator"></param>
        /// <returns></returns>
        public CampaignImportResult Import(HttpPostedFileBase file,ITaxonomyService taxonomies, Orchard.IOrchardServices services, ISessionLocator sessionLocator)
        {
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            Fanatics.Email.Controller.Campaign campaignController = new Fanatics.Email.Controller.Campaign();
            Fanatics.Email.Controller.DataExtension deController = new Fanatics.Email.Controller.DataExtension();

            BusinessUnitService buService = new BusinessUnitService();
            UserService userService = new UserService();
            TemplateService templateService = new TemplateService();
            AudienceService audienceService = new AudienceService();

            var businessUnits = buService.Get(taxonomies,null,null);
            
            CampaignImportResult result = new CampaignImportResult();
            Fanatics.Email.Model.ImportResult apiImportResult = new Fanatics.Email.Model.ImportResult();
             
            using (var sr = new StreamReader(file.InputStream))
            {
                string line;
                int rowNumber = 1;
                // read first line with headers
                line = sr.ReadLine();
                List<string> campaignNames = new List<string>();
                List<Fanatics.Email.Model.EmailSendCreateRequest> requests = new List<Fanatics.Email.Model.EmailSendCreateRequest>();
                List<string> campaignErrors = new List<string>();

                while ((line = sr.ReadLine()) != null)
                {
                    campaignErrors = new List<string>();
                    rowNumber++;
                    var delimiters = new char[] { '\t' };
                    var columns = line.Split(delimiters);

                    if (columns.Count() > 20)
                        result.Errors.Add(string.Format("Line {0} - invalid column count.", rowNumber));
                    else {

                        var emailName = columns.Count() > 0 ? columns[0]: "";
                        var businessUnit = columns.Count() > 1 ? columns[1] : "";
                        var deploymentDate = columns.Count() > 2 ? columns[2] : "";
                        var subjectline = columns.Count() > 3 ? columns[3] : "";
                        var preheader = columns.Count() > 4 ? columns[4] : "";
                        var preheaderURL = columns.Count() > 5 ? columns[5] : "";
                        var assetsLocation = columns.Count() > 6 ? columns[6] : "";
                        var template = columns.Count() > 7 ? columns[7] : "";
                        var disclaimer = columns.Count() > 8 ? columns[8] : "";
                        var theme = columns.Count() > 9 ? columns[9] : "";
                        var cName = columns.Count() > 10 ? columns[10] : "";
                        var cNumber = columns.Count() > 11 ? columns[11] : "";
                        var audience = columns.Count() > 12 ? columns[12] : "";
                        var supression = columns.Count() > 13 ? columns[13] : "";
                        var createUI = columns.Count() > 14 ? columns[14] : "";
                        var createEmail = columns.Count() > 15 ? columns[15] : "";
                        var quickDeploy = columns.Count() > 16 ? columns[16] : "";
                        var createUIPerAudience = columns.Count() > 17 ? columns[17] : "";
                        var testEmail = columns.Count() > 18 ? columns[18] : "";
                        var isNewBU = columns.Count() > 19 ? columns[19] : "";

                        // validate required fields
                        if (string.IsNullOrEmpty(emailName))
                            campaignErrors.Add(string.Format("Line {0} - Email Name is Required.", rowNumber));

                        if (string.IsNullOrEmpty(businessUnit))
                            campaignErrors.Add(string.Format("Line {0} - Business Unit is Required.", rowNumber));
                        
                        // validate date parsing
                        var deploymentDateParse = new DateTime();
                        if(!DateTime.TryParse(deploymentDate,out deploymentDateParse))
                            campaignErrors.Add(string.Format("Line {0} - Invalid Deployment Date.", rowNumber));

                        // validate if campaign with that name exists
                        if(campaignController.NameExists(mid,emailName))
                            campaignErrors.Add(string.Format("Line {0} - a campaign named: {1} already exists.", rowNumber,emailName));

                        // validate that business unit exists
                        var bu = businessUnits.Where(c => c.Name.ToLower() == businessUnit.ToLower()).FirstOrDefault();
                        if (bu == null && businessUnit != "")
                            campaignErrors.Add(string.Format("Line {0} - Business Unit named: {1} does not exist.", rowNumber, businessUnit));

                        // check if user can create campaigns for that BU
                        if(businessUnit != "" && bu != null)
                        {
                            if(!userService.UserHasAccessToBusinessUnit(int.Parse(bu.ID), services.WorkContext.CurrentUser.UserName,sessionLocator))
                                campaignErrors.Add(string.Format("Line {0} - You do not have access to create campaigns for Business Unit: {1}.", rowNumber, businessUnit));
                        }

                        // check if Email name is twice in file
                        if (campaignNames.Contains(emailName.ToLower()))
                            campaignErrors.Add(string.Format("Line {0} - Email Name {1} is more than once in the file.", rowNumber, emailName));
                        else
                            campaignNames.Add(emailName.ToLower());

                        // if createUI or email are specified, validate that subjectline is present
                        if((createEmail.ToLower() == "true" || createUI.ToLower() == "true") && string.IsNullOrEmpty(subjectline))
                            campaignErrors.Add(string.Format("Line {0} - Subject Line is required to create UI or Email.", rowNumber));

                        // validate that cname and cnumber are present when create ui per audience is specified
                        if(createUI.ToLower() == "true" && createUIPerAudience.ToLower() == "true" && (string.IsNullOrEmpty(cName) || string.IsNullOrEmpty(cNumber)))
                            campaignErrors.Add(string.Format("Line {0} - CNAME and CNUMBER have to be specified to create UI per audience.", rowNumber));

                        // validate that audience is provided if UI sends is requested
                        if (createUI.ToLower() == "true" && string.IsNullOrEmpty(audience))
                            campaignErrors.Add(string.Format("Line {0} - Audience is required to create UI Send.", rowNumber));

                        // validate that bu has the necessary values that are needed to perform the import
                        if ((createUI.ToLower() == "true" || createEmail.ToLower() == "true") && bu != null && string.IsNullOrEmpty(bu.MID.ToString()))
                            campaignErrors.Add(string.Format("Line {0} - MID is required for Business Unit to create UI Send or Email Objects.", rowNumber));

                        if (createUI.ToLower() == "true" && bu != null && quickDeploy.ToLower() == "true" && string.IsNullOrEmpty(bu.QuickDeployFolderID.ToString()))
                            campaignErrors.Add(string.Format("Line {0} - Quick Deploy Folder ID is required for Business Unit to create UI Send for Quick Deploy.", rowNumber));

                        if (createUI.ToLower() == "true" && bu != null && quickDeploy.ToLower() != "true" &&  string.IsNullOrEmpty(bu.UISendsFolderId.ToString()))
                            campaignErrors.Add(string.Format("Line {0} - UI Send Folder ID is required for Business Unit to create UI Send.", rowNumber));

                        if (createEmail.ToLower() == "true" && bu != null && string.IsNullOrEmpty(bu.EmailsFolderId.ToString()))
                            campaignErrors.Add(string.Format("Line {0} - Email Folder ID is required for Business Unit to create Email.", rowNumber));

                        // set campaign object 
                        if (campaignErrors.Count == 0)
                        {
                            List<dynamic> templates = templateService.GetByBU(int.Parse(bu.ID.ToString()), services.ContentManager, taxonomies);
                            var templateObject = templates.Where(t => t.Name.ToLower() == template.ToLower()).FirstOrDefault();
                            
                            Fanatics.Email.Model.EmailSendCreateRequest request = new Fanatics.Email.Model.EmailSendCreateRequest();
                            request.ProcesssRequest = (createUI.ToLower() == "true" || createEmail.ToLower() == "true");
                            request.Overwrite = true;
                            request.EmailsFolderId = createEmail.ToLower() == "true" ? int.Parse(bu.EmailsFolderId.ToString()) : 0;
                            request.UISendsFolderId = createUI.ToLower() == "true" ? int.Parse(bu.UISendsFolderId.ToString()) : 0;
                            request.CreateQuickDeploy = quickDeploy.ToLower() == "true";
                            request.CreateUIPerAudience = createUIPerAudience.ToLower() == "true";
                            request.MID = int.Parse(bu.MID.ToString());
                            
                            Fanatics.Email.Model.Campaign campaign = new Fanatics.Email.Model.Campaign();
                            campaign.EmailName = emailName;
                            campaign.BusinessUnit = businessUnit;
                            campaign.Template = template;
                            campaign.Theme = theme;
                            campaign.SubjectLine = subjectline;
                            campaign.Preheader = preheader;
                            campaign.PreheaderURL = preheaderURL;
                            campaign.AssetsLocation = assetsLocation;
                            campaign.Disclaimer = disclaimer;
                            campaign.CreatedBy = services.WorkContext.CurrentUser.UserName;
                            campaign.DeploymentDate = deploymentDateParse;
                            campaign.AssetsDueDate = campaign.DeploymentDate.AddDays(bu.AssetsDueDateDaysOffset * -1);
                            campaign.QAApprovalDate = campaign.DeploymentDate.AddDays(bu.QAApprovalDateDaysOffset * -1);
                            campaign.TemplateSFMCName = templateObject != null ? templateObject.SFMCName : "" ;

                            // parse audiences
                            bool isNew = false;
                            bool.TryParse(isNewBU, out isNew);
                            List<Fanatics.Email.Model.Audience> audienceRows = audienceService.GetByBusinessUnit(bu.Name, isNew);

                            if (!string.IsNullOrEmpty(audience))
                            {
                                var audiences = audience.Split(',');
                                foreach (var a in audiences)
                                {
                                    var audienceFound = audienceRows.Where(au => au.Name.ToLower() == a.ToLower()).FirstOrDefault();
                                    if (audienceFound != null)
                                    {
                                        campaign.Audiences.Add(new Fanatics.Email.Model.Audience()
                                        {
                                            AudienceType = "DEAudience",
                                            Name = a,
                                            Value = audienceFound.Value
                                        });
                                    }
                                }
                            }

                            // parse supressions
                            if (!string.IsNullOrEmpty(supression))
                            {
                                var supressions = supression.Split(',');
                                foreach (var a in supressions)
                                {
                                    var audienceFound = audienceRows.Where(au => au.Name.ToLower() == a.ToLower()).FirstOrDefault();
                                    if (audienceFound != null)
                                    {
                                        campaign.Audiences.Add(new Fanatics.Email.Model.Audience()
                                        {
                                            AudienceType = "Supression",
                                            Name = a,
                                            Value = audienceFound.Value
                                        });
                                    }
                                }
                            }

                            request.Campaign = campaign;
                            requests.Add(request);
                        }
                        else
                            result.Errors.AddRange(campaignErrors);
                    }
                }

                if (requests.Count > 0)
                {
                    apiImportResult = campaignController.Import(mid, requests);
                    result.Errors.AddRange(apiImportResult.Errors);
                    result.ProcessedCount = apiImportResult.ProcessedCount;
                    result.OverallStatus = apiImportResult.Status;
                }      
            }

            return result;
        }
    }
}