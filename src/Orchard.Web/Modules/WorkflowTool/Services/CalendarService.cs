﻿using Orchard;
using Orchard.Data;
using Orchard.Taxonomies.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WorkflowTool.Services
{
    public class CalendarService
    {
        public List<dynamic> GetEvents(ITaxonomyService taxonomies, ISessionLocator SessionLocator, IOrchardServices services)
        {
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);
            string campaignsDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignsDE"].ToString();

            Fanatics.Email.Controller.Campaign campaignController = new Fanatics.Email.Controller.Campaign();
            BusinessUnitService buService = new BusinessUnitService();

            var campaigns = campaignController.GetActive(campaignsDE,mid);
            var bus = buService.Get(taxonomies, SessionLocator, services);
            var baseURL = HttpContext.Current.Request.Url.Scheme+"://"+ HttpContext.Current.Request.Url.Authority + "/workflow/campaign/{0}";
            var events = campaigns.Join(bus, c => c.BusinessUnit, b => b.Name, (c, b) => new {mid=b.MID, id = c.Id, title = c.EmailName, start = c.DeploymentDate, color = b.CalendarColor, url=String.Format(baseURL,c.Id) }).ToList<dynamic>();

            return events;
        } 
    }
}