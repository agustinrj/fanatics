﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkflowTool.Services
{
    public class ServiceFactory
    {
        private BusinessUnitService businessUnitService;
        private UserService userService;
        private TemplateService templateService;
        private CampaignService campaignService;
        private AudienceService audienceService;
        private CalendarService calendarService;

        public BusinessUnitService BusinessUnit
        {
            get {
                if (businessUnitService == null)
                    businessUnitService = new BusinessUnitService();
                return businessUnitService;
            }
        }

        public UserService User
        {
            get
            {
                if (userService == null)
                    userService = new UserService();
                return userService;
            }
        }

        public TemplateService Template
        {
            get
            {
                if (templateService == null)
                    templateService = new TemplateService();
                return templateService;
            }
        }

        public CampaignService CampaignService
        {
            get
            {
                if (campaignService == null)
                    campaignService = new CampaignService();
                return campaignService;
            }
        }

        public CalendarService CalendarService
        {
            get
            {
                if (calendarService == null)
                    calendarService = new CalendarService();
                return calendarService;
            }
        }

        public AudienceService AudienceService
        {
            get
            {
                if (audienceService == null)
                    audienceService = new AudienceService();
                return audienceService;
            }
        }
    }
}