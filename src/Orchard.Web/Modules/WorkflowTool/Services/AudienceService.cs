﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WorkflowTool.Services
{
    public class AudienceService
    {
        public List<Fanatics.Email.Model.Audience> GetByBusinessUnit(string buName, bool IsNewBU)
        {
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);
            string audienceDE = ConfigurationManager.AppSettings["WorkflowTool.AudienceDE"].ToString();

            Fanatics.Email.Controller.Audience audienceController = new Fanatics.Email.Controller.Audience();
            return audienceController.GetByBusinessUnit(audienceDE,mid,buName, IsNewBU);
        }
    }
}