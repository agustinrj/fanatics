﻿using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WorkflowTool.Models;

namespace WorkflowTool.Handlers
{
    public class WorkflowToolTemplateHandler : ContentHandler
    {
        public WorkflowToolTemplateHandler(IRepository<WorkflowToolTemplatePartRecord> repository)
        {
            Filters.Add(StorageFilter.For(repository));

            OnRemoved<WorkflowToolTemplatePart>((context, part) =>
            {
                repository.Delete(part.Record);
            });

        }
    } 
}