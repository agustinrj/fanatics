﻿using System.Collections.Generic;
using Orchard.Environment.Extensions.Models;
using Orchard.Security.Permissions;

namespace WorkflowTool
{
    public class Permissions : IPermissionProvider
    {
        public static readonly Permission AccessWorkflowTool = new Permission { Description = "Access Workflow Tool", Name = "AccessWorkflowTool" };
        public static readonly Permission WorkflowToolCampaignOwner = new Permission { Description = "Workflow Tool Campaign Owner", Name = "WorkflowToolCampaignOwner" };

        public virtual Feature Feature { get; set; }

        public IEnumerable<Permission> GetPermissions()
        {
            return new[] {
                AccessWorkflowTool,
                WorkflowToolCampaignOwner,
            };
        }

        public IEnumerable<PermissionStereotype> GetDefaultStereotypes()
        {
            return new[] {
                new PermissionStereotype {
                    Name = "Workflow Tool User",
                    Permissions = new[] {AccessWorkflowTool}
                },
                 new PermissionStereotype {
                    Name = "Workflow Campaign Owner",
                    Permissions = new[] { WorkflowToolCampaignOwner }
                },
            };
        }

    }
}