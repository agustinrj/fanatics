﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WorkflowTool.Models;

namespace WorkflowTool.Drivers
{
    public class WorkflowToolTemplateDriver : ContentPartDriver<WorkflowToolTemplatePart>
    {
        protected override DriverResult Display(
        WorkflowToolTemplatePart part, string displayType, dynamic shapeHelper)
        {
            return ContentShape("Parts_WorkflowToolTemplate",
                () => shapeHelper.Parts_WorkflowToolTemplate(
                    SFMCName: part.SFMCName,
                    ThumbnailID: part.ThumbnailID,
                    MapImageID: part.MapImageID,
                    BusinessUnits: part.BusinessUnits));
        }

        //GET
        protected override DriverResult Editor(WorkflowToolTemplatePart part, dynamic shapeHelper)
        {
            return ContentShape("Parts_WorkflowToolTemplate_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts/WorkflowToolTemplate",
                    Model: part,
                    Prefix: Prefix));
        }

        //POST
        protected override DriverResult Editor(
            WorkflowToolTemplatePart part, IUpdateModel updater, dynamic shapeHelper)
        {
            var request = ((((((Orchard.Core.Contents.Controllers.AdminController)(updater)).Services).WorkContext).HttpContext).Request);
            var thumbnailDummy = request.Params["WorkflowToolTemplatePart.ThumbnailImage.SelectedIds"];
            var mapImageDummy = request.Params["WorkflowToolTemplatePart.MapImage.SelectedIds"];
            var descriptionDummy = request.Params["WorkflowToolTemplatePart.TemplateDescription.Text"];
            var togglesDummy = request.Params["WorkflowToolTemplatePart.AvailableToggles.SelectedValues"];

            if (thumbnailDummy != null)
            {
                thumbnailDummy = thumbnailDummy.Replace(",", "");
                int thumbnailID = 0;
                int.TryParse(thumbnailDummy, out thumbnailID);
                part.ThumbnailID = thumbnailID;
            }

            if (mapImageDummy != null)
            {
                mapImageDummy = mapImageDummy.Replace(",", "");
                int mapImageID = 0;
                int.TryParse(mapImageDummy, out mapImageID);
                part.MapImageID = mapImageID;
            }

            part.Description = descriptionDummy != null ? descriptionDummy : "";
            part.Toggles = togglesDummy != null ? togglesDummy : "";

            var buKeys = request.Params.AllKeys.Where(k => k.Contains("WorkflowToolTemplatePart.BusinessUnit.Terms") && k.Contains("IsChecked")).Select(s => s.Replace("IsChecked", "Id")).ToList();
            List<String> buValues = new List<string>();

            foreach (var key in buKeys)
            {
                if (request.Params[key]!= null)
                    buValues.Add(request.Params[key]);
            }

            part.BusinessUnits = string.Join(",",buValues);

            updater.TryUpdateModel(part, Prefix, null, null);
            return Editor(part, shapeHelper);
        }
    }
}