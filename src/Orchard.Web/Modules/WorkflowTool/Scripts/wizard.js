﻿define(['jquery','handlebars','bootstrap','ckeditor','moment','fuelux','validate','underscore'], function ($) {
    var templates = [];
    var feedbackArray = [];
    var isLoading = false;

    //Register handlebars helpers
    RegisterHelpers();

    // initialize date pickers for new campaigns only
    if (campaignID == '') {
        $('#promo-start-date').datepicker({ allowPastDates: false });
        $('#promo-end-date').datepicker({ allowPastDates: false });
        $('#step1-deployment-date').datepicker({ allowPastDates: false });
    }

    // handles scroll for wizard nav
    $('.wizard-nav').on('affixed.bs.affix', function () {
        $('.wizard-nav').addClass('navbar-fixed-top');
    });

    $('.wizard-nav').on('affixed-top.bs.affix', function () {
        $('.wizard-nav').removeClass('navbar-fixed-top');
    });

    // Initialize Wizard
    $('#workflow-wizard').wizard();

    // Wizard change step validation
    $('.wizard-action').click(function (e) {
        var isValid = true;
        if ($(this).data('step') != 1)
        {
            if ($('#step1-bu-dropdown').selectlist('selectedItem').value == "") {
                showModal({ title: "Business Unit is required", message: "Please select the Business Unit for this campaign to continue", readonly: false });
                e.preventDefault();
                isValid = false;
            }

            if (!isValidDeploymentDate() && isValid) {
                showModal({ title: "Invalid Date", message: "Please enter a valid deployment date to continue", readonly: false });
                e.preventDefault();
                isValid = false;
            }
        }
        if ($(this).data('step') > 2) {
            if ($('.thumbnail.active').length == 0) {
                showModal({ title: "Template is required", message: "Please select a Template for this campaign to continue", readonly: false });
                e.preventDefault();
                isValid = false;
            }
            if($(this).data('step') == 5)
            {
                $('#brief').empty();
                var c = GetCampaignObject();
                var source = $("#brief-template").html();
                var template = Handlebars.compile(source);
                var html = template(JSON.parse(c.Campaign));
                $('#brief').append(html);
            }
        }

        if (isValid) {
            $('#workflow-wizard').wizard('selectedItem', {
                step: $(this).data('step')
            });

            $('.wizard-action').removeClass('btn-primary');
            $(this).toggleClass('btn-primary');
        }
    });

    //initialize editor
    CKEDITOR.replace('step1disclaimer');
    CKEDITOR.instances.step1disclaimer.config.disableNativeSpellChecker = false;
    CKEDITOR.instances.step1disclaimer.config.height = '150px';
    CKEDITOR.replace('step1additionalnotes');
    CKEDITOR.instances.step1additionalnotes.config.height = '150px';
    CKEDITOR.replace('step6feedback');

    // Set deployment time selector;
    LoadTimeSelector();

    // get business Units
    LoadBusinessUnits();

    // get ops owners
    LoadOpsOwners();

    // handles change event on dropdown
    $('#step1-bu-dropdown').on('changed.fu.selectlist', function (e, data) {
        // Load campaign owners and templates for selected business unit.
        if (data.value != "")
        {
            LoadBusinessUnitUsers(data.value);
            LoadBusinessUnitTemplates(data.value,null,true);
        }
        
    });
    
    // Campaign Submission
    $('#btn-save').click(function () {
     
        var $btn = $(this).button('loading');
        var hasErrors = false;

        if (!isValidDeploymentDate()) {
            showModal({ title: "Invalid Date", message: "Please enter a valid deployment date to continue", readonly: false });
            hasErrors = true;
        }
        
        if (hasErrors == false) {
            $('form').each(function () {
                if (!$(this).validate({ ignore: "" }).form()) {
                    hasErrors = true;
                }
            });
            if (hasErrors) {
                showModal({ title: "Template fields are required", message: "Some of the fields for the template you picked are required.", readonly: false });
                e.preventDefault();
            }
        }

        if ($('#step1-email-name').val() == "" && hasErrors == false) {
            showModal({ title: "Email Name is required", message: "Please enter an email name to continue", readonly: false });
            hasErrors = true;
        }

        if ($('#step1-bu-dropdown').selectlist('selectedItem').value == "" && hasErrors == false) {
            showModal({ title: "Business Unit is required", message: "Please select the Business Unit for this campaign to continue", readonly: false });
            hasErrors = true;
        }

        if (!hasErrors) {
            $.post("/workflow/campaign/save", GetCampaignObject(), function () { })
            .done(function (result) {
                if (result.Status.toLowerCase() == "ok") {
                    showModal({ title: "Campaign Succesfully Saved!", message: "Page will refresh in 1 second...", readonly: true });
                    setTimeout(function () { window.location.href = '/workflow/campaign/' + result.CampaignID }, 1000);
                }
                else {
                    if (result.Message != null)
                        showModal({ title: "Campaign Submission Error", message: "A Campaign with the same name already exists", readonly: false });
                    else
                        showModal({ title: "Campaign Submission Error", message: "There has been an error submitting your campaign. Please contact the system administrator.", readonly: false });
                }
            })
            .fail(function () {
                showModal({ title: "Campaign Submission Error", message: "There has been an error submitting your campaign. Please contact the system administrator.", readonly: false });
            })
            .always(function () {
                $btn.button('reset');
            });
        }
        else
            $btn.button('reset');
    });

    // Load existing campaign
    if (campaignID != '') {
        if (campaign == null) {
            showModal({ title: "Campaign not found", message: "You campaign was not found<br/>please go back to the homepage or contact the system administrator.", readonly: false });
            $('#promo-start-date').datepicker({ allowPastDates: false });
            $('#promo-end-date').datepicker({ allowPastDates: false });
            $('#step1-deployment-date').datepicker({ allowPastDates: false });
        }
        else
            LoadCampaign(campaign);
    }

    // Load existing step
    if (activeStep != '') {
        $('#workflow-wizard').wizard('selectedItem', {
            step: activeStep
        });

        if (activeStep == '5') {
            $('#brief').empty();
            var c = GetCampaignObject();
            var source = $("#brief-template").html();
            var template = Handlebars.compile(source);
            var html = template(JSON.parse(c.Campaign));
            $('#brief').append(html);
        }

        $('.wizard-action').removeClass('btn-primary');
        $(".wizard-action[data-step='" + activeStep + "']").toggleClass('btn-primary');
    }
  
   
    // campaign Feedback submission
    $('#btn-feedback').click(function (e) {
        e.preventDefault();
        var feedback = CKEDITOR.instances.step6feedback.getData();

        if (campaignID == '')
            showModal({ title: "Please save campaign", message: "Please save the Campaign before adding any feedback", readonly: false });
        else {
            if (feedback != "") {
                var $btn = $(this).button('loading');
                var campaignFeedback = {};
                campaignFeedback.CampaignID = campaignID;
                campaignFeedback.Feedback = feedback;
                campaignFeedback.__RequestVerificationToken = antiForgeryToken;

                $.post("/workflow/campaign-feedback", campaignFeedback, function () { })
                .done(function (result) {
                    if(result.Status.toLowerCase() == 'ok')
                    {
                        var what = Object.prototype.toString;
                        if (what.call(campaign.Feedback).toLowerCase().indexOf("array") < 0)
                            feedbackArray.push(campaign.Feedback);
                        else
                            feedbackArray = campaign.Feedback;

                        feedbackArray.push(result.Feedback);

                        $('.step6-feedback').empty();
                        var source = $('#step6-feedback-template').html();
                        var template = Handlebars.compile(source);
                        var html = template({ Feedback: feedbackArray });
                        $('.step6-feedback').append(html);

                        CKEDITOR.instances.step6feedback.setData('');
                    }
                    else
                        showModal({ title: "Feedback Submission Error", message: "There has been an error submitting your campaign. Please contact the system administrator.", readonly: false });
                })
                .fail(function () {
                    showModal({ title: "Feedback Submission Error", message: "There has been an error submitting your campaign. Please contact the system administrator.", readonly: false });
                })
                .always(function () {
                    $btn.button('reset');
                });
            }
        }
    });

    // create UI
    $('#btn-create-ui').click(function (e) {
      
        if ($('.chk-de:checked').length == 0)
            showModal({ title: "Create UI Send Error", message: "Please select at least one audience to continue", readonly: false });
        else {
            $('#create-ui-success').hide();
            $('#create-ui-audiences').empty();
            var c = GetCampaignObject();
            var source = $("#create-ui-audiences-template").html();
            var template = Handlebars.compile(source);
            var html = template(JSON.parse(c.Campaign));
            $('#create-ui-audiences').append(html);
            $('#create-ui-modal').modal('toggle');
        }
    });

    $('#btn-create-ui-submit').click(function (e) {

        if ($('#chk-ui-per-audience').hasClass('checked') && $('#step1-cname').val() == '' && $('#step1-cnumber').val() == '') {
            alert("CName and CNumber fields are required when choosing to create a UI Send per audience");
        }
        else {
            var $btn = $(this).button('loading');

            // check if campaign objects exist first.
            var post = {};
            post.campaignID = campaignID;
            post.__RequestVerificationToken = antiForgeryToken;

            $.post("/workflow/campaign/uisend-exists", post, function () { })
                .done(function (result) {
                    var overwrite = false;
                    var submitForm = true;

                    if (result) {
                        if (confirm("UI Sends for this campaign already exists.\n\nClick OK to Overwrite them or Cancel to abort this operation."))
                            overwrite = true;
                        else {
                            $('#create-ui-modal').modal('toggle');
                            $btn.button('reset');
                            submitForm = false;
                        }
                    }

                    if (submitForm) {
                        // get the request object
                        var request = GetEmailSendCreateRequest();
                        request.Overwrite = overwrite;
                        // submit!
                        $.post("/workflow/campaign/create-uisend", request, function () { })
                             .done(function (result) {
                                 if (result.Status == "OK") {
                                     $('#create-ui-success').fadeIn(function () { setTimeout(hideCreateUIModal, 3000); });
                                 }
                                 else
                                     alert(result.Message);
                             })
                             .fail(function (error) {
                                 alert("Unexpected error, please contact the system administrator");
                             })
                            .always(function () {
                                $btn.button('reset');
                            });
                    }
                })
                .fail(function () {
                    alert("Unexpected error, please contact the system administrator");
                    $btn.button('reset');
                });
        }
    });

    // Create Email functions
    $('#btn-create-email').click(function (e) {
        if ($('#step4-subject-line').val() == "")
            showModal({ title: "Subject line is required", message: "A subject line is required to create the email in SFMC", readonly: false });
        else {
            var $btn = $(this).button('loading');

            // check if email objects exist first.
            var post = {};
            post.campaignID = campaignID;
            post.__RequestVerificationToken = antiForgeryToken;

            $.post("/workflow/campaign/email-exists", post, function () { })
                 .done(function (result) {
                     var overwrite = false;
                     var submitForm = true;

                     if (result) {
                         if (confirm("The Email for this campaign already exist.\n\nClick OK to Overwrite it or Cancel to abort this operation."))
                             overwrite = true;
                         else {
                             $btn.button('reset');
                             submitForm = false;
                         }
                     }

                     if (submitForm) {
                         // get the request object
                         var request = GetEmailSendCreateRequest();
                         request.Overwrite = overwrite;

                         // submit!
                         $.post("/workflow/campaign/create-email", request, function () { })
                              .done(function (result) {
                                  if (result.Status == "OK") {
                                      showModal({ title: "Email creation complete", message: "Email succesfully created!", readonly: false });
                                  }
                                  else
                                      alert(result.Message);
                              })
                              .fail(function (error) {
                                  alert("Unexpected error, please contact the system administrator");
                              })
                             .always(function () {
                                 $btn.button('reset');
                             });
                     }
                 })
                 .fail(function () {
                     alert("Unexpected error, please contact the system administrator");
                     $btn.button('reset');
                 });
        }
        });
});

/*-------------------------------------------- Functions ----------------------------------------------*/

function GetEmailSendCreateRequest()
{
    var emailSendCreateRequest = {};
    var c = GetCampaignObject();
    emailSendCreateRequest.CampaignString = c.Campaign;
    emailSendCreateRequest.__RequestVerificationToken = antiForgeryToken;
    emailSendCreateRequest.MID = $('#step1-bu-dropdown').selectlist('selectedItem').mid;
    emailSendCreateRequest.IsNewBU = $('#step1-bu-dropdown').selectlist('selectedItem').isnew;
    emailSendCreateRequest.QuickDeployFolderId  = $('#step1-bu-dropdown').selectlist('selectedItem').quickdeployfolderid;
    emailSendCreateRequest.EmailsFolderId = $('#step1-bu-dropdown').selectlist('selectedItem').emailsfolderid;
    emailSendCreateRequest.UISendsFolderId = $('#step1-bu-dropdown').selectlist('selectedItem').uisendsfolderid;
    emailSendCreateRequest.Overwrite = false;
    emailSendCreateRequest.CreateQuickDeploy = $('#chk-quick-deploy').hasClass('checked');
    emailSendCreateRequest.CreateUIPerAudience = $('#chk-ui-per-audience').hasClass('checked');

    return emailSendCreateRequest;
}

function RegisterHelpers(){
    // handlebars helper to format dates
    window.Handlebars.registerHelper('formatDate', function (val, block) {
        var d = new Date(val);
        return d.getMonth() + 1 + "/" + d.getDate() + "/" + d.getFullYear() + " " + formatAMPM(d);
    });

    
    window.Handlebars.registerHelper('formatDateJSON', function (val, block) {
        var d = new Date(val.match(/\d+/)[0] * 1);
        return d.getMonth() + 1 + "/" + d.getDate() + "/" + d.getFullYear() + " " + formatAMPM(d);
    });

    // handlebars helper to print audiences in columns
    window.Handlebars.registerHelper('everyNth', function (context, every, options) {
        var fn = options.fn, inverse = options.inverse;
        var ret = "";
        if (context && context.length > 0) {
            for (var i = 0, j = context.length; i < j; i++) {
                var modZero = i % every === 0;
                ret = ret + fn(_.extend({}, context[i], {
                    isModZero: modZero,
                    isModZeroNotFirst: modZero && i > 0,
                    isLast: i === context.length - 1,
                    isMod36: i % 36 === 0
                }));
            }
        } else {
            ret = inverse(this);
        }
        return ret;
    });

    Handlebars.registerHelper("foreach", function (arr, options) {
        if (options.inverse && !arr.length)
            return options.inverse(this);

        return arr.map(function (item, index) {
            item.$index = index;
            item.$first = index === 0;
            item.$last = index === arr.length - 1;
            return options.fn(item);
        }).join('');
    });
}

// Loads campaign into User interface
function LoadCampaign(campaignObject)
{
    // main campaign attributes
    showModal({ title: "Please wait...", message: "Loading campaign", readonly: true });

    var d = new Date(campaignObject.DeploymentDate.match(/\d+/)[0] * 1);
    $('#step1-deployment-date').datepicker({ allowPastDates: false, date: d });
    $('#deployment-time input').val(formatAMPM(d));
    $('#step1-bu-dropdown').selectlist('selectByText', campaignObject.BusinessUnit);
    $('#step1-email-theme').val(campaignObject.Theme);
    //$('#step1-owner').selectlist('selectByText', campaignObject.OpsOwner);
    $('#step4-subject-line').val(campaignObject.SubjectLine); 
    $('#step4-preheader').val(campaignObject.Preheader);
    $('#step4-preheaderURL').val(campaignObject.PreheaderURL);
    $('#step1-assets-location').val(campaignObject.AssetsLocation);
    $('#step1-email-name').val(campaignObject.EmailName);
    $('#step1-cname').val(campaignObject.CName);
    $('#step1-cnumber').val(campaignObject.CNumber);

    CKEDITOR.instances.step1disclaimer.setData(campaignObject.Disclaimer);
    CKEDITOR.instances.step1additionalnotes.setData(campaignObject.Notes);
    $('#step1-job-id').val(campaignObject.JobIDs);
    $('#hid-status').val(campaignObject.Status);
    
    LoadBusinessUnitUsers($('#step1-bu-dropdown').selectlist('selectedItem').value);
    LoadBusinessUnitTemplates($('#step1-bu-dropdown').selectlist('selectedItem').value, campaign.Template, false);
    
    // Detect when each ajax operation completes so we perform the next one
    // This is a fallback because jquery does not support synchronous ajax anymore
    $(document).ajaxComplete(function (event, request, settings) {
        if(settings.url.indexOf('gettemplates') > 0)
        {
            setTimeout(function () {
                // set selected template
                var template = $(".thumbnail[data-name='" + campaign.Template + "']");
                template.addClass('active');

                // Load template details
                $('#step3-template').attr('src', template.data('map'));

                GetDetails(template.data('value'), template.data('index'), false);
            }, 300);
        }

        if (settings.url.indexOf('getdetails') > 0)
        {
            setTimeout(function () {
                // toggles
                $.each(campaignObject.Toggles, function (i, toggle) {
                    $(".chk-toggle[data-value='" + toggle + "']").prop('checked', true).parent().addClass('checked');
                });

                // template spots
                $.each(campaignObject.TemplateConfiguration, function (i, template) {
                    $('#' + template.ContentName).val(template.ContentValue);
                });
            }, 300);
        }

        if (settings.url.indexOf('getaudiences') > 0)
        {
            // audiences
            setTimeout(function () {
                $.each(campaignObject.Audiences, function (i, audience) {
                    switch (audience.AudienceType) {
                        case "DEAudience":
                            $(".chk-de[data-value='" + audience.Value + "']").prop('checked', true).parent().addClass('checked');
                            break;
                        case "PSID":
                            $(".chk-psid[data-value='" + audience.Value + "']").prop('checked', true).parent().addClass('checked');
                            break;
                        case "TeamID":
                            $(".chk-teamid[data-value='" + audience.Value + "']").prop('checked', true).parent().addClass('checked');
                            break;
                        case "Custom":
                            $('#custom-criteria-text').val(audience.Value);
                            break;
                        case "Exclusions":
                            $('#exclusions-text').val(audience.Value);
                            break;
                        case "Supression":
                            $(".chk-supression[data-value='" + audience.Value + "']").prop('checked', true).parent().addClass('checked');
                            break;
                    }
                });
            }, 300);
        }

        if (settings.url.indexOf('getusers') > 0)
        {
            // Campaign owners
            setTimeout(function () {
                $(".chk-user").removeAttr('checked').parent().removeClass('checked');

                $.each(campaignObject.CampaignOwners, function (i, user) {
                    $(".chk-user[data-username='" + user.UserName + "']").prop('checked', true).parent().addClass('checked');
                });
            }, 300);
        }
    });
     
    // promos
    $.each(campaignObject.Promos, function (i, promo) {
        $('#step1-email-promo').val(promo.PromoName);
        $('#step1-promo-code').val(promo.PromoCode);
        var startDate = new Date(promo.StartDate.match(/\d+/)[0] * 1);
        var endDate = new Date(promo.EndDate.match(/\d+/)[0] * 1);
        $('#promo-start-date').datepicker({ allowPastDates: false, date: startDate });
        $('#promo-end-date').datepicker({ allowPastDates: false, date: endDate });
    });

    if (campaignObject.Promos.length == 0) {
        $('#promo-start-date').datepicker({ allowPastDates: false });
        $('#promo-end-date').datepicker({ allowPastDates: false });
    }

    // feedback
    // fix for handlebars template and issue with arrays.
    var what = Object.prototype.toString;
    if (what.call(campaignObject.Feedback).toLowerCase().indexOf("array") < 0)
        feedbackArray.push(campaignObject.Feedback);
    else
        feedbackArray = campaignObject.Feedback;

    $('.step6-feedback').empty();
    var source = $('#step6-feedback-template').html();
    var template = Handlebars.compile(source);
    var html = template({ Feedback: feedbackArray });
    $('.step6-feedback').append(html);
    
}

// gets the campaign object for submission
function GetCampaignObject()
{
    // main Campaign Object
    var post = {};
    post.__RequestVerificationToken = antiForgeryToken;

    var campaignObject = {};
    var AssetsDueDate = new Date ($('#step1-deployment-date').datepicker('getDate'));
    var QAApprovalDate = new Date ($('#step1-deployment-date').datepicker('getDate'));

    var AssetsDueDateDaysOffset = $('#step1-bu-dropdown').selectlist('selectedItem').assetsduedatedaysoffset;
    var QAApprovalDateDaysOffset = $('#step1-bu-dropdown').selectlist('selectedItem').qaapprovaldatedaysoffset;

    AssetsDueDate.setDate(AssetsDueDate.getDate() - AssetsDueDateDaysOffset);
    QAApprovalDate.setDate(QAApprovalDate.getDate() - QAApprovalDateDaysOffset);

    var d =  $('#step1-deployment-date').datepicker('getDate');
    var DeploymentDate = new Date(d.getMonth() + 1 + "/" + d.getDate() + "/" + d.getFullYear() + " " + $('#deployment-time input').val());

    campaignObject.ID = campaignID;
    campaignObject.MessageType = $('#step1-message-type').selectlist('selectedItem').value;
    campaignObject.BusinessUnit = $('#step1-bu-dropdown').selectlist('selectedItem').name;
    campaignObject.Theme = $('#step1-email-theme').val();
    campaignObject.OpsOwner = $('#step1-owner').selectlist('selectedItem').value;
    campaignObject.OpsOwnerEmail = $('#step1-owner').selectlist('selectedItem').email;
    campaignObject.SubjectLine = $('#step4-subject-line').val();
    campaignObject.Preheader = $('#step4-preheader').val();
    campaignObject.PreheaderURL = $('#step4-preheaderURL').val();
    campaignObject.AssetsLocation = $('#step1-assets-location').val();
    campaignObject.EmailName = $('#step1-email-name').val();
    campaignObject.DeploymentDate = DeploymentDate.toJSON();
    campaignObject.CreatedBy = $('.chk-owner').data('username');
    campaignObject.Status = $('#hid-status').val();
    campaignObject.Disclaimer = CKEDITOR.instances.step1disclaimer.getData();
    campaignObject.JobIDs = $('#step1-job-id').val();
    campaignObject.LastModifiedBy = $('.chk-owner').data('username');
    campaignObject.Notes = CKEDITOR.instances.step1additionalnotes.getData();
    campaignObject.AssetsDueDate = AssetsDueDate.toJSON();
    campaignObject.QAApprovalDate = QAApprovalDate.toJSON();
    campaignObject.Template = $('.thumbnail.active').data('name');
    campaignObject.TemplateSFMCName = $('.thumbnail.active').data('value');
    campaignObject.TemplateMap = $('.thumbnail.active').data('map');
    campaignObject.CName = $('#step1-cname').val();
    campaignObject.CNumber = $('#step1-cnumber').val();
    // campaign Promos
    var promos = [];

    if ($('#step1-email-promo').val() != "")
    {
        var promo = {};
        promo.PromoName = $('#step1-email-promo').val();
        promo.PromoCode = $('#step1-promo-code').val()
        promo.StartDate = $('#promo-start-date').datepicker('getDate').toJSON();
        promo.EndDate = $('#promo-end-date').datepicker('getDate').toJSON();
        promos.push(promo);
    }

    campaignObject.Promos = promos;

    // template spots
    var templateConf = [];
    campaignObject.TemplateConfigurationJson = [];

    $.each($('.template-spot'), function (i, t) {
        var sectionName = $(this).data('name');
        var section = {};
        section.Section = sectionName;
        section.Fields = [];
        $.each($(this).find('.spot-field'), function (index, spot) {
            var conf = {};
            conf.Section = sectionName;
            conf.ContentName = $(this).attr('id');
            conf.ContentValue = $(this).val();

            if ($(this).val() != "") {
                templateConf.push(conf);
                section.Fields.push(conf);
            }
        });
        if (section.Fields.length > 0)
            campaignObject.TemplateConfigurationJson.push(section);
    });

    campaignObject.TemplateConfiguration = templateConf;
    
    // audiences
    campaignObject.Audiences = new Array();
    campaignObject.DEAudiences = [];
    campaignObject.Supressions = [];

    $.each($('.chk-de:checked'), function (i, c) {
        var de = {};
        de.FolderID = 0;
        de.Name = $(this).data('name');
        de.AudienceType = "DEAudience";
        de.Value = $(this).val();
        campaignObject.Audiences.push(de);
        campaignObject.DEAudiences.push({ Name: $(this).data('name'),Value: $(this).val() });
    });

    campaignObject.PSIDAudiences = [];
    $.each($('.chk-psid:checked'), function (i, c) {
        var psid = {};
        psid.FolderID = 0;
        psid.Name = "";
        psid.AudienceType = "PSID";
        psid.Value = $(this).val();
        campaignObject.Audiences.push(psid);
        campaignObject.PSIDAudiences.push({ Name: $(this).val() });
    });

    campaignObject.TeamIDAudiences = [];
    $.each($('.chk-teamid:checked'), function (i, c) {
        var teamid = {};
        teamid.FolderID = 0;
        teamid.Name = "";
        teamid.AudienceType = "TeamID";
        teamid.Value = $(this).val();
        campaignObject.Audiences.push(teamid);
        campaignObject.TeamIDAudiences.push({ Name: $(this).val() });
    });

    $.each($('.chk-supression:checked'), function (i, c) {
        var sup = {};
        sup.FolderID = 0;
        sup.Name = $(this).data('name');
        sup.AudienceType = "Supression";
        sup.Value = $(this).val();
        campaignObject.Audiences.push(sup);
        campaignObject.Supressions.push({ Name: $(this).data('name'), Value: $(this).val() });
    });

    if ($('#custom-criteria-text').val() != "")
    {
        var custom = {};
        custom.FolderID = 0;
        custom.Name = "";
        custom.AudienceType = "Custom";
        custom.Value = $('#custom-criteria-text').val();
        campaignObject.Audiences.push(custom);
        campaignObject.CustomCriteria = custom.Value;
    }

    if ($('#exclusions-text').val() != "") {
        var exclusions = {};
        exclusions.FolderID = 0;
        exclusions.Name = "";
        exclusions.AudienceType = "Exclusions";
        exclusions.Value = $('#exclusions-text').val();
        campaignObject.Audiences.push(exclusions);
        campaignObject.Exclusions = exclusions.Value;
    }

    // owners
    campaignObject.CampaignOwners = [];
    $.each($('.chk-user:checked'), function (i, user) {
        var owner = {};
        owner.UserName = $(this).data('username');
        owner.UserEmail = $(this).data('email');
        campaignObject.CampaignOwners.push(owner);
    });

    // toggles
    campaignObject.Toggles = [];
    $.each($('.chk-toggle:checked'), function (i, user) {
        campaignObject.Toggles.push($(this).val());
    });

    post.Campaign = JSON.stringify(campaignObject);

    return post;
}

// Populates Business Units
function LoadBusinessUnits()
{
    $.ajax({
        type: 'POST',
        url: "/workflow/businessunit",
        data: { __RequestVerificationToken: antiForgeryToken },
        dataType: 'json',
        async: false
    })
    .done(function (result) {
        var source = $("#step1-bus-template").html();
        var template = Handlebars.compile(source);
        var html = template({ bu: result.Data});
        $('.bu-dropdown').append(html);
        $('#step1-bu-dropdown').selectlist();
    })
    .fail(function () {
      alert("error");
    });
}

// Loads business Unit users by selected ID
function LoadBusinessUnitUsers(buID)
{
        var url = '/workflow/businessunit/' + buID + '/getusers';
        $.ajax({
            type: 'POST',
            url: url,
            data: { __RequestVerificationToken: antiForgeryToken },
            dataType: 'json'
        })
        .done(function (result) {
            $('.step1-campaign-owners').empty();
            var source = $('#step1-campaign-owners-template').html();
            var template = Handlebars.compile(source);
            var html = template({ user: result.Data });
            $('.step1-campaign-owners').append(html);
          
        })
        .fail(function () {
            alert("error");
        });
}

// Load ops owners
function LoadOpsOwners() {
    var url = '/workflow/user/get-ops';
    $.ajax({
        type: 'POST',
        url: url,
        data: { __RequestVerificationToken: antiForgeryToken },
        dataType: 'json'
    })
    .done(function (result) {
        $('#step1-owner').empty();
        var source = $('#step1-ops-owners-template').html();
        var template = Handlebars.compile(source);
        var html = template({ user: result.Data });
        $('#step1-owner').append(html);
        $('#step1-owner').selectlist();

        if(campaign.OpsOwner)
            $('#step1-owner').selectlist('selectByText', campaign.OpsOwner);

        $.each(result.Data, function (i, u) {
            if (u.CanCreateObjects && campaignID != '')
                $('#btn-create-ui,#btn-create-email').show();
        });
    })
    .fail(function () {
        alert("error");
    });
}

// Loads templates by selected business unit ID
function LoadBusinessUnitTemplates(buID,templateName,displayModal) {
    var url = '/workflow/businessunit/' + buID + '/gettemplates';

    if (displayModal)
        showModal({ title: "Please wait...", message: "Loading templates", readonly: true });

    setTimeout(function () {
        return $.ajax({
            type: 'POST',
            url: url,
            data: { __RequestVerificationToken: antiForgeryToken },
            dataType: 'json'
        })
        .done(function (result) {
            templates = result.Data;
            $('.step2-templates').empty();
            var source = $('#step2-templates-template').html();
            var template = Handlebars.compile(source);
            var html = template({ template: result.Data });
            $('.step2-templates').append(html);

            if (displayModal)
              hideModal();

            // handles template selection
            $('.template a').click(function () {
                $('.thumbnail').removeClass('active');
                $(this).find('.thumbnail').addClass('active');
                $('#step3-template').attr('src', $(this).find('.thumbnail').data('map'));
                return GetDetails($(this).find('.thumbnail').data('value'), $(this).find('.thumbnail').data('index'),true);
            });
        })
        .fail(function () {
            alert("error");
        });
    }, 300);
}

// Loads template details
function GetDetails(sfmcName, index, displayModal)
{
    if (sfmcName != null) {
        var t = templates[index];

        $('#step3-toggles').empty();
        var source = $('#step3-toggles-template').html();
        var template = Handlebars.compile(source);
        var html = template(t);
        $('#step3-toggles').append(html);

        var url = '/workflow/template/' + sfmcName + '/getdetails';

        if (displayModal)
            showModal({ title: "Please wait...", message: "Loading template details", readonly: true });

        setTimeout(function () {
            return $.ajax({
                type: 'POST',
                url: url,
                data: { __RequestVerificationToken: antiForgeryToken },
                dataType: 'json',
                async: false
            })
            .done(function (result) {
                hideModal();

                $('#accordion').empty();
                var source = $('#accordion-template').html();
                var template = Handlebars.compile(source);
                var templates = JSON.parse(result.Data.Spots).contents.content;
                var what = Object.prototype.toString;

                if (what.call(templates).toLowerCase().indexOf("array") < 0) {
                    var array = [];
                    array.push(templates);
                    templates = array;
                }

                $.each(templates, function (i, t) {
                    // fix for handlebars template and issue with arrays.
                    if (what.call(t.field).toLowerCase().indexOf("array") < 0) {
                        var array = [];
                        array.push(t.field);
                        templates[i].field = array;
                    }
                });

                var html = template({ content: templates });
                $('#accordion').append(html);

                GetAudiences($('#step1-bu-dropdown').selectlist('selectedItem').name);
            })
            .fail(function () {
                alert("error");
            });
        }, 300);
    }
    else
        hideModal();
}

// Gets Audiences by selected Business Unit
function GetAudiences(buName)
{
    var url = '/workflow/businessunit/'+buName+'/getaudiences';
    var isNew = false;
    if($('#step1-bu-dropdown').selectlist('selectedItem').isnew != "" && $('#step1-bu-dropdown').selectlist('selectedItem').isnew != null)
        isNew = $('#step1-bu-dropdown').selectlist('selectedItem').isnew;

    return $.ajax({
        type: 'POST',
        url: url,
        data: { __RequestVerificationToken: antiForgeryToken, IsNewBU: isNew },
        dataType: 'json',
        async: false
    })
    .done(function (result) {
        $('#step4-audiences').empty();
        var source = $('#step4-audiences-template').html();
        var template = Handlebars.compile(source);
        var html = template(result.Data);
        $('#step4-audiences').append(html);

        // handles check all items for audiences
        $('.chk-all input').on('change', function () {
            if (!$(this).parent().hasClass('checked'))
                $(this).parents('.form-group').siblings().find('.checkbox input').removeAttr('checked').parent().removeClass('checked');
            else
                $(this).parents('.form-group').siblings().find('.checkbox input').prop('checked', true).parent().addClass('checked');
        });

        $('.chk-audience input').on('change', function () {
            var checked = $(this).parents('.form-group').parent().find('.chk-audience input:checked').length;
            var total = $(this).parents('.form-group').parent().find('.chk-audience input').length;

            // check the check all button if we have checked all or not
            if (total == checked) {
                $(this).parents('.form-group').parent().find('.chk-all input').prop('checked', true);
                $(this).parents('.form-group').parent().find('.chk-all .checkbox-custom').addClass('checked');
            }
            else {
                $(this).parents('.form-group').parent().find('.chk-all input').prop('checked', false);
                $(this).parents('.form-group').parent().find('.chk-all .checkbox-custom').removeClass('checked');
            }
        });
    })
    .fail(function () {
        alert("error");
    });
}

// Modal helpers
function showModal(modalContent)
{
    $('#message-modal-content').empty();
    var source = $('#message-modal-template').html();
    var template = Handlebars.compile(source);
    var html = template(modalContent);
    $('#message-modal-content').append(html);
    $('#message-modal').modal('show');
}

function hideModal()
{
    $('#message-modal').modal('hide');
    $('.modal-backdrop').remove();
}

function hideCreateUIModal() {
    $('#create-ui-modal').modal('toggle')
}
// Modal helpers end

// Populates time options 
function LoadTimeSelector()
{
    var d = new Date();
    d.setHours(0);
    d.setMinutes(0);
    for (i = 0; i < 24; i++)
    {
        d.setHours(i);
        d.setMinutes(0);
        $('.deployment-time .dropdown-menu').append('<li data-value="'+formatAMPM(d)+'"><a href="#">'+formatAMPM(d)+'</a></li>');
        d.setMinutes(30);
        $('.deployment-time .dropdown-menu').append('<li data-value="'+formatAMPM(d)+'"><a href="#">'+formatAMPM(d)+'</a></li>');
    }
}

// Formats hours for time picker
function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

// validates deployment dates
function isValidDate(d) {
    if (Object.prototype.toString.call(d) !== "[object Date]")
        return false;
    return !isNaN(d.getTime());
}

function isValidDeploymentDate() {
    var d = $('#step1-deployment-date').datepicker('getDate');
    var DeploymentDate = new Date(d.getMonth() + 1 + "/" + d.getDate() + "/" + d.getFullYear() +" "+ $('#deployment-time input').val());
    return isValidDate(DeploymentDate);
}