﻿var eventsData;
var selectedBUs = [];
define(['jquery', 'handlebars', 'bootstrap', 'moment', 'fuelux', 'fullcalendar'], function ($) {
    LoadEvents();
    LoadBusinessUnits();
    $('#chk-all input').prop('checked', true);

    $('.dropdown-menu a').on('click', function (event) {

        var $target = $(event.currentTarget),
            val = $target.attr('data-value'),
            $inp = $target.find('input'),
            idx;

        if (!($target.hasClass('reset-filter'))) {
            if ((idx = selectedBUs.indexOf(val)) > -1) {
                selectedBUs.splice(idx, 1);
                setTimeout(function () { $inp.prop('checked', false) }, 0);
                setTimeout(function () { $('.bu-' + val).prop('checked', false) }, 0);
            } else {
                selectedBUs.push(val);
                $('#li-reset').show();
                setTimeout(function () { $inp.prop('checked', true) }, 0);
                setTimeout(function () { $('.bu-' + val).prop('checked', true) }, 0);
            }

            FindByBusinessUnit();
        }
        else
        {
            $('#li-reset').hide();
            selectedBUs = [];
            $('#calendar').fullCalendar('removeEvents');
            $('#calendar').fullCalendar('addEventSource', eventsData);

            $.each($('.dropdown-menu input'), function (i, bu) {
                $(bu).prop('checked', false);
            });
        }

        $(event.target).blur();
        return false;
    });
});

function LoadEvents() {
    var url = '/workflow/calendar/get-events';
    $.ajax({
        type: 'POST',
        url: url,
        data: { __RequestVerificationToken: antiForgeryToken },
        dataType: 'json'
    })
    .done(function (result) {
        eventsData = result.Data;
        $('#loader').hide();
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            editable: true,
            eventLimit: true, 
            events: result.Data,
            eventRender: function (event, element) {
                element.attr('title', event.title);
            }
        });

    })
    .fail(function () {
        alert("error");
    });
}

function LoadBusinessUnits() {
    $.ajax({
        type: 'POST',
        url: "/workflow/businessunit/getall",
        data: { __RequestVerificationToken: antiForgeryToken },
        dataType: 'json',
        async: false
    })
    .done(function (result) {
        var source = $("#bu-colors-template").html();
        var template = Handlebars.compile(source);
        var html = template({ bu: result.Data });
        $('.bu-dropdown').append(html);
        $('#bu-colors').selectlist();
        $('.dropdown-menu').css('width', '300px');
    })
    .fail(function () {
        alert("error");
    });
}

function FindByBusinessUnit()
{
    var results = [];
    $('#calendar').fullCalendar('removeEvents');

    $.each(selectedBUs, function (i, bu) {
        $.each(eventsData, function (i, event) {
            if (event.mid == parseInt(bu))
                results.push(event);
        });
    });
    $('#calendar').fullCalendar('addEventSource', results);
}