var dateChanged = false;

define(['jquery', 'handlebars','bootstrap', 'fuelux', 'moment','daterangepicker'], function ($) {
    var apiURL = '/workflow/campaign/';
    LoadBusinessUnits();
    LoadOpsOwners();
    var today = new Date();
    var tomorrow = new Date(today);
    tomorrow.setDate(today.getDate() + 1);
    var nextTwoWeeks = new Date(today);
    nextTwoWeeks.setDate(today.getDate() + 14);

    $('#deployment-date').daterangepicker({
        startDate: today,
        endDate: nextTwoWeeks,
        ranges: {
            'Today': [new Date(), new Date()],
            'Tomorrow': [tomorrow, tomorrow],
            'Next Two Weeks': [today, nextTwoWeeks]
        }
    });
  
    $('#deployment-date').on('apply.daterangepicker', function (ev, picker) {
        dateChanged = true;
        $('#main-grid').repeater('render');
    });

    // drop down menu
    $(".dropdown-menu li a").click(function () {
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
    });

    $("#btnSearch").click(function () {
        
    });

    function campaignsDataSource(options, callback) {
        // define the columns for the grid
        var columns = [
        {
            'label': 'View Campaign',
            'property': 'Id',
            'sortable': true,
            'width': 100
        },
        {
            'label': 'Business Unit',
            'property': 'BusinessUnit',
            'sortable': true,
            'width': 150
        },
        {
            'label': 'Deployment Date',
            'property': 'DeploymentDate',
            'sortable': true,
            'width': 160
        },
        {
            'label': 'Day of Week',
            'property': 'DayOfWeek',
            'sortable': true,
            'width': 70
        },
        //{
        //    'label': 'Week No.',
        //    'property': 'WeekNo',
        //    'sortable': true
        //},
        //{
        //    'label': 'Theme',
        //    'property': 'Theme',
        //    'sortable': true
        //},
        {
            'label': 'Email Name',
            'property': 'EmailName',
            'sortable': true,
            'width': 200
        },
        {
            'label': 'Status',
            'property': 'Status',
            'sortable': true,
            'width': 100
        },
        {
            'label': 'Ops Owner',
            'property': 'OpsOwner',
            'sortable': true,
            'width': 100
        },
        //{
        //    'label': 'JobID(s)',
        //    'property': 'JobIDs',
        //    'sortable': true
        //},
        {
            'label': 'Assets Due Date',
            'property': 'AssetsDueDate',
            'sortable': true,
            'width': 80
        },
        {
            'label': 'QA Approval Date',
            'property': 'QAApprovalDate',
            'sortable': true,
            'width': 80
        },
        {
            'label': 'Subject Line',
            'property': 'SubjectLine',
            'sortable': true,
            'width': 300
        },
        {
            'label': 'Preheader',
            'property': 'Preheader',
            'sortable': true,
            'width':300
        },
        {
            'label': 'View Brief',
            'property': 'ViewBrief',
            'sortable': false,
            'width': 80
        },
        {
            'label': 'Actions',
            'property': 'Actions',
            'sortable': false,
            'width': 80
        },
        ];

        // set options
        var pageIndex = options.pageIndex;
        var pageSize = options.pageSize;
        var options = {
            'pageIndex': pageIndex,
            'pageSize': pageSize,
            'sortDirection': options.sortDirection || 'asc',
            'sortBy': options.sortProperty || 'DeploymentDate',
            'filterBy': options.filter.value || '',
            'searchBy': options.search || '',
            'startDate': dateChanged ? GetFormattedDate($('#deployment-date').data('daterangepicker').startDate) : '',
            'endDate': dateChanged ? GetFormattedDate($('#deployment-date').data('daterangepicker').endDate) : ''
        };

        // call API, posting options
        $.ajax({
            'type': 'get',
            'url': apiURL + 'get',
            'data': options
        })
      .done(function (result) {

          var items = result.Data.items;
          var totalItems = result.Data.total;
          var totalPages = Math.ceil(totalItems / pageSize);
          var startIndex = (pageIndex * pageSize) + 1;
          var endIndex = (startIndex + pageSize) - 1;

          if (endIndex > items.length) {
              endIndex = items.length;
          }

          // configure datasource
          var dataSource = {
              'page': pageIndex,
              'pages': totalPages,
              'count': totalItems,
              'start': startIndex,
              'end': endIndex,
              'columns': columns,
              'items': items
          };

          // pass the datasource back to the repeater
          callback(dataSource);
      });
    }

    function GetFormattedDate(d) {
        return d.month() + 1 + "/" + d.date() + "/" + d.year();
    }

    function customColumnRenderer(helpers, callback) {
        // determine what column is being rendered
        var column = helpers.columnAttr;

        // get all the data for the entire row
        var rowData = helpers.rowData;
        var customMarkup = '';

        // only override the output for specific columns.
        // will default to output the text value of the row item
        switch (column) {
            case 'DeploymentDate':
                var d = new Date(rowData.DeploymentDate.match(/\d+/)[0] * 1);
                customMarkup = d.toLocaleDateString() + " " + d.toLocaleTimeString();
                break;
            case 'AssetsDueDate':
                var d = new Date(rowData.AssetsDueDate.match(/\d+/)[0] * 1);
                customMarkup = d.getMonth() + 1 + "/" + d.getDate() + "/" + d.getFullYear();
                break;
            case 'QAApprovalDate':
                var d = new Date(rowData.QAApprovalDate.match(/\d+/)[0] * 1);
                customMarkup = d.getMonth() + 1 + "/" + d.getDate() + "/" + d.getFullYear();
                break;
            case 'Id':
                var dropdownMarkup = '<div class="dropdown" style="font-size:10px"> \
                                       <button  class="btn btn-default dropdown-toggle" style="font-size:10px" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> \
                                        Go to... \
                                        <span class="caret"></span> \
                                      </button> \
                                      <ul class="dropdown-menu" style="position:relative;font-size:10px"> \
                                        <li><a href="/workflow/campaign/' + rowData.Id + '?step=1">1 - Campaign Basics</a></li> \
                                        <li><a href="/workflow/campaign/' + rowData.Id + '?step=2">2 - Template Selection</a></li> \
                                        <li><a href="/workflow/campaign/' + rowData.Id + '?step=3">3 - Audience Targeting</a></li> \
                                        <li><a href="/workflow/campaign/' + rowData.Id + '?step=4">4 - Campaign Brief</a></li> \
                                        <li><a href="/workflow/campaign/' + rowData.Id + '?step=5">5 - Final Brief</a></li> \
                                        <li><a href="/workflow/campaign/' + rowData.Id + '?step=6">6 - Feedback</a></li> \
                                      </ul> \
                                    </div>';

                customMarkup = dropdownMarkup;
                break;
            case 'ViewBrief':
                customMarkup = '<a href="/workflow/brief/' + rowData.Id + '">View Brief</a>';
                break;
            case 'Status':
                customMarkup = '<a href="Javascript:void(0)" onclick="changeStatus(\'' + rowData.Id + '\',\'' + rowData.Status + '\',\'' + rowData.EmailName + '\');">' + rowData.Status + '</a>';
                break;
            case 'OpsOwner':
                var owner = rowData.OpsOwner == "" ? "N/A" : rowData.OpsOwner;
                customMarkup = '<a href="Javascript:void(0)" onclick="changeOwner(\'' + rowData.Id + '\',\'' + owner + '\',\'' + rowData.EmailName + '\');">' + owner + '</a>';
                break;
            case 'JobIDs':
                var jobId = rowData.JobIDs == "" ? "N/A" : rowData.JobIDs;
                customMarkup = '<a href="Javascript:void(0)" onclick="changeJobIDs(\'' + rowData.Id + '\',\'' + jobId + '\',\'' + rowData.EmailName + '\');">' + jobId + '</a>';
                break;
            case 'Actions':
                customMarkup = '<p><a href="Javascript:void(0)" onclick="copy(\'' + rowData.Id + '\',\'' + rowData.EmailName + '\');">Copy</a></p> \
                                <p><a href="Javascript:void(0)" onclick="deleteCampaign(\'' + rowData.Id + '\',\'' + rowData.EmailName + '\');">Delete</a></p>';
                break;
            default:
                // otherwise, just use the existing text value
                customMarkup = helpers.item.text();
                break;
        }

        helpers.item.html(customMarkup);

        callback();
    }
    
    $('#main-grid').repeater({ list_columnRendered: customColumnRenderer, dataSource: campaignsDataSource, list_columnSizing: true });


    $('#btn-change-status').click(function () {
        var url = '/workflow/campaign-changestatus';
        var status = $('#status-select-list').selectlist('selectedItem').value;
        var campaignId = $(this).data('id');

        var $btn = $(this).button('loading');
        var post = {};
        post.__RequestVerificationToken = antiForgeryToken;
        post.CampaignID = campaignId;
        post.Status = status;

        $.post(url, post, function () { })
        .done(function (result) {
            $('#change-status-modal').modal('toggle');
            $('#main-grid').repeater('render');
        })
        .fail(function () {
            $('#change-status-modal').modal('toggle');
        })
        .always(function () {
            $btn.button('reset');
        });
    });

    $('#btn-change-owner').click(function () {
        var url = '/workflow/campaign-changeowner';
        var owner = $('#owner-select-list').selectlist('selectedItem').value;
        var email = $('#owner-select-list').selectlist('selectedItem').email;
        var campaignId = $(this).data('id');

        var $btn = $(this).button('loading');
        var post = {};
        post.__RequestVerificationToken = antiForgeryToken;
        post.CampaignID = campaignId;
        post.Owner = owner;
        post.OwnerEmail = email;

        $.post(url, post, function () { })
        .done(function (result) {
            $('#change-owner-modal').modal('toggle');
            $('#main-grid').repeater('render');
        })
        .fail(function () {
            $('#change-owner-modal').modal('toggle');
        })
        .always(function () {
            $btn.button('reset');
        });
    });

    $('#btn-change-jobids').click(function () {
        var url = '/workflow/campaign-changejobids';
        var campaignId = $(this).data('id');

        var $btn = $(this).button('loading');
        var post = {};
        post.__RequestVerificationToken = antiForgeryToken;
        post.CampaignID = campaignId;
        post.JobIDs = $('#jobids').val();
        
        $.post(url, post, function () { })
        .done(function (result) {
            $('#change-jobids-modal').modal('toggle');
            $('#main-grid').repeater('render');
        })
        .fail(function () {
            $('#change-jobids-modal').modal('toggle');
        })
        .always(function () {
            $btn.button('reset');
        });
    });

    $('#btn-copy').click(function () {
        $('#copy-error').hide();
        var url = '/workflow/campaign-copy';
        var campaignId = $(this).data('id');
        var $btn = $(this).button('loading');

        var post = {};
        post.__RequestVerificationToken = antiForgeryToken;
        post.CampaignID = campaignId;
        post.campaignName = $('#campaign-name').val();

        $.post(url, post, function () { })
        .done(function (result) {
            if (result.Message != null)
                $('#copy-error').show();
            else {
                $('#copy-modal').modal('toggle');
                $('#main-grid').repeater('render');
            }
        })
        .fail(function () {
            $('#copy-modal').modal('toggle');
        })
        .always(function () {
            $btn.button('reset');
        });
    });

    $('#btn-delete').click(function () {
        var url = '/workflow/campaign-delete';
        var campaignId = $(this).data('id');
        var $btn = $(this).button('loading');

        var post = {};
        post.__RequestVerificationToken = antiForgeryToken;
        post.campaignID = campaignId;
    
        $.post(url, post, function () { })
        .done(function (result) {
                $('#delete-modal').modal('toggle');
                $('#main-grid').repeater('render');
        })
        .fail(function () {
            $('#delete-modal').modal('toggle');
        })
        .always(function () {
            $btn.button('reset');
        });
    });
});


function changeOwner(id, owner, emailName) {
    $('#btn-change-owner').data('id', id);
    $('#change-owner-message').text('Change ops owner for campaign ' + emailName + ':');
    $('#owner-select-list').selectlist('selectByText', owner);
    $('#change-owner-modal').modal('toggle');
}

function changeStatus(id, status, emailName) {
    $('#btn-change-status').data('id', id);
    $('#change-status-message').text('Change status of campaign ' + emailName + ':');
    $('#status-select-list').selectlist('selectByText', status);
    $('#change-status-modal').modal('toggle');

}

function changeJobIDs(id, jobid, emailName) {
    $('#btn-change-jobids').data('id', id);
    $('#change-jobids-message').text('Change Job ID(s) for campaign ' + emailName + ':');
    $('#jobids').val(jobid);
    $('#change-jobids-modal').modal('toggle');
}

function copy(id,emailName) {
    $('#btn-copy').data('id', id);
    $('#copy-message').text('Choose a new name for campaign ' + emailName + ':');
    $('#campaign-name').val('');
    $('#copy-modal').modal('toggle');
}

function deleteCampaign(id, emailName) {
    $('#btn-delete').data('id', id);
    $('#delete-message').text('Are you sure you would like to delete ' + emailName + '?');
    $('#delete-modal').modal('toggle');
}

function LoadBusinessUnits() {
    $.ajax({
        type: 'POST',
        url: "/workflow/businessunit",
        data: { __RequestVerificationToken: antiForgeryToken },
        dataType: 'json',
        async: false
    })
    .done(function (result) {
        var source = $("#home-bus-template").html();
        var template = Handlebars.compile(source);
        var html = template({ bu: result.Data });
        $('.bu-dropdown').append(html);
        $('#home-bu-dropdown').selectlist();
    })
    .fail(function () {
        alert("error");
    });
}


// Load ops owners
function LoadOpsOwners() {
    var url = '/workflow/user/get-ops';
    $.ajax({
        type: 'POST',
        url: url,
        data: { __RequestVerificationToken: antiForgeryToken },
        dataType: 'json'
    })
    .done(function (result) {
        $('#owner-select-list').empty();
        var source = $('#owner-select-list-template').html();
        var template = Handlebars.compile(source);
        var html = template({ user: result.Data });
        $('#owner-select-list').append(html);
        $('#owner-select-list').selectlist();
    })
    .fail(function () {
        alert("error");
    });
}