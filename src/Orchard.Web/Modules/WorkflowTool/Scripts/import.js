﻿define(['jquery', 'handlebars', 'bootstrap'], function ($) {
    $('#btn-import').click(function (e) {
        e.preventDefault();
        $('#error').hide();
        $(this).button('loading');

        var files = $('#file').get(0).files;

        if (files.length == 0 || files == null)
        {
            $('#error').text('Please upload a file to continue');
            $('#error').show();
            $(this).button('reset');
        }
        else
            $('form').submit();
    });
});