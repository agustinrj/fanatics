﻿requirejs.config({
    paths: {
        'jquery': '//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min',
        'wizard': '/Modules/WorkflowTool/Scripts/wizard.js?v=1',
        'brief': '/Modules/WorkflowTool/Scripts/brief.js?v=1',
        'import': '/Modules/WorkflowTool/Scripts/import.js?v=1',
        'calendar': '/Modules/WorkflowTool/Scripts/calendar.js?v=1',
        'main': '/Modules/WorkflowTool/Scripts/main.js?v=1',
        'handlebars': '/Modules/WorkflowTool/Scripts/vendor/handlebars/handlebars',
        'bootstrap': '//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min',
        'ckeditor': '/Modules/WorkflowTool/Scripts/vendor/ckeditor/ckeditor',
        'moment': '/Modules/WorkflowTool/Scripts/vendor/moment/moment',
        'fuelux': '//www.fuelcdn.com/fuelux/3.6.3/js/fuelux.min',
        'validate': '//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min',
        'underscore': '/Modules/WorkflowTool/Scripts/vendor/underscore/underscore',
        'daterangepicker': '/Modules/WorkflowTool/Scripts/vendor/daterangepicker/daterangepicker',
        'fullcalendar': '/Modules/WorkflowTool/Scripts/vendor/fullcalendar/fullcalendar.min'
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'bootstrap': {
            deps: ['jquery'],
            exports: '$.fn.popover'
        },
        'validate': {
            deps: ['jquery']
        },
        'fuelux': {
            deps: ['jquery']
        }
    }
});