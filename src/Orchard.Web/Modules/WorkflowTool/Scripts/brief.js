﻿define(['jquery','handlebars','bootstrap','ckeditor','moment','fuelux','validate'], function ($) {
    if (campaignID != '') {
        if (campaign == null)
            window.location.href = '/workflow';
    }
    
    RegisterHelpers();

        $('#brief').empty();
        var source = $("#brief-template").html();
        var template = Handlebars.compile(source);
        var html = template(formatCampaignObject(campaign));
        $('#brief').append(html);
    
       
});


function RegisterHelpers() {
    // handlebars helper to format dates
    window.Handlebars.registerHelper('formatDate', function (val, block) {
        var d = new Date(val.match(/\d+/)[0] * 1);
        return d.getMonth() + 1 + "/" + d.getDate() + "/" + d.getFullYear() + " " + formatAMPM(d);
    });

    // handlebars helper to print audiences in columns
    window.Handlebars.registerHelper('everyNth', function (context, every, options) {
        var fn = options.fn, inverse = options.inverse;
        var ret = "";
        if (context && context.length > 0) {
            for (var i = 0, j = context.length; i < j; i++) {
                var modZero = i % every === 0;
                ret = ret + fn(_.extend({}, context[i], {
                    isModZero: modZero,
                    isModZeroNotFirst: modZero && i > 0,
                    isLast: i === context.length - 1,
                    isMod36: i % 36 === 0
                }));
            }
        } else {
            ret = inverse(this);
        }
        return ret;
    });

    Handlebars.registerHelper("foreach", function (arr, options) {
        if (options.inverse && !arr.length)
            return options.inverse(this);

        return arr.map(function (item, index) {
            item.$index = index;
            item.$first = index === 0;
            item.$last = index === arr.length - 1;
            return options.fn(item);
        }).join('');
    });
}

// formats the campaign object to display in the brief screen
function formatCampaignObject(campaignObject)
{
    var newCampaignObject = campaignObject;
    newCampaignObject.TemplateConfigurationJson = [];
    newCampaignObject.DEAudiences = [];
    newCampaignObject.PSIDAudiences = [];
    newCampaignObject.TeamIDAudiences = [];
    newCampaignObject.CustomCriteria = "";
    newCampaignObject.Exclusions = "";

    $.each(campaignObject.Audiences, function (i, a) {
        switch (a.AudienceType) {
            case "DEAudience":
                var de = {};
                de.Name = a.Name;
                newCampaignObject.DEAudiences.push(de);
                break;
            case "PSID":
                var psid = {};
                psid.Name = a.Value;
                newCampaignObject.PSIDAudiences.push(psid);
                break;
            case "TeamID":
                var teamid = {};
                teamid.Name = a.Value;
                newCampaignObject.TeamIDAudiences.push(teamid);
                break;
            case "Custom":
                newCampaignObject.CustomCriteria = a.Value;
                break;
            case "Exclusions":
                newCampaignObject.Exclusions = a.Value;
                break;
            case "Supression":
                var sup = {};
                sup.Name = a.Name;
                newCampaignObject.Supressions.push(sup);
                break;
        }
    });

    // auxiliary array to check if we already added that section
    var arr = [];
    // iterate through each template section
    $.each(campaignObject.TemplateConfiguration, function (i, t) {
        var template = {};
        template.Section = t.Section;
        template.Fields = [];

        // populate fields in array
        $.each(campaignObject.TemplateConfiguration, function (i2, t2) {
            var field = {};
            if (template.Section == t2.Section) {
                field.ContentName = t2.ContentName;
                field.ContentValue = t2.ContentValue;
                template.Fields.push(field);
            }
        });

        if ($.inArray(t.Section, arr) < 0) {
            arr.push(t.Section);
            newCampaignObject.TemplateConfigurationJson.push(template);
        }
    });

    return newCampaignObject;
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}