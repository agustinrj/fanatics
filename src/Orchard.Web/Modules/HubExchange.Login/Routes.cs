﻿using Orchard.Mvc.Routes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HubExchange.Login
{
    public class Routes : IRouteProvider
    {
        public void GetRoutes(ICollection<RouteDescriptor> routes)
        {
            foreach (var routeDescriptor in GetRoutes())
                routes.Add(routeDescriptor);
        }

        public IEnumerable<RouteDescriptor> GetRoutes()
        {
            return new[] {
                new RouteDescriptor {
                    Priority = 5,
                    Route = new Route(
                        "Login", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "HubExchange.Login"}, // this is the name of your module
                            {"controller", "Login"},
                            {"action", "Login"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "HubExchange.Login"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                }
            };
        }
    }
}