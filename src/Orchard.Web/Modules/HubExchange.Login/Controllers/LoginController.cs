﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orchard.Themes;
using System.Web.Mvc;
using Fanatics.Email.Controller.Core;
using Orchard.Security;
using Orchard;
using Orchard.Users.Models;
using Orchard.Users.Events;
using Orchard.ContentManagement;
using Fanatics.Email.Model.HubExchange;

namespace HubExchange.Login.Controllers
{
    
    public class LoginController: Controller {

        private readonly IContentManager _contentManager;
        private readonly IAuthenticationService _authenticationService;
        private readonly IUserEventHandler _userEventHandler;

        private ControllerFactory _controllers;

        public ControllerFactory Controllers
        {
            get
            {
                if (_controllers == null)
                    _controllers = new ControllerFactory();
                return _controllers;
            }
        }

        public LoginController(IContentManager contentManager,
            IAuthenticationService authenticationService,
            IUserEventHandler userEventHandler) {

            _contentManager = contentManager;
            _authenticationService = authenticationService;
            _userEventHandler = userEventHandler;
        }

        public ActionResult Login(string jwt)
        {
            try
            {
                System.Web.Helpers.AntiForgeryConfig.SuppressXFrameOptionsHeader = true;

                if (jwt.Trim().Length > 0)
                {
                    var response = Controllers.HubExchange.Login(jwt.Trim());

                    var user = _contentManager.Query<UserPart, UserPartRecord>().Where(u => u.NormalizedUserName == SessionManager.UserName).List().FirstOrDefault();

                    // TODO, if user is null, redirect user to not auth page.
                    if(user == null)
                        return new RedirectResult("~/not-authorized");

                    _authenticationService.SignIn(user, true);
                    _userEventHandler.LoggedIn(user);
                    
                    // Redirect to the application redirectURL specified in App Center.
                    //return new RedirectResult(response.request.application.redirectUrl);
                    return new RedirectResult("https://pages.exacttarget.com/page.aspx?QS=3935619f7de112efe001b0ec41fe1ce5cf00fe8ef5d3904b21392953132ebb44&oauthToken=" + Session["oauthToken"]);
                }

                return new RedirectResult(Request.Url.ToString());

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}