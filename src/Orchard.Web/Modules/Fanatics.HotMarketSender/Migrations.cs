﻿using System;
using System.Collections.Generic;
using System.Data;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.MetaData;
using Orchard.ContentManagement.MetaData.Builders;
using Orchard.Core.Contents.Extensions;
using Orchard.Data.Migration;
using Orchard.Indexing;

namespace Fanatics.HotMarketSender
{
    public class Migrations : DataMigrationImpl
    {
        public int Create()
        {
            // Creating table SchedulePartRecord
            SchemaBuilder.CreateTable("SchedulePartRecord", table => table
                .ContentPartRecord()
                .Column("League", DbType.String)
                .Column("Week", DbType.Int32)
                .Column("GameID", DbType.String)
                .Column("Game", DbType.String)
                .Column("BusinessUnitID", DbType.Int32)
                .Column("Active", DbType.Boolean)
                .Column("ScheduledUI", DbType.String)
                .Column("ScheduledUITaskID", DbType.String)
                .Column("FinalScore", DbType.String)
                .Column("TeamID1", DbType.String)
                .Column("TeamID2", DbType.String)
                .Column("UISendTeamID1", DbType.String)
                .Column("UISendTeamID2", DbType.String)
            );
            return 1;
        }

        public int UpdateFrom1()
        {
            SchemaBuilder.AlterTable("SchedulePartRecord", t => t
                 .AddColumn("APIAttempts", DbType.Int32));

            SchemaBuilder.AlterTable("SchedulePartRecord", t => t
                 .AddColumn("ResultCollectedDate", DbType.DateTime));

            return 2;
        }

        public int UpdateFrom2()
        {
            SchemaBuilder.AlterTable("SchedulePartRecord", t => t
                 .AddColumn("GameEndDate", DbType.DateTime));

            return 3;
        }

        public int UpdateFrom3()
        {
            SchemaBuilder.CreateTable("Authentication", table => table
              .ContentPartRecord()
              .Column("TokenID", DbType.String)
              .Column("ExpirationDate", DbType.DateTime));

              return 4;
        }


        public int UpdateFrom4()
        {
            SchemaBuilder.AlterTable("SchedulePartRecord", t => t
                 .AddColumn("UISendStatus", DbType.String));
         
            return 5;
        }

        public int UpdateFrom5()
        {
            SchemaBuilder.AlterTable("SchedulePartRecord", t => t
                 .AddColumn("IsNewBU", DbType.Boolean));

            return 6;
        }
    }
}