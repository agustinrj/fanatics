﻿define(['jquery', 'handlebars', 'bootstrap', 'moment', 'fuelux', 'validate', 'underscore'], function ($) {

    function scheduleDataSource(options, callback) {
        // define the columns for the grid
        var columns = [
        {
            'label': 'League',
            'property': 'League',
            'sortable': true
        },
        {
            'label': 'Week',
            'property': 'Week',
            'sortable': true
        },
        {
            'label': 'Game',
            'property': 'Game',
            'sortable': false
        },
        {
            'label': 'Actions',
            'property': 'Actions',
            'sortable': false
        }
        ];

        // set options
        var pageIndex = options.pageIndex;
        var pageSize = options.pageSize;
        var options = {
            'pageIndex': pageIndex,
            'pageSize': pageSize,
            'sortDirection': options.sortDirection || 'asc',
            'sortBy': options.sortProperty || '',
            'searchBy': options.search || ''
        };

        // call API, posting options
        $.ajax({
            'type': 'get',
            'url': '/hotmarketsender/getschedules',
            'data': options
        })
      .done(function (result) {

          var items = result.Data.items;
          var totalItems = result.Data.total;
          var totalPages = Math.ceil(totalItems / pageSize);
          var startIndex = (pageIndex * pageSize) + 1;
          var endIndex = (startIndex + pageSize) - 1;

          if (endIndex > items.length) {
              endIndex = items.length;
          }

          // configure datasource
          var dataSource = {
              'page': pageIndex,
              'pages': totalPages,
              'count': totalItems,
              'start': startIndex,
              'end': endIndex,
              'columns': columns,
              'items': items
          };

          // pass the datasource back to the repeater
          callback(dataSource);
      });
    }

    function customColumnRenderer(helpers, callback) {
        // determine what column is being rendered
        var column = helpers.columnAttr;

        // get all the data for the entire row
        var rowData = helpers.rowData;
        var customMarkup = '';

        // only override the output for specific columns.
        // will default to output the text value of the row item
        switch (column) {
            case 'Game':
                customMarkup = rowData.Home + ' - ' + rowData.Away;
                break;
            case 'Actions':
                customMarkup = '<p><a href="/hotmarketsender/edit/' + rowData.GameId + '">Edit</a>&nbsp; \
                                <a href="Javascript:void(0)" onclick="deleteSchedule(\'' + rowData.GameId + '\');">Delete</a></p>';
                break;
            default:
                // otherwise, just use the existing text value
                customMarkup = helpers.item.text();
                break;
        }

        helpers.item.html(customMarkup);

        callback();
    }

    $('#main-grid').repeater({ list_columnRendered: customColumnRenderer, dataSource: scheduleDataSource });
});

function deleteSchedule(gameID)
{
    if(confirm('Are you sure you would like to remove this schedule?'))
    {
        var post = {};
        post.gameID = gameID;
        post.__RequestVerificationToken = antiForgeryToken;
   
        $.post("/hotmarketsender/delete-schedule", post, function () { })
        .done(function (result) {
            if (result.Status.toLowerCase() == 'ok') {
                showModal({ title: "Delete Schedule", message: "Schedule succesfully deleted", readonly: false });
                $('#main-grid').repeater('render');
            }
            else
                showModal({ title: "Delete Schedule", message: "There has been an error processing your request. Please contact the system administrator.", readonly: false });
            })
            .fail(function () {
                showModal({ title: "Delete Schedule", message: "There has been an error processing your request. Please contact the system administrator.", readonly: false });
            });
    }
}

// Modal helpers
function showModal(modalContent) {
    $('#message-modal-content').empty();
    var source = $('#message-modal-template').html();
    var template = Handlebars.compile(source);
    var html = template(modalContent);
    $('#message-modal-content').append(html);
    $('#message-modal').modal('show');
}

function hideModal() {
    $('#message-modal').modal('hide');
    $('.modal-backdrop').remove();
}
