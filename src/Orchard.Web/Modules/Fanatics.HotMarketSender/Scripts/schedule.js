﻿var schedule, selectedLeague, firstLoad;
define(['jquery', 'handlebars', 'bootstrap', 'moment', 'fuelux', 'validate', 'underscore'], function ($) {

    RegisterHandlebarsHelpers();
    LoadBusinessUnits();

    // events
    $('#league').change(function (data, handler) {
        if ($("#league option:selected").val() != "" && $("#league option:selected").val() != selectedLeague) {
            $('#week').empty();
            $('#game').empty();
            LoadSchedule($("#league option:selected").val());
        }
    });

    $('#week').change(function (data, handler) {
        if ($("#week option:selected").val() != "") {
            LoadGames(parseInt($("#week option:selected").val()));
            $('#game-panel').show();
        }
    });

    $('#game').change(function (data, handler) {
        $('#bu-panel').show();
        if ($("#game option:selected").val() != "" && scheduleModel == null) {
            var post = {};
            post.gameID = $("#game option:selected").val();
            post.__RequestVerificationToken = antiForgeryToken;

            // check if there's not an schedule already for that game and ask the user if the'd like to navigate to that schedule.
           $.post("/hotmarketsender/getbygameid", post, function () { })
           .done(function (result) {
               if (result.Status.toLowerCase() == 'ok') {
                   if (result.Data != null) {
                       var message = "<p>An schedule for this game was already created.</p>"
                       message += '<p><a href="/hotmarketsender/edit/' + $("#game option:selected").val() + '"> Click here</a> to navigate to that schedule or close this modal to continue.</p>';
                       message += '<p><strong>Important Note: </strong> Saving will overwrite any schedule associated with this game</p>'
                       showModal({ title: "Create/Edit Schedule", message: message, readonly: false });
                   }
               }
           });
        }
    });

    $('#btn-save').click(function () {        
        SaveSchedule();
    });
});

/*-------------------------------------------- Functions ----------------------------------------------*/
function Load()
{
    if(scheduleModel != null)
    {
        firstLoad = true;
        $("#league").val(scheduleModel.League);
        $('#league').change();
    }
}

function LoadUIs() {
    if (scheduleModel != null) {
        $.each(scheduleModel.UISends, function (i, ui) {
            if ($('#' + ui.BusinessUnit).length > 0) {
                $('#' + ui.BusinessUnit).addClass('checked');
                $('#' + ui.BusinessUnit + ' label').addClass('checked');
                $('#' + ui.BusinessUnit + ' input').attr('checked', 'checked');
                AddRemoveConfigSection(true, ui.BusinessUnit, $('#' + ui.BusinessUnit).data('name'));
                $('#bu-' + ui.BusinessUnit).find('.home').val(ui.Team1UISend);
                $('#bu-' + ui.BusinessUnit).find('.away').val(ui.Team2UISend);
            }
        });
    }
}

function LoadSchedule(league)
{
    selectedLeague = league;

    var post = {};
    post.league = league;
    post.__RequestVerificationToken = antiForgeryToken;

    $.post("/hotmarketsender/sportradar/getschedule", post, function () { })
    .done(function (result) {
        if (result.Status.toLowerCase() == 'ok') {
            schedule = JSON.parse(result.Data);
            $('#week-panel').show();
            LoadWeeks();
        }
        else
            showModal({ title: "Schedule retrieval error", message: "There has been an error retrieving schedules. Please contact the system administrator.", readonly: false });
    })
    .fail(function () {
        showModal({ title: "Schedule retrieval error", message: "There has been an error retrieving schedules. Please contact the system administrator.", readonly: false });
    });
}

function SaveSchedule() {
    var $btn = $('#btn-save').button('loading');
    $('#btn-cancel').toggleClass('disabled');

    var post = {};

    post.League = selectedLeague;
    post.GameOffset = $("#league option:selected").data('gameoffset');
    post.GameStartDate = $("#game option:selected").data('scheduled');
    post.Week = parseInt($("#week option:selected").val());
    post.GameId = $("#game option:selected").val();
    post.Home = $("#game option:selected").data('home');
    post.Away = $("#game option:selected").data('away');
    
    post.UISends  = [];
    $.each($('.chk-bu:checked'), function (i, bu) {
        var uiSend = {};
        var mid = parseInt($(this).val());
        var isNew = $(this).data("isnew");
        var $bu = $('#bu-' + mid);
        uiSend.BusinessUnit = mid;
        uiSend.IsNewBU = isNew;
        uiSend.Team1UISend = $bu.find('.home').val();
        uiSend.Team2UISend = $bu.find('.away').val();
        post.UISends.push(uiSend);
    });

    post.UISendsString = JSON.stringify(post.UISends);
    post.__RequestVerificationToken = antiForgeryToken;

    $.post("/hotmarketsender/update", post, function () { })
   .done(function (result) {
       if (result.Errors.length == 0)
           showModal({ title: "Create/Edit Schedule", message: "Scheduled successfully saved!", readonly: false });
       else {
           var ul = 'Errors occurred:<ul>';
           $.each(result.Errors, function (i, error) {
               ul += '<li>' + error + '</li>';
           });

           ul += '</ul>';
           showModal({ title: "Create/Edit Schedule", message: ul, readonly: false });
       }
   })
   .fail(function () {
       showModal({ title: "Schedule retrieval error", message: "There has been an error retrieving schedules. Please contact the system administrator.", readonly: false });
   })
    .always(function () {
        $('#btn-cancel').toggleClass('disabled');
        $btn.button('reset');
    });
}

function LoadWeeks() {
    $('#week').empty();
    var source = $('#week-template').html();
    var template = Handlebars.compile(source);
    var html = template(schedule);
    $('#week').append(html);

    if(firstLoad && scheduleModel != null)
    {
        $('#week select').val(scheduleModel.Week);
        $('#week select').change();
    }
}

function LoadGames(weekNumber)
{
    $('#game').empty();
    var games = schedule.weeks[weekNumber-1].games;
    var source = $('#game-template').html();
    var template = Handlebars.compile(source);
    var html;

    switch (selectedLeague.toLowerCase()) {
        case "nfl":
            html = template({ nflgames: games});
            break;
        case "ncaafb": 
            html = template({ ncaafbgames: games });
            break;
    }
    $('#game').append(html);

    if (firstLoad && scheduleModel != null) {
        $('#game select').val(scheduleModel.GameId);
        $('#game select').change();

        LoadUIs();
    }
}

function LoadBusinessUnits() {
   
    var post = {};
    post.__RequestVerificationToken = antiForgeryToken;

    $.post("/hotmarketsender/businessunit", post, function () { })
    .done(function (result) {
        if (result.Status.toLowerCase() == 'ok') {
            var source = $("#business-units-template").html();
            var template = Handlebars.compile(source);
            var html = template({ bu: result.Data });
            $('#business-units').append(html);
            
            // apend click event to checkbox
            $('.checkbox').click(function () {
                AddRemoveConfigSection($(this).hasClass('checked'), $(this).attr('id'), $(this).data('name'));
            });

            // if we are on edit mode, we load the schedule
            Load();
        }
        else
            showModal({ title: "BU retrieval error", message: "There has been an error retrieving Business Units. Please contact the system administrator.", readonly: false });
    })
    .fail(function () {
        showModal({ title: "BU retrieval error", message: "There has been an error retrieving Business Units. Please contact the system administrator.", readonly: false });
    });
}

// Adds or removes a configuration section based on the business unit selected
function AddRemoveConfigSection(add,mid,buName)
{
    if (!add)
        $('#bu-config #bu-' + mid).remove();
    else
    {
        var conf = {};
        conf.MID = mid;
        conf.buName = buName;
        conf.home = $("#game option:selected").data('home');
        conf.away = $("#game option:selected").data('away');

        var source = $('#bu-config-template').html();
        var template = Handlebars.compile(source);
        var html = template(conf);
        $('#bu-config').append(html);
    }
}

// registers handlebars helpers
function RegisterHandlebarsHelpers() {
    window.Handlebars.registerHelper("inc", function (value, options) {
        return parseInt(value) + 1;
    });
}

// Modal helpers
function showModal(modalContent) {
    $('#message-modal-content').empty();
    var source = $('#message-modal-template').html();
    var template = Handlebars.compile(source);
    var html = template(modalContent);
    $('#message-modal-content').append(html);
    $('#message-modal').modal('show');
}

function hideModal() {
    $('#message-modal').modal('hide');
    $('.modal-backdrop').remove();
}


