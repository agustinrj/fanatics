﻿requirejs.config({
    paths: {
        'jquery': '//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min',
        'schedule': '/Modules/Fanatics.HotmarketSender/Scripts/schedule',
        'schedule-index': '/Modules/Fanatics.HotmarketSender/Scripts/schedule-index',
        'sends': '/Modules/Fanatics.HotmarketSender/Scripts/sends',
        'handlebars': '/Modules/Fanatics.HotmarketSender/Scripts/vendor/handlebars/handlebars',
        'bootstrap': '//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min',
        'moment': '/Modules/Fanatics.HotmarketSender/Scripts/vendor/moment/moment',
        'fuelux': '//www.fuelcdn.com/fuelux/3.6.3/js/fuelux.min',
        'validate': '//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min',
        'underscore': '/Modules/Fanatics.HotmarketSender/Scripts/vendor/underscore/underscore'
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'bootstrap': {
            deps: ['jquery'],
            exports: '$.fn.popover'
        },
        'validate': {
            deps: ['jquery']
        },
        'fuelux': {
            deps: ['jquery']
        }
    }
});