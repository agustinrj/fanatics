﻿define(['jquery', 'handlebars', 'bootstrap', 'moment', 'fuelux', 'validate', 'underscore'], function ($) {
    $('#btn-cancel').click(function () {
        if($('.chk-ui:checked').length > 0)
        {
            var url = '/hotmarketsender/cancel-sends';
            var $btn = $(this).button('loading');
            var post = {};
            post.__RequestVerificationToken = antiForgeryToken;
            post.Schedules = [];
            var schedules = [];
            $.each($('.chk-ui:checked'), function (i, ui) {
                var schedule = {};
                schedule.Id = $(this).val();
                schedule.ScheduledUI = $(this).data('ui');
                schedule.BusinessUnitID = $(this).data('mid');
                schedule.IsNewBU = $(this).data('isnew');
                schedule.ScheduledUITaskID = $(this).data('taskid');
                schedules.push(schedule);
            });

            post.Schedules = JSON.stringify(schedules);

            $.post(url, post, function () { })
           .done(function (result) {
               $('#message').text('');
               $('#message').empty();

               if (result.Errors.length == 0)
                   $('#message').text('Sends successfully canceled');
               else
               {
                   var ul = 'Errors occurred:<ul>';
                   $.each(result.Errors, function (i, error) {
                       ul += '<li>' + error + '</li>';
                   });

                   ul += '</ul>';
                   $('#message').append(ul);
               }

               $('#confirmation-modal').modal({ backdrop: 'static' });
           })
           .fail(function () {
               $('#message').text('Could not cancel sends, please contact the system administrator');
               $('#confirmation-modal').modal({ backdrop: 'static' });
           })
           .always(function () {
               $btn.button('reset');
           });

        }
    });
});

function hoverFix() {
    var el = this;
    var par = el.parentNode;
    var next = el.nextSibling;
    par.removeChild(el);
    setTimeout(function () { par.insertBefore(el, next); }, 0)
}