﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fanatics.HotMarketSender.Models
{
    public class ScheduledSends
    {
        public bool ValidToken { get; set; }
        public List<SchedulePartRecord> Schedules { get; set; }
    }
}