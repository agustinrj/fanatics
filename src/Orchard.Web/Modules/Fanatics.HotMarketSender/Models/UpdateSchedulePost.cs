﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotMarketSender.Models
{
  
    public class UpdateSchedulePost
    {
        public string League { get; set; }
        public int Week { get; set; }
        public int GameOffset { get; set; }
        public string GameId { get; set; }
        public string Home { get; set; }
        public string Away { get; set; }
        public DateTime GameStartDate { get; set; }
        public List<UISend> UISends { get; set; }
        public string UISendsString { get; set; }
        public UpdateSchedulePost()
        {
            UISends = new List<UISend>();
        }
    }

    public class UISend
    {
        public int BusinessUnit { get; set; }
        public bool IsNewBU { get; set; }
        public string Team1UISend { get; set; }
        public string Team2UISend { get; set; }
    }
}