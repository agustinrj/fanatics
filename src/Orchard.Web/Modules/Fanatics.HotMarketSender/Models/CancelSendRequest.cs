﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fanatics.HotMarketSender.Models
{
    public class CancelSendRequest
    {
        public int Id { get; set; }
        public string ScheduledUI { get; set; }
    }
}