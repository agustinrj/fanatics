﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Records;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fanatics.HotMarketSender.Models
{
    public class Authentication:ContentPartRecord
    {
        public virtual string TokenID { get; set; }
        public virtual DateTime ExpirationDate { get; set; }
    }

    public class AuthenticationPart : ContentPart<Authentication>
    {
        [Required]
        public string TokenID
        {
            get { return Retrieve(r => r.TokenID); }
            set { Store(r => r.TokenID, value); }
        }

        [Required]
        public DateTime ExpirationDate
        {
            get { return Retrieve(r => r.ExpirationDate); }
            set { Store(r => r.ExpirationDate, value); }
        }
    }
}