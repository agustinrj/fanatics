﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Records;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Fanatics.HotMarketSender.Models
{
    public class SchedulePartRecord : ContentPartRecord
    {
        public virtual string League { get; set; }
        public virtual int Week { get; set; }
        public virtual string GameID { get; set; }
        public virtual string Game { get; set; }
        public virtual int BusinessUnitID { get; set; }
        public virtual bool Active { get; set; }
        public virtual string ScheduledUI { get; set; }
        public virtual string ScheduledUITaskID { get; set; }
        public virtual string FinalScore { get; set; }
        public virtual string TeamID1 { get; set; }
        public virtual string TeamID2 { get; set; }
        public virtual string UISendTeamID1 { get; set; }
        public virtual string UISendTeamID2 { get; set; }
        public virtual string UISendStatus { get; set; }
        public virtual bool IsNewBU { get; set; }
        public virtual int APIAttempts { get; set; }
        public virtual DateTime ResultCollectedDate { get; set; }
        public virtual DateTime GameEndDate { get; set; }
    }

    public class SchedulePart : ContentPart<SchedulePartRecord>
    {
        [Required]
        public string League
        {
            get { return Retrieve(r => r.League); }
            set { Store(r => r.League, value); }
        }

        [Required]
        public int Week
        {
            get { return Retrieve(r => r.Week); }
            set { Store(r => r.Week, value); }
        }

        [Required]
        public string GameID
        {
            get { return Retrieve(r => r.GameID); }
            set { Store(r => r.GameID, value); }
        }

        [Required]
        public string Game
        {
            get { return Retrieve(r => r.Game); }
            set { Store(r => r.Game, value); }
        }

        [Required]
        public int BusinessUnitID
        {
            get { return Retrieve(r => r.BusinessUnitID); }
            set { Store(r => r.BusinessUnitID, value); }
        }

        [Required]
        public bool Active
        {
            get { return Retrieve(r => r.Active); }
            set { Store(r => r.Active, value); }
        }

        [Required]
        public string ScheduledUI
        {
            get { return Retrieve(r => r.ScheduledUI); }
            set { Store(r => r.ScheduledUI, value); }
        }

        [Required]
        public string ScheduledUITaskID
        {
            get { return Retrieve(r => r.ScheduledUITaskID); }
            set { Store(r => r.ScheduledUITaskID, value); }
        }

        [Required]
        public string FinalScore
        {
            get { return Retrieve(r => r.FinalScore); }
            set { Store(r => r.FinalScore, value); }
        }

        [Required]
        public string TeamID1
        {
            get { return Retrieve(r => r.TeamID1); }
            set { Store(r => r.TeamID1, value); }
        }

        [Required]
        public string TeamID2
        {
            get { return Retrieve(r => r.TeamID2); }
            set { Store(r => r.TeamID2, value); }
        }

        [Required]
        public string UISendTeamID1
        {
            get { return Retrieve(r => r.UISendTeamID1); }
            set { Store(r => r.UISendTeamID1, value); }
        }

        [Required]
        public string UISendTeamID2
        {
            get { return Retrieve(r => r.UISendTeamID2); }
            set { Store(r => r.UISendTeamID2, value); }
        }

        public string UISendStatus
        {
            get { return Retrieve(r => r.UISendStatus); }
            set { Store(r => r.UISendStatus, value); }
        }

        public bool IsNewBU
        {
            get { return Retrieve(r => r.IsNewBU); }
            set { Store(r => r.IsNewBU, value); }
        }

        public int APIAttempts
        {
            get { return Retrieve(r => r.APIAttempts); }
            set { Store(r => r.APIAttempts, value); }
        }

        public DateTime ResultCollectedDate
        {
            get { return Retrieve(r => r.ResultCollectedDate); }
            set { Store(r => r.ResultCollectedDate, value); }
        }

        public DateTime GameEndDate
        {
            get { return Retrieve(r => r.GameEndDate); }
            set { Store(r => r.GameEndDate, value); }
        }
    }
}