﻿using Fanatics.HotMarketSender.Models;
using Orchard.ContentManagement;
using Orchard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fanatics.HotMarketSender.Services
{
    public class SchedulesService
    {
        public ScheduledSends GetScheduledSends(ISessionLocator SessionLocator,IContentManager contentManager,string token)
        {
            ScheduledSends sends = new ScheduledSends();

            // first authenticate the token
            List<dynamic> users = new List<dynamic>();

            var session = SessionLocator.For(typeof(Authentication));

            var tokenID = session.CreateQuery("SELECT TokenID " +
                           "FROM HotMarketSender.Models.Authentication h " +
                           "WHERE h.TokenID = '" + token+"' AND ExpirationDate = '"+DateTime.Today.ToShortDateString()+"'").UniqueResult();

            sends.ValidToken = tokenID != null;

            if (sends.ValidToken)
            {
                session = SessionLocator.For(typeof(SchedulePartRecord));
                sends.Schedules = new List<SchedulePartRecord>();

               var schedules = session.CreateQuery("SELECT s.Id,s.League,s.Week,s.GameID,s.Game,s.BusinessUnitID,s.Active,s.ScheduledUI,s.ScheduledUITaskID,s.FinalScore,s.TeamID1,s.TeamID2,s.UISendTeamID1,s.UISendTeamID2,s.APIAttempts,s.ResultCollectedDate,s.GameEndDate,s.UISendStatus,s.IsNewBU " +
                           "FROM HotMarketSender.Models.SchedulePartRecord s " +
                           "WHERE s.UISendStatus = 'scheduled'").List();

                foreach (var s in schedules)
                {
                    var sched = ((object[])(s));

                    sends.Schedules.Add(new SchedulePartRecord()
                    {
                        Id = (int)sched[0],
                        IsNewBU = (bool)sched[18],
                        BusinessUnitID = (int)sched[5],
                        FinalScore = sched[9].ToString(),
                        ScheduledUI = sched[7].ToString(),
                        ScheduledUITaskID = sched[8].ToString()
                    });
                }
            }

            return sends;
        }

        public string DeleteSchedule(string gameID)
        {
            string status = "ok";

            try
            {
                using (var db = new FanaticsEntities())
                {
                    var schedules = db.HotMarketSender_SchedulePartRecord.Where(s => s.GameID == gameID).ToList();
                    db.HotMarketSender_SchedulePartRecord.RemoveRange(schedules);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                status = "Unexpected error while deleting database records.";
            }

            return status;
        }


        public List<string> UpdateSchedule(ScheduleViewModel post)
        {
            List<string> errors = new List<string>();
            Fanatics.Email.Controller.EmailSendDefinition esdController = new Fanatics.Email.Controller.EmailSendDefinition();


            // First, we check if the UI exists
            foreach (var send in post.UISends)
            {
                try
                {
                    if (!string.IsNullOrEmpty(send.Team1UISend))
                    {
                        var esd = esdController.GetByMidCustomerKey(send.BusinessUnit, send.Team1UISend, send.IsNewBU);
                        if (esd == null)
                            errors.Add(string.Format("UI Send {0} does not exists in MID {1}.", send.Team1UISend, send.BusinessUnit));
                    }
                }
                catch (Exception ex)
                {
                    errors.Add(string.Format("Unexpected error while retrieving UI Send {0} on MID {1}.", send.Team1UISend,send.BusinessUnit));
                }

                try
                {
                    if (!string.IsNullOrEmpty(send.Team2UISend))
                    {
                        var esd2 = esdController.GetByMidCustomerKey(send.BusinessUnit, send.Team2UISend, send.IsNewBU);
                        if (esd2 == null)
                            errors.Add(string.Format("UI Send {0} does not exists in BU {1}.", send.Team2UISend, send.BusinessUnit));
                    }
                }
                catch (Exception ex)
                {
                    errors.Add(string.Format("Unexpected error while retrieving UI Send {0} on MID {1}.", send.Team2UISend, send.BusinessUnit));
                }
            }

            // If there were no errors we create/update the record
            if(errors.Count == 0)
            {
                try
                {
                    using (var db = new FanaticsEntities())
                    {
                        var schedules = db.HotMarketSender_SchedulePartRecord.Where(s => s.GameID == post.GameId && post.League.ToLower() == s.League.ToLower()).ToList();
                        db.HotMarketSender_SchedulePartRecord.RemoveRange(schedules);
                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    errors.Add("Unexpected error while deleting database records.");
                    return errors;
                }

                try
                {
                    int id = 0;
                    using (var db = new FanaticsEntities())
                    {
                        id = db.HotMarketSender_SchedulePartRecord.OrderByDescending(s=>s.Id).FirstOrDefault().Id;
                    }

                    foreach (var send in post.UISends)
                    {
                        using (var db = new FanaticsEntities())
                        {
                            HotMarketSender_SchedulePartRecord schedule = new HotMarketSender_SchedulePartRecord();
                            id += 1;
                            schedule.BusinessUnitID = send.BusinessUnit;
                            schedule.IsNewBU = send.IsNewBU;
                            schedule.Id = id;
                            schedule.GameID = post.GameId;
                            schedule.League = post.League;
                            schedule.TeamID1 = post.Home;
                            schedule.TeamID2 = post.Away;
                            schedule.UISendTeamID1 = send.Team1UISend;
                            schedule.UISendTeamID2 = send.Team2UISend;
                            schedule.Week = post.Week;
                            schedule.APIAttempts = 0;
                            schedule.Active = true;
                            schedule.GameEndDate = post.GameStartDate.AddMinutes(post.GameOffset);
                            db.HotMarketSender_SchedulePartRecord.Add(schedule);
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    errors.Add("Unexpected error while creating schedules.");
                    return errors;
                }
            }

            return errors;
        }

        public List<ScheduleViewModel> GetSchedules(int pageIndex, int pageSize, string sortDirection, string sortBy, string searchBy)
        {
            List<ScheduleViewModel> retList = new List<ScheduleViewModel>();

            using (var db = new FanaticsEntities())
            {
                var schedules = db.HotMarketSender_SchedulePartRecord.Where(s => String.IsNullOrEmpty(s.UISendStatus)).OrderBy(s => s.GameEndDate).GroupBy(s => s.GameID).ToList();

                foreach (var foundSchedule in schedules)
                {
                    var schedule = foundSchedule.First();

                    retList.Add(new ScheduleViewModel()
                    {
                        GameId = schedule.GameID,
                        Week = schedule.Week.Value,
                        League = schedule.League,
                        Home = schedule.TeamID1,
                        Away = schedule.TeamID2
                    });
                }
            }

            List<ScheduleViewModel> results = retList;

            // Search
            if (!string.IsNullOrEmpty(searchBy))
            {
                results = retList.Where(p => p.Week.ToString().Equals(searchBy) ||
                    p.Home.ToLower().Contains(searchBy.ToLower()) ||
                    p.Away.ToLower().Contains(searchBy.ToLower()) ||
                    p.League.ToLower().Contains(searchBy.ToLower())
                    ).ToList();
            }

            // Sorting
            if (!string.IsNullOrEmpty(sortBy))
            {
                var objectType = typeof(ScheduleViewModel);
                var propInfo = objectType.GetProperty(sortBy);
                if (sortDirection == "asc")
                    results = results.OrderBy(p => propInfo.GetValue(p, null)).ToList();
                else
                    results = results.OrderByDescending(p => propInfo.GetValue(p, null)).ToList();

            }

            return results;
        }

        public ScheduleViewModel GetSchedule(string gameID)
        {
            ScheduleViewModel schedule = null;

            using (var db = new FanaticsEntities())
            {
                var schedules = db.HotMarketSender_SchedulePartRecord.Where(s => s.GameID == gameID).ToList();

                if (schedules.Count > 0)
                {
                    schedule = new ScheduleViewModel();
                    schedule.GameId = gameID;
                    schedule.Week = schedules[0].Week.Value;
                    schedule.Home= schedules[0].TeamID1;
                    schedule.Away = schedules[0].TeamID2;
                    schedule.League = schedules[0].League;
                    
                    foreach (var foundSchedule in schedules)
                    {
                        schedule.UISends.Add(new UISend()
                        {
                            BusinessUnit = foundSchedule.BusinessUnitID.Value,
                            IsNewBU = foundSchedule.IsNewBU.HasValue ? foundSchedule.IsNewBU.Value : false,
                            Team1UISend = foundSchedule.UISendTeamID1,
                            Team2UISend = foundSchedule.UISendTeamID2
                        });
                    }
                }
            }

            return schedule;
        }

        public List<string> CancelSends(List<SchedulePartRecord> schedules, ISessionLocator SessionLocator)
        {
            Fanatics.Email.Controller.Send sendController = new Fanatics.Email.Controller.Send();
            List<string> errors = new List<string>();

            foreach (var send in schedules)
            {
                string status = "";

                try
                {
                    status = sendController.Cancel(send.BusinessUnitID, int.Parse(send.ScheduledUITaskID), send.IsNewBU);
                    UpdateSendStatus(SessionLocator, send.Id,"canceled");
                }
                catch (Exception ex)
                {
                    errors.Add(string.Format("Could not cancel send {0}. Please attempt to cancel this send in SFMC.",send.ScheduledUI));
                }
            }

            return errors;
        }

        public void UpdateSendStatus(ISessionLocator SessionLocator, int id, string status)
        {
            var session = SessionLocator.For(typeof(SchedulePartRecord));
            var t = session.BeginTransaction();
            string queryText = string.Format("Update HotMarketSender_SchedulePartRecord set UISendStatus = '{0}' where Id = {1}", status, id);
            var query = session.CreateSQLQuery(queryText);

            query.ExecuteUpdate();
            t.Commit();
        }
    }
}