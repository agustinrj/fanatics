﻿using Orchard;
using Orchard.Data;
using Orchard.Taxonomies.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace Fanatics.HotMarketSender.Services
{
    public class BusinessUnitService
    {
        /// <summary>
        /// Returns a list of Business Units available for the user
        /// </summary>
        /// <returns></returns>
        public List<dynamic> Get(ITaxonomyService taxonomies)
        {
            List<dynamic> bus = new List<dynamic>();
            List<dynamic> authorizedBus = new List<dynamic>();
            var taxonomy = taxonomies.GetTaxonomyByName("Fanatics Business Units");
            var terms = taxonomies.GetTerms(taxonomy.Id);

            foreach (var t in terms)
            {
                int AssetsDueDateDaysOffset = 0;
                int QAApprovalDateDaysOffset = 0;
                int QuickDeployFolderID = 0;
                int UISendsFolderId = 0;
                int EmailsFolderId = 0;
                int MID = 0;
                bool isNew = false;

                var xml = t.ContentItem.VersionRecord.Data;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);
                System.Xml.XmlNode n1 = doc.SelectSingleNode("/Data/FanaticsTerm/AssetsDueDateDaysOffset");
                System.Xml.XmlNode n2 = doc.SelectSingleNode("/Data/FanaticsTerm/QAApprovalDateDaysOffset");
                System.Xml.XmlNode n3 = doc.SelectSingleNode("/Data/FanaticsTerm/QuickDeployFolderID");
                System.Xml.XmlNode n4 = doc.SelectSingleNode("/Data/FanaticsTerm/MID");
                System.Xml.XmlNode n5 = doc.SelectSingleNode("/Data/FanaticsTerm/EmailsFolderId");
                System.Xml.XmlNode n6 = doc.SelectSingleNode("/Data/FanaticsTerm/UISendsFolderId");
                System.Xml.XmlNode n7 = doc.SelectSingleNode("/Data/FanaticsTerm/IsNew");

                if (n1 != null)
                    int.TryParse(n1.InnerText, out AssetsDueDateDaysOffset);

                if (n2 != null)
                    int.TryParse(n2.InnerText, out QAApprovalDateDaysOffset);

                if (n3 != null)
                    int.TryParse(n3.InnerText, out QuickDeployFolderID);

                if (n4 != null)
                    int.TryParse(n4.InnerText, out MID);

                if (n5 != null)
                    int.TryParse(n5.InnerText, out EmailsFolderId);

                if (n6 != null)
                    int.TryParse(n6.InnerText, out UISendsFolderId);

                if (n7 != null)
                    bool.TryParse(n7.InnerText, out isNew);

                bus.Add(new { ID = t.FullPath.Replace("/", ""), Name = t.Name, AssetsDueDateDaysOffset = AssetsDueDateDaysOffset, QAApprovalDateDaysOffset = QAApprovalDateDaysOffset, QuickDeployFolderID = QuickDeployFolderID, MID = MID, EmailsFolderId = EmailsFolderId, UISendsFolderId = UISendsFolderId,IsNew = isNew });
            }

            return bus;

        }
    }
}