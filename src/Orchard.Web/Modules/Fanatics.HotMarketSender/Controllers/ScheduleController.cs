﻿using Fanatics.HotMarketSender.Models;
using Fanatics.HotMarketSender.Services;
using Orchard.ContentManagement;
using Orchard.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Fanatics.HotMarketSender.Controllers
{
    public class ScheduleController : Controller
    {
        public ISessionLocator SessionLocator { get; set; }

        public IContentManager ContentManager { get; set; }

        public ScheduleController(
            ISessionLocator sessionLocator, IContentManager contentManager)
        {
            SessionLocator = sessionLocator;
            ContentManager = contentManager;
        }

        public ActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
                return new HttpUnauthorizedResult();

            return View();
        }

        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
                return new HttpUnauthorizedResult();

            return View();
        }

        public ActionResult Edit(string gameID)
        {
            if (!User.Identity.IsAuthenticated)
                return new HttpUnauthorizedResult();

            SchedulesService service = new SchedulesService();
            var schedule = service.GetSchedule(gameID);
            return View("Create",schedule);
        }

        [OutputCache(Duration = 0)]
        public ActionResult Sends(string token)
        {
            SchedulesService service = new SchedulesService();
            var model = service.GetScheduledSends(SessionLocator, ContentManager, token);
            return View(model);
        }

        [HttpPost]
        public JsonResult CancelSends(CancelSendsPost post)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var schedules = serializer.Deserialize<List<SchedulePartRecord>>(post.Schedules);

            SchedulesService service = new SchedulesService();
            var errors = service.CancelSends(schedules, SessionLocator);

            return Json(new { Status = "ok", Errors = errors });
        }

        [HttpPost]
        public JsonResult UpdateSchedule(ScheduleViewModel post)
        {
            if (!User.Identity.IsAuthenticated)
                throw new UnauthorizedAccessException();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var uisends = serializer.Deserialize<List<UISend>>(post.UISendsString);
            post.UISends = uisends;

            SchedulesService service = new SchedulesService();
            var errors = service.UpdateSchedule(post);

            return Json(new { Status = "ok", Errors = errors });
        }

        [HttpPost]
        public JsonResult DeleteSchedule(string gameID)
        {
            if (!User.Identity.IsAuthenticated)
                throw new UnauthorizedAccessException();

            SchedulesService service = new SchedulesService();
            var status = service.DeleteSchedule(gameID);

            return Json(new { Status = status});
        }

        [HttpPost]
        public JsonResult GetByGameId(string gameID)
        {
            if (!User.Identity.IsAuthenticated)
                throw new UnauthorizedAccessException();

            SchedulesService service = new SchedulesService();
            var schedule = service.GetSchedule(gameID);
            return Json(new { Status = "ok", Data = schedule });
        }

        public JsonResult GetSchedules(int pageIndex, int pageSize, string sortDirection, string sortBy, string searchBy)
        {
            if (!User.Identity.IsAuthenticated)
                throw new UnauthorizedAccessException();

            SchedulesService service = new SchedulesService();

            var schedules = service.GetSchedules(pageIndex, pageSize, sortDirection, sortBy, searchBy);

            int count = schedules.Count;

            var data = new { total = count, items = schedules.Skip(pageIndex * pageSize).Take(pageSize).ToList() };

            return Json(new { Status = "success", Data = data }, JsonRequestBehavior.AllowGet);
        }
    }
}