﻿using Orchard.Taxonomies.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fanatics.HotMarketSender.Controllers
{
    public class BusinessUnitController : Controller
    {
        public ITaxonomyService Taxonomies { get; set; }

        public BusinessUnitController(
            ITaxonomyService taxonomies)
        {
            Taxonomies = taxonomies;
        }

        /// <summary>
        /// Gets all available business units
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Get()
        {
            HotMarketSender.Services.BusinessUnitService buService = new Services.BusinessUnitService();
            var taxononomies = buService.Get(Taxonomies);

            return Json(new { Status = "ok", Data = taxononomies });
        }
    }
}