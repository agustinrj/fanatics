﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sportradar.Sdk;
using Sportradar.Sdk.API;

namespace Fanatics.HotMarketSender.Controllers
{
    public class SportRadarController : Controller
    {

        [HttpPost]
        public JsonResult GetSchedule(string league)
        {
            switch (league.ToLower())
            {
                case "nfl":
                    return Json(new { Status = "ok", Data = NFL.GetSchedule(null).ToString() });
                case "ncaafb":
                    return Json(new { Status = "ok", Data = NCAAFB.GetSchedule(null).ToString() });
            }

            return Json(new { Status = "error", Data = "not found" });
        }
    }
}