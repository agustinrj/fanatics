﻿using Orchard.Mvc.Routes;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;

namespace Fanatics.HotMarketSender
{
    public class Routes : IRouteProvider
    {
        public void GetRoutes(ICollection<RouteDescriptor> routes)
        {
            foreach (var routeDescriptor in GetRoutes())
                routes.Add(routeDescriptor);
        }

        public IEnumerable<RouteDescriptor> GetRoutes()
        {
            return new[] {
                new RouteDescriptor {
                    Priority = 5,
                    Route = new Route(
                        "hotmarketsender/create",  
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"},  
                            {"controller", "Schedule"},
                            {"action", "Create"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"}  
                        },
                        new MvcRouteHandler())
                },
                 new RouteDescriptor {
                    Priority = 5,
                    Route = new Route(
                        "hotmarketsender",
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"},
                            {"controller", "Schedule"},
                            {"action", "Index"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"}
                        },
                        new MvcRouteHandler())
                },
                  new RouteDescriptor {
                    Priority = 5,
                    Route = new Route(
                        "hotmarketsender/getschedules",
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"},
                            {"controller", "Schedule"},
                            {"action", "GetSchedules"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"}
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Route = new Route(
                        "hotmarketsender/edit/{gameID}",
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"},
                            {"controller", "Schedule"},
                            {"action", "Edit"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"}
                        },
                        new MvcRouteHandler())
                },
                 new RouteDescriptor {
                    Priority = 5,
                    Route = new Route(
                        "hotmarketsender/update",
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"},
                            {"controller", "Schedule"},
                            {"action", "UpdateSchedule"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"}
                        },
                        new MvcRouteHandler())
                }
                 ,
                 new RouteDescriptor {
                    Priority = 5,
                    Route = new Route(
                        "hotmarketsender/delete-schedule",
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"},
                            {"controller", "Schedule"},
                            {"action", "DeleteSchedule"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"}
                        },
                        new MvcRouteHandler())
                }
                 ,
                 new RouteDescriptor {
                    Priority = 5,
                    Route = new Route(
                        "hotmarketsender/getbygameid",
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"},
                            {"controller", "Schedule"},
                            {"action", "GetByGameId"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"}
                        },
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Route = new Route(
                        "hotmarketsender/sportradar/getschedule",
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"},
                            {"controller", "SportRadar"},
                            {"action", "GetSchedule"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"}
                        },
                        new MvcRouteHandler())
                }
                ,
                new RouteDescriptor {
                    Route = new Route(
                        "hotmarketsender/businessunit",
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"},
                            {"controller", "BusinessUnit"},
                            {"action", "Get"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"}
                        },
                        new MvcRouteHandler())
                },
                 new RouteDescriptor {
                    Route = new Route(
                        "hotmarketsender/sends/{token}", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"}, // this is the name of your module
                            {"controller", "Schedule"},
                            {"action", "Sends"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                }
                 ,new RouteDescriptor {
                    Route = new Route(
                        "hotmarketsender/cancel-sends", // this is the name of the page url
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"}, // this is the name of your module
                            {"controller", "Schedule"},
                            {"action", "CancelSends"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Fanatics.HotMarketSender"} // this is the name of your module
                        },
                        new MvcRouteHandler())
                }
            };
        }
    }
}