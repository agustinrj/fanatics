﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Fanatics.Email.Model.HubExchange
{
    public static class SessionManager
    {
        #region Session Properties

        public static string SOAPEndPoint
        {
            get
            {
                return HttpContext.Current.Session["SOAPEndPoint"] == null ? null : HttpContext.Current.Session["SOAPEndPoint"].ToString();
            }
            set
            {
                HttpContext.Current.Session["SOAPEndPoint"] = value;
            }
        }

        public static string InternalOauthToken
        {
            get
            {
                return HttpContext.Current.Session["internalOauthToken"] == null ? null : HttpContext.Current.Session["internalOauthToken"].ToString();
            }
            set
            {
                HttpContext.Current.Session["internalOauthToken"] = value;
            }
        }

        public static string OAuthToken
        {
            get
            {
                return HttpContext.Current.Session["oauthToken"] == null ? null : HttpContext.Current.Session["oauthToken"].ToString();
            }
            set
            {
                HttpContext.Current.Session["oauthToken"] = value;
            }
        }

        public static string RefreshToken
        {
            get
            {
                return HttpContext.Current.Session["refreshToken"] == null ? string.Empty : HttpContext.Current.Session["refreshToken"].ToString();
            }
            set
            {
                HttpContext.Current.Session["refreshToken"] = value;
            }
        }

        public static string userId
        {
            get
            {
                return HttpContext.Current.Session["userID"] == null ? string.Empty : HttpContext.Current.Session["userID"].ToString();
            }
            set
            {
                HttpContext.Current.Session["userID"] = value;
            }
        }

        public static string MID
        {
            get
            {
                return HttpContext.Current.Session["MID"] == null ? string.Empty : HttpContext.Current.Session["MID"].ToString();
            }
            set
            {
                HttpContext.Current.Session["MID"] = value;
            }
        }

        public static DateTime TokenRequest
        {
            set
            {
                HttpContext.Current.Session["tokenRequest"] = value;
            }
        }

        public static string EncodedJWT
        {
            get
            {
                return HttpContext.Current.Session["EncodedJWT"] == null ? string.Empty : HttpContext.Current.Session["EncodedJWT"].ToString();
            }
            set
            {
                HttpContext.Current.Session["EncodedJWT"] = value;
            }
        }

        public static string DecodedJWT
        {
            get
            {
                return HttpContext.Current.Session["DecodedJWT"] == null ? string.Empty : HttpContext.Current.Session["DecodedJWT"].ToString();
            }
            set
            {
                HttpContext.Current.Session["DecodedJWT"] = value;
            }
        }

        public static string UserName
        {
            get
            {
                return HttpContext.Current.Session["UserName"] == null ? null : HttpContext.Current.Session["UserName"].ToString();
            }
            set
            {
                HttpContext.Current.Session["UserName"] = value;
            }
        }
        #endregion

        /// <summary>
        /// Returns wether the session for the FUEL API is about to expired. 
        /// The fuel Session Timeout is specified in web.config
        /// </summary>
        /// <returns></returns>
        public static bool FuelApiSessionExpired()
        {
            return HttpContext.Current.Session["tokenRequest"] == null ? true : (DateTime.Now - ((DateTime)HttpContext.Current.Session["tokenRequest"])).TotalSeconds > int.Parse(ConfigurationManager.AppSettings["FuelSessionTimeout"]);
        }
    }
}
