﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Model
{
    public class Send
    {
        public int ID { get; set; }
        public string Status { get; set; }
        public int NumberTargeted { get; set; }
    }
}
