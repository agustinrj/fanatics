﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Model
{
    public class SendScheduleResult
    {
        public string Message { get; set; }
        public int TaskID { get; set; }
    }
}
