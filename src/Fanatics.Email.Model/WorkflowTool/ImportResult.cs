﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Model
{
    public class ImportResult
    {
        public int ProcessedCount { get; set; }
        public string Status { get; set; }
        public List<string> Errors{ get; set; }


        public ImportResult()
        {
            Errors = new List<string>();
        }

    }
}
