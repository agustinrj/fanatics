﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Model
{
    public class EmailSendCreateRequest
    {
        public Campaign Campaign { get; set; }
        public string CampaignString { get; set; }
        public bool ProcesssRequest { get; set; }
        public int MID { get; set; }
        public bool IsNewBU { get; set; }
        public int QuickDeployFolderId { get; set; }
        public int EmailsFolderId { get; set; }
        public int UISendsFolderId { get; set; }
        public bool CreateQuickDeploy { get; set; }
        public bool CreateUIPerAudience { get; set; }
        public bool Overwrite { get; set; }
    }
}
