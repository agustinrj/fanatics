﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Model
{
    public class CampaignFeedback
    {
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Feedback { get; set; }
    }
}
