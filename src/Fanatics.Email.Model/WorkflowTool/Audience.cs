﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Model
{
    public class Audience
    {
        public int FolderID { get; set; }
        public string AudienceType { get; set; }
        public string AudienceGroup { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
