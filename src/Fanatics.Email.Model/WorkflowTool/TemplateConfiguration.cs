﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Model
{
    public class TemplateConfiguration
    {
        public string ContentValue { get; set; }
        public string Section { get; set; }
        public string ContentName { get; set; }
    }
}
