﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Model.WorkflowTool
{
    public class CampaignNotification
    {
        public string EmailAddress { get; set; }
        public string CampaignID { get; set; }
        public string CampaignName { get; set; }
        public string NotificationType { get; set; }
        public string NotificationBody { get; set; }
        public bool IsAdmin { get; set; }
        public string UserName { get; set; }
    }
}
