﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Model
{
    public class CampaignOwner
    {
        public string UserName { get; set; }
        public string UserEmail { get; set; }
    }
}
