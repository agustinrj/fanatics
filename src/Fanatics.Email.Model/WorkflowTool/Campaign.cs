﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Model  
{
    public class Campaign
    {
        public string Id { get; set; }
        public string CreatedBy { get; set; }
        public string Disclaimer { get; set; }
        public string LastModifiedBy { get; set; }
        public string Notes { get; set; }
        public string AssetsLocation { get; set; }
        public string BusinessUnit { get; set; }
        public string MessageType { get; set; }
        public DateTime DeploymentDate { get; set; }
        public string DayOfWeek { get; set; }
        public int WeekNo  { get; set; }
        public string Theme  { get; set; }
        public string EmailName { get; set; }
        public string Status  { get; set; }
        public string OpsOwner  { get; set; }
        public string OpsOwnerEmail { get; set; }
        public string JobIDs  { get; set; }
        public DateTime AssetsDueDate { get; set; }
        public DateTime QAApprovalDate { get; set; }
        public string SubjectLine  { get; set; }
        public string Preheader { get; set; }
        public string PreheaderURL { get; set; }
        public string Template { get; set; }
        public string TemplateSFMCName { get; set; }
        public string TemplateMap { get; set; }
        public string CNumber { get; set; }
        public string CName { get; set; }
        public List<string> Toggles { get; set; }
        public List<CampaignOwner> CampaignOwners { get; set; }
        public List<Audience> Audiences{ get; set; }
        public List<Promo> Promos { get; set; }
        public List<TemplateConfiguration> TemplateConfiguration { get; set; }
        public List<CampaignFeedback> Feedback{ get; set; }

        public Campaign() 
        {
            Toggles = new List<string>();
            CampaignOwners = new List<CampaignOwner>();
            Audiences = new List<Audience>();
            Promos = new List<Promo>();
            TemplateConfiguration = new List<TemplateConfiguration>();
            Feedback = new List<CampaignFeedback>();
        }
    }
}
