﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Model
{
    public class Email
    {
        public string Name { get; set; }
        public string HTMLBody { get; set; }
        public string PreHeader { get; set; }
        public string Subject { get; set; }
        public int ID { get; set; }
    }
}
