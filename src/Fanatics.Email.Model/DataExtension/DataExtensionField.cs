﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Model
{
    public class DataExtensionField
    {
        public string Name { get; set; }
        public string FieldType { get; set; }
        public string DefaultValue { get; set; }
        public bool IsRequired { get; set; }
        public bool IsPrimaryKey { get; set; }
        public int MaxLength { get; set; }
        public int Ordinal { get; set; }
    }
}
