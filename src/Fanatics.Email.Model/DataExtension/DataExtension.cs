﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Model
{
    public class DataExtension
    {
        public string ExternalKey { get; set; }
        public bool HasPrimaryKey { get; set; }
        public string ObjectID { get; set; }
        public List<IDictionary<String, object>> Rows { get; set; }

        public DataExtension() 
        {
            Rows = new List<IDictionary<String, object>>();
        }
    }
}
