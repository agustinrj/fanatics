﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Model
{
    public class BusinessUnit
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsNew { get; set; }
    }
}
