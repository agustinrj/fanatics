﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Model
{
    public class EmailSendDefinition
    {
        public string Name { get; set; }
        public string CustomerKey { get; set; }
        public string SubjectLine { get; set; }
        public string EmailName { get; set; }
        public List<EmailSendDefinitionList> SendAudience{ get; set; }
        public List<EmailSendDefinitionList> ExclusionAudience { get; set; }
        public Send Send { get; set; }
        public bool DeduplicateByEmail { get; set; }

        public EmailSendDefinition() {
            SendAudience = new List<EmailSendDefinitionList>();
            ExclusionAudience = new List<EmailSendDefinitionList>();
        }
    }
}
