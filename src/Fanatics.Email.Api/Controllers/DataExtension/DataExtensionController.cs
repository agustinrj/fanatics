﻿using Fanatics.Email.Api.Models;
using Fanatics.Email.Model.HubExchange;
using Microsoft.Owin;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Script.Serialization;
namespace Fanatics.Email.Api.Controllers.DataExtension
{
    [RoutePrefix("api/dataextension")]
    public class DataExtensionController : BaseController
    {
        [Route("gettime")]
        public IHttpActionResult GetTime()
        {
            return Json(new { now = DateTime.Now, utc=DateTime.UtcNow });
        }

        [Authorize]
        [Route("")]
        public IHttpActionResult Get(string externalKey, bool isNew)
        {
            try
            {
                int mid = 0;
                int.TryParse(isNew? ConfigurationManager.AppSettings["NewETClient"] : ConfigurationManager.AppSettings["ETClient"], out mid);

                var de = Controllers.DataExtension.GetByExternalKeyMID(externalKey,isNew, mid,null);

                return Json(de.Rows);
            }
            catch (Exception e)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(e.Message)
                };
                throw new HttpResponseException(resp);
            }
        }

        [Authorize]
        [Route("getFields")]
        public IHttpActionResult GetFields(string externalKey, bool isNew)
        {
            try
            {
                int mid = 0;
                int.TryParse(isNew ? ConfigurationManager.AppSettings["NewETClient"] : ConfigurationManager.AppSettings["ETClient"], out mid);

                var fields = Controllers.DataExtension.GetFieldsByExternalKeyMID(externalKey,isNew, mid);

                return Ok(fields);
            }
            catch (Exception e)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(e.Message)
                };
                throw new HttpResponseException(resp);
            }
        }

        [Authorize]
        [Route("update")]
        public IHttpActionResult Update([FromBody]DataExtensionPost post)
        {
            try
            {
                int mid = 0;
                int.TryParse(post.IsNew ? ConfigurationManager.AppSettings["NewETClient"] : ConfigurationManager.AppSettings["ETClient"], out mid);

                var rows = new System.Web.Script.Serialization.JavaScriptSerializer()
                .Deserialize<Dictionary<string, object>>(post.Row);

                return Ok(new {resultStatus = Controllers.DataExtension.Update(rows,post.ExternalKey,post.IsNew,mid)});
            }
            catch (Exception e)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(e.Message)
                };
                throw new HttpResponseException(resp);
            }
        }

        [Authorize]
        [Route("remove")]
        public IHttpActionResult Remove([FromBody]DataExtensionPost post)
        {
            try
            {
                int mid = 0;
                int.TryParse(post.IsNew ? ConfigurationManager.AppSettings["NewETClient"] : ConfigurationManager.AppSettings["ETClient"], out mid);

                var rows = new System.Web.Script.Serialization.JavaScriptSerializer()
                .Deserialize<Dictionary<string, object>>(post.Row);

                return Ok(new { resultStatus = Controllers.DataExtension.Delete(rows, post.ExternalKey,post.IsNew, mid) });
            }
            catch (Exception e)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(e.Message)
                };
                throw new HttpResponseException(resp);
            }
        }
    }
}