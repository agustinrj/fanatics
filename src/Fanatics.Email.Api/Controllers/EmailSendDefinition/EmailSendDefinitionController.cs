﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace Fanatics.Email.Api.Controllers
{
    [RoutePrefix("api/email-send-definition")]
    public class EmailSendDefinitionController : BaseController
    {
        [Authorize]
        [Route("")]
        public IHttpActionResult Get(int mid, bool isNew)
        {
            var folder = Controllers.Folder.GetByMidContentTypeName(mid,isNew, "userinitiatedsends", ConfigurationManager.AppSettings["DeploymentFolderName"]);

            if (folder == null)
                throw new Exception("Folder Not Found");

            return Ok(Controllers.EmailSendDefinition.GetNamesByMidFolderId(mid, folder.ID,isNew));
        }

        [Authorize]
        [Route("")]
        public IHttpActionResult Get(int mid,bool isNew,string customerKey)
        {
            return Ok(Controllers.EmailSendDefinition.GetByMidCustomerKey(mid,customerKey,isNew));
        }
    }
}
