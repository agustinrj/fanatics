﻿using Fanatics.Email.Api.Models;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace Fanatics.Email.Api.Controllers
{
    [RoutePrefix("api/send")]
    public class SendController : BaseController
    {
        [Authorize]
        [Route("schedule")]
        public IHttpActionResult Schedule([FromBody]SchedulePost post)
        {
            try
            {
                return Ok(Controllers.Send.Schedule(post.mid, post.customerKey, post.subjectLine, post.isNewBU));
            }
            catch (Exception e)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(e.Message)
                };
                throw new HttpResponseException(resp);
            }           
        }

        [Authorize]
        [Route("cancel")]
        public IHttpActionResult Cancel([FromBody]CancelPost post)
        {
            try
            {
            return Ok(Controllers.Send.Cancel(post.mid, post.sendID, post.isNewBU));
             }
            catch (Exception e)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(e.Message)
                };
                throw new HttpResponseException(resp);
            }  
        }

        [Authorize]
        [Route("test")]
        public IHttpActionResult Test([FromBody]SchedulePost post)
        {
            try
            {
                return Ok(Controllers.Send.Test(post.mid, post.customerKey, post.subjectLine, post.isNewBU));
            }
            catch (Exception e)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(e.Message)
                };
                throw new HttpResponseException(resp);
            }
        }
    }
}
