﻿using Fanatics.Email.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Fanatics.Email.Api.Controllers
{
    [RoutePrefix("api/auth")]
    
    public class AuthController : BaseController
    {
        [Authorize]
        [Route("refreshTokenIfNeeded")]
        public IHttpActionResult refreshTokenIfNeeded(RefreshTokenPost post)
        {
            try
            {
                string returnToken = post.Token;
                string returnInternalOauthToken = post.InternalOauthToken;
                string returnRefreshToken = post.RefreshToken;
                DateTime returnTokenRequest = post.TokenRequest;

                // Fuel Session expires after 1000 seconds
                bool sessionExpired = post.TokenRequest == null ? true : (DateTime.Now - post.TokenRequest).TotalSeconds > 1000;

                if (sessionExpired)
                {
                    var newToken = Controllers.HubExchange.GetAccessToken(post.Token, post.InternalOauthToken, post.RefreshToken, true);
                    returnToken = newToken.legacyToken;
                    returnInternalOauthToken = newToken.accessToken;
                    returnRefreshToken = newToken.refreshToken;
                    returnTokenRequest = DateTime.Now;
                }

                var response = new
                {
                    InternalOauthToken = returnToken,
                    OAuthToken = returnInternalOauthToken,
                    RefreshToken = returnRefreshToken,
                    TokenRequest = returnTokenRequest
                };

                return Ok(response);
            }
            catch (Exception e)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(e.Message)
                };
                throw new HttpResponseException(resp);
            }
        }
    }

    
}
