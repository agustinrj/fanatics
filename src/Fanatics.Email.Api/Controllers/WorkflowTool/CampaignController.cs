﻿using Fanatics.Email.Api.Models;
using Fanatics.Email.Model.HubExchange;
using Microsoft.Owin;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Script.Serialization;
using Fanatics.Email.Model;

namespace Fanatics.Email.Api.Controllers.Campaign
{
    [RoutePrefix("api/campaign")]
    public class CampaignController : BaseController
    {
       
        [Route("saveupdate")]
        public IHttpActionResult SaveUpdate(Fanatics.Email.Model.Campaign post)
        {
            try
            {
                int mid = 0;
                int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

                return Ok(new { resultStatus =  "campaign Updated"});
            }
            catch (Exception e)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(e.Message)
                };
                throw new HttpResponseException(resp);
            }
        }

        [Route("getdetails")]
        public IHttpActionResult GetDetails(int campaignID)
        {
            try
            {
                int mid = 0;
                int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

                return Ok(new { resultStatus = "campaign Updated" });
            }
            catch (Exception e)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(e.Message)
                };
                throw new HttpResponseException(resp);
            }
        }
    }
}