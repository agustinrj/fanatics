﻿using Fanatics.Email.Controller;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Sportradar.Sdk;
using Sportradar.Sdk.API;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;


namespace Fanatics.Email.Api.Controllers
{
    [RoutePrefix("api/hotmarket-sender")]
    [Authorize(Roles = "scheduler")]
    public class HotMarketSenderController: BaseController
    {

        [Route("process")]
        [HttpPost]
        public IHttpActionResult Process()
        {
            try
            {
                List<dynamic> scores = new List<dynamic>();
                List<HotMarketSender_SchedulePartRecord> activeSchedules = new List<HotMarketSender_SchedulePartRecord>();
                bool scheduled = false;

                using (var db =  new FanaticsEntities())
                {
                    // Get pending schedules from DB
                    activeSchedules = db.HotMarketSender_SchedulePartRecord.Where(s=>s.Active.HasValue && s.Active.Value == true).ToList();

                    foreach (var schedule in activeSchedules)
                    {
                        // check if we already have the score so we don't hit the api more than once
                        var boxScore = scores.Where(s => s.id == schedule.GameID).FirstOrDefault();

                        if (boxScore != null)
                            ScheduleUIForWinner(boxScore, schedule);
                        else
                        {
                            // if Today > than when we are supposed to hit the API, we hit the API
                            // we don't hit the API if we've been hitting it more than 20 times
                            var apiAttempts = schedule.APIAttempts == null ? 0 : schedule.APIAttempts;

                            if (DateTime.UtcNow > schedule.GameEndDate && apiAttempts < 20 && schedule.Active.HasValue && schedule.Active.Value)
                            {
                                dynamic score = null;

                                // get score
                                switch (schedule.League.ToLower())
                                {
                                    case "nfl":
                                        score = NFL.GetBoxScore(schedule.GameID);
                                        break;
                                    case "ncaafb":
                                        score = NCAAFB.GetBoxScore(null, schedule.Week.Value, schedule.TeamID1, schedule.TeamID2);
                                        break;
                                }

                                // add api attemp (whether we got a winner or not)
                                schedule.APIAttempts = schedule.APIAttempts == null ? 1 : schedule.APIAttempts + 1;

                                // if status = closed, get winner
                                if (score.status == "complete" || score.status == "closed")
                                {
                                    // update collected date for tracking purposes
                                    schedule.ResultCollectedDate = DateTime.UtcNow;
                                    bool result = ScheduleUIForWinner(score, schedule);
                                    if (result)
                                        scheduled = result;

                                    scores.Add(score);
                                }
                            }
                            else
                            {
                                if(apiAttempts >= 20)
                                {
                                    // if we reached the limit set all the schedules for the game ID to inactive.
                                    var exceededSchedules = activeSchedules.Where(s => s.GameID == schedule.GameID).ToList();
                                    foreach (var exceededSchedule in exceededSchedules)
                                        exceededSchedule.Active = false;
                                }
                            }
                        }
                    }

                    db.SaveChanges();
                }

                if (scheduled)
                    SendSMSNotification();

                return Ok();
            }
            catch (Exception e)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(e.Message)
                };
                throw new HttpResponseException(resp);
            }
        }

        [Route("updatestatus")]
        [HttpPost]
        public IHttpActionResult updatestatus()
        {

            try
            {
                List<HotMarketSender_SchedulePartRecord> schedules = new List<HotMarketSender_SchedulePartRecord>();
                Fanatics.Email.Controller.Send sendController = new Send();

                using (var db = new FanaticsEntities())
                {
                    // Get schedules from DB
                    schedules = db.HotMarketSender_SchedulePartRecord.Where(s => s.UISendStatus.ToLower() == "scheduled").ToList();

                    foreach (var schedule in schedules)
                    {
                        // get status from sfmc   
                        var status = sendController.GetSendStatus(schedule.BusinessUnitID.Value, int.Parse(schedule.ScheduledUITaskID), schedule.IsNewBU.Value);
                        if (status.ToLower() == "complete")
                            schedule.UISendStatus = status;
                    }

                    db.SaveChanges();
                }
                
                return Ok();
            }
            catch (Exception e)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(e.Message)
                };
                throw new HttpResponseException(resp);
            }
        }
        private bool ScheduleUIForWinner(dynamic boxScore,HotMarketSender_SchedulePartRecord schedule)
        {
            Fanatics.Email.Controller.Send sendController = new Send();

            bool scheduled = false;
            string scheduleUI = "";
            Result result = new Result();

            switch (schedule.League.ToLower())
            {
                case "nfl":
                    result = NFL.GetWinnerAndScore(boxScore);
                    break;
                case "ncaafb":
                    result = NCAAFB.GetWinnerAndScore(boxScore);
                    break;
            }

            if (!string.IsNullOrEmpty(result.Winner))
            {
                if (result.Winner.ToLower() == schedule.TeamID1.ToLower())
                    scheduleUI = schedule.UISendTeamID1;
                else if (result.Winner.ToLower() == schedule.TeamID2.ToLower())
                    scheduleUI = schedule.UISendTeamID2;
            }

            using (var db = new FanaticsEntities())
            {
                var dbSchedule = db.HotMarketSender_SchedulePartRecord.Where(s => s.Id == schedule.Id).FirstOrDefault();
                dbSchedule.Active = false;
                dbSchedule.FinalScore = result.FinalScore;

                // if we have a winner we schedule the ui
                if (!string.IsNullOrEmpty(scheduleUI))
                {
                    dbSchedule.ScheduledUI = scheduleUI;
                    dbSchedule.ScheduledUITaskID = sendController.Schedule(dbSchedule.BusinessUnitID.Value,scheduleUI,"",dbSchedule.IsNewBU.Value).TaskID.ToString(); // TODO: get task ID once ui is scheduled. 
                    dbSchedule.UISendStatus = "scheduled";
                    scheduled = true;
                }
                db.SaveChanges();
            }
            return scheduled;
        }

        private void SendSMSNotification()
        {
            // get token to put on url of SMS message.
            string tokenID = RandomString(10);

            using (var db = new FanaticsEntities())
            {
                var token = db.HotMarketSender_Authentication.Where(t => t.ExpirationDate == DateTime.Today).FirstOrDefault();
                int id = db.HotMarketSender_Authentication.Count();
                if (token != null)
                    tokenID = token.TokenID;
                else
                {
                    db.HotMarketSender_Authentication.Add(new HotMarketSender_Authentication()
                    {
                        Id = id,
                        TokenID = tokenID,
                        ExpirationDate = DateTime.Today
                    });
                    db.SaveChanges();
                }
            }

            // Login to fuel API
            string authToken = GetFuelToken();

            // Send SMS!
            if(!string.IsNullOrEmpty(authToken))
                SendListSMS(authToken, tokenID);
        }

        private string GetFuelToken()
        {
            var authBody = new { clientId = "33ab46x5pgwetba7w8tp4gpk", clientSecret = "pu7vhZff6rmdf4nHR2Fb5ZAK" };
            var client = new RestClient("https://auth.exacttargetapis.com");
            client.AddHandler("application/json", new DynamicJsonDeserializer());
            var request = new RestRequest("v1/requestToken", Method.POST);
            request.AddJsonBody(authBody);
            var response = client.Execute<JObject>(request).Data.ToObject<dynamic>();
            return response.accessToken;
        }

        private void SendListSMS(string authToken,string urlToken)
        {
            StringBuilder body = new StringBuilder();
            body.AppendLine("{\"TargetListIds\": [\""+ ConfigurationManager.AppSettings["scheduler.smslist"]+ "\"],");
            body.AppendLine("\"OverrideTemplateTargetLists\": \"true\",");
            body.AppendLine("\"OverrideTemplateExclusionLists\": \"false\",");
            body.AppendLine("\"IgnoreExclusionLists\": \"true\",");
            body.AppendLine("\"OverrideMessageText\": \"true\",");
            body.AppendLine("\"MessageText\":\"Hot Market Sender new UIs scheduled: http://fanatics-dashboard.azurewebsites.net/hotmarketsender/sends/"+urlToken+"\",");
            body.AppendLine("\"AllowDuplication\": \"false\"}");
       
            var client = new RestClient("https://www.exacttargetapis.com");
            client.AddHandler("application/json", new DynamicJsonDeserializer());
            var request = new RestRequest(String.Format("sms/v1/messageList/{0}/send", ConfigurationManager.AppSettings["scheduler.smsMessage"]), Method.POST);
            request.AddParameter("Application/Json", body, ParameterType.RequestBody);
            request.AddHeader("Authorization", "Bearer " + authToken);

            var response = client.Execute<JObject>(request).Data.ToObject<dynamic>();
        }

        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private void createSchedules(int weekNumber,string league)
        {
            // get nfl schedule
            if (league == "nfl")
            {
                var nfl = NFL.GetSchedule(null);
                var week = nfl.weeks[weekNumber - 1];
                int id = 0;
                using (var db = new FanaticsEntities())
                {
                    id = db.HotMarketSender_SchedulePartRecord.Count();
                }

                foreach (var game in week.games)
                {
                    using (var db = new FanaticsEntities())
                    {
                        HotMarketSender_SchedulePartRecord schedule = new HotMarketSender_SchedulePartRecord();
                        id += 1;
                        schedule.BusinessUnitID = 1;
                        schedule.Id = id;
                        schedule.GameID = game.id;
                        schedule.League = "nfl";
                        schedule.TeamID1 = game.home.alias;
                        schedule.TeamID2 = game.away.alias;
                        schedule.UISendTeamID1 = game.home.alias + " UI SEND";
                        schedule.UISendTeamID2 = game.away.alias + " UI SEND";
                        schedule.Week = weekNumber;
                        schedule.APIAttempts = 0;
                      
                        schedule.GameEndDate = ((DateTime)game.scheduled).AddHours(7).AddMinutes(12);
                        db.HotMarketSender_SchedulePartRecord.Add(schedule);
                        db.SaveChanges();
                    }
                }
            }
            else
            {
                // get ncaa schedule
                var week = NCAAFB.GetWeeklySchedule(null,weekNumber);
                int id = 0;
                using (var db = new FanaticsEntities())
                {
                    id = db.HotMarketSender_SchedulePartRecord.Count();
                }

                foreach (var game in week.games)
                {
                   
                        using (var db = new FanaticsEntities())
                        {
                            HotMarketSender_SchedulePartRecord schedule = new HotMarketSender_SchedulePartRecord();
                        id += 1;
                            schedule.BusinessUnitID = 1;
                            schedule.Id = id;
                            schedule.GameID = game.id;
                            schedule.League = "ncaafb";
                            schedule.TeamID1 = game.home;
                            schedule.TeamID2 = game.away;
                            schedule.UISendTeamID1 = game.home + " UI SEND";
                            schedule.UISendTeamID2 = game.away + " UI SEND";
                            schedule.Week = weekNumber;
                        schedule.APIAttempts = 0;
                        schedule.GameEndDate = ((DateTime)game.scheduled).AddHours(7).AddMinutes(30);
                            db.HotMarketSender_SchedulePartRecord.Add(schedule);
                            db.SaveChanges();
                        }
                    
                }
            }
        }
          
    }
}