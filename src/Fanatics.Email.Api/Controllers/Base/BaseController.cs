﻿using Fanatics.Email.Controller.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Fanatics.Email.Api.Controllers
{
    public class BaseController : ApiController
    {
        private ControllerFactory _controllers;

        public ControllerFactory Controllers {
            get {
                if (_controllers == null)
                    _controllers = new ControllerFactory();
                return _controllers;
            }
        }

        public string UserName {
            get {
                // get user from token
                return ((System.Security.Claims.ClaimsIdentity)(User.Identity)).Claims.FirstOrDefault(c => c.Type == "user").Value;
            }
        }


    }
}
