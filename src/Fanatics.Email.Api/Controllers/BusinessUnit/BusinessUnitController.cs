﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Fanatics.Email.Api.Controllers.BusinessUnit
{
    [RoutePrefix("api/business-unit")]
    public class BusinessUnitController : BaseController
    {
        [Authorize]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(Controllers.BusinessUnit.Get());
        }
    }
}
