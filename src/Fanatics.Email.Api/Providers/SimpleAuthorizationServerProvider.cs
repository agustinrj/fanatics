﻿using Fanatics.Email.Controller;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Api
{
    class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
           
            Auth auth = new Auth();

            var data = await context.Request.ReadFormAsync();
            var token = data["token"];
            var userId = data["userId"];
            
            if (token != null && userId != null)
            {
                if (!auth.LoginByTokenUserId(token,userId))
                {
                    context.SetError("invalid_grant", "The user token or userID are incorrect or token expired.");
                    return;
                }
            }
            else
            {
                if (!auth.Login(context.UserName, context.Password))
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }
            }
           
           var identity = new ClaimsIdentity(context.Options.AuthenticationType);
           if (context.UserName != null)
            identity.AddClaim(new Claim("user", context.UserName));
            
            context.Validated(identity);
        }
    }
}
