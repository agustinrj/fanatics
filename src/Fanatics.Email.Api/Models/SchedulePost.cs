﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fanatics.Email.Api.Models
{
    public class SchedulePost
    {
        public int mid { get; set; }
        public string customerKey { get; set; }
        public string subjectLine { get; set; }
        public bool isNewBU { get; set; }
    }
}