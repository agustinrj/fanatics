﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fanatics.Email.Api.Models
{
    public class DataExtensionPost
    {
        public string Row { get; set; }
        public string ExternalKey { get; set; }
        public bool IsNew { get; set; }
    }
}