﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fanatics.Email.Api.Models
{
    public class RefreshTokenPost
    {
        public string Token { get; set; }
        public string InternalOauthToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTime TokenRequest { get; set; }
    }
}