﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fanatics.Email.Api.Models
{
    public class CancelPost
    {
        public int mid { get; set; }
        public int sendID { get; set; }
        public bool isNewBU { get; set; }
    }
}