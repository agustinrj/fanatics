﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;

namespace Sportradar.Sdk.API
{
    public static class NFL
    {
        private static string scheduleURL = "nfl-ot1/games/{year}/PST/schedule.json?api_key={apiKey}";
        private static string boxscoreURL = "nfl-ot1/games/{gameID}/boxscore.json?api_key={apiKey}";

        public static dynamic GetSchedule(int ? year)
        {
            var client = new RestClient("http://api.sportradar.us");
            client.AddHandler("application/json", new DynamicJsonDeserializer());

            var request = new RestRequest(scheduleURL, Method.GET);
            request.AddUrlSegment("apiKey", System.Configuration.ConfigurationManager.AppSettings["sportsradar.apikey.nfl"]);
            request.AddUrlSegment("year", year.HasValue ? year.Value.ToString() : GetSeasonYear().ToString());
           
            var response = client.Execute<JObject>(request).Data.ToObject<dynamic>();
            return response;
        }
        
        public static dynamic GetBoxScore(string gameID)
        {
            var client = new RestClient("http://api.sportradar.us");
            client.AddHandler("application/json", new DynamicJsonDeserializer());

            var request = new RestRequest(boxscoreURL, Method.GET);
            request.AddUrlSegment("apiKey", System.Configuration.ConfigurationManager.AppSettings["sportsradar.apikey.nfl"]);
            request.AddUrlSegment("gameID", gameID);

            var response = client.Execute<JObject>(request).Data.ToObject<dynamic>();
            return response;
        }

        public static Result GetWinnerAndScore(dynamic score)
        {
            Result result = new Result();

            if (score.status == "complete" || score.status == "closed")
            {
                result.FinalScore = string.Format("{0} {1} - {2} {3}", score.summary.home.alias, score.summary.home.points, score.summary.away.alias, score.summary.away.points);

                if (score.summary.home.points > score.summary.away.points)
                    result.Winner = score.summary.home.alias;

                if (score.summary.away.points > score.summary.home.points)
                    result.Winner= score.summary.away.alias;

            }
         
            return result;
        }

        public static int GetSeasonYear()
        {
            int year = DateTime.Now.Year - 1;

            if (DateTime.Now.Month >= 8 && DateTime.Now.Month < 1)
                year = DateTime.Now.Year;

            return year;
        }
    }
}
