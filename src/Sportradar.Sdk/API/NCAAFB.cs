﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;

namespace Sportradar.Sdk.API
{
    /// <summary>
    /// Implementation of the NCAA Football API
    /// </summary>
    public static class NCAAFB
    {
        private static string scheduleURL = "ncaafb-t1/{year}/REG/schedule.json?api_key={apiKey}";
        private static string boxscoreURL = "ncaafb-t1/{year}/REG/{week}/{away}/{home}/boxscore.json?api_key={apiKey}";
        private static string weeklyscheduleURL = "ncaafb-t1/{year}/REG/{week}/schedule.json?api_key={apiKey}";

        public static int GetSeasonYear()
        {
            int year = DateTime.Now.Year - 1;

            if (DateTime.Now.Month >= 8 && DateTime.Now.Month < 1)
                year = DateTime.Now.Year;

            return year;
        }

        public static dynamic GetSchedule(int? year)
        {
            var client = new RestClient("http://api.sportradar.us");
            client.AddHandler("application/json", new DynamicJsonDeserializer());

            var request = new RestRequest(scheduleURL, Method.GET);
            request.AddUrlSegment("apiKey", System.Configuration.ConfigurationManager.AppSettings["sportsradar.apikey.ncaafb"]);
            request.AddUrlSegment("year", year.HasValue ? year.Value.ToString() : GetSeasonYear().ToString());

            var response = client.Execute<JObject>(request).Data.ToObject<dynamic>();
            return response;
        }

        public static dynamic GetWeeklySchedule(int? year,int week)
        {
            var client = new RestClient("http://api.sportradar.us");
            client.AddHandler("application/json", new DynamicJsonDeserializer());

            var request = new RestRequest(weeklyscheduleURL, Method.GET);
            request.AddUrlSegment("apiKey", System.Configuration.ConfigurationManager.AppSettings["sportsradar.apikey.ncaafb"]);
            request.AddUrlSegment("year", year.HasValue ? year.Value.ToString() : GetSeasonYear().ToString());
            request.AddUrlSegment("week", week.ToString());

            var response = client.Execute<JObject>(request).Data.ToObject<dynamic>();
            return response;
        }

        public static dynamic GetBoxScore(int? year,int week, string homeTeam, string awayTeam)
        {
            var client = new RestClient("http://api.sportradar.us");
            client.AddHandler("application/json", new DynamicJsonDeserializer());
            var request = new RestRequest(boxscoreURL, Method.GET);

            request.AddUrlSegment("apiKey", System.Configuration.ConfigurationManager.AppSettings["sportsradar.apikey.ncaafb"]);
            request.AddUrlSegment("year", year.HasValue ? year.Value.ToString() : GetSeasonYear().ToString());
            request.AddUrlSegment("week", week.ToString());
            request.AddUrlSegment("away", awayTeam);
            request.AddUrlSegment("home", homeTeam);

            var response = client.Execute<JObject>(request).Data.ToObject<dynamic>();
            return response;
        }

        public static Result GetWinnerAndScore(dynamic score)
        {
            Result result = new Result();

            if (score.status == "complete" || score.status == "closed")
            {
                result.FinalScore = string.Format("{0} {1} - {2} {3}", score.home_team.id, score.home_team.points, score.away_team.id, score.away_team.points);

                if (score.home_team.points > score.away_team.points)
                    result.Winner= score.home_team.id;

                if (score.away_team.points > score.home_team.points)
                    result.Winner = score.away_team.id;

            }
         

            return result;
        }
    }
}
