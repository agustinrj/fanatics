﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sportradar.Sdk
{
    public class Result
    {
        public string Winner { get; set; }
        public string FinalScore { get; set; }
    }
}
