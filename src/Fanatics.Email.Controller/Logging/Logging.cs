﻿using Fanatics.Email.Controller.ETClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Controller.Logging
{
    public class Logging:BaseController
    {
        public void SendErrorEmail(string error)
        {
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);
            string adminEmail = ConfigurationManager.AppSettings["WorkflowTool.AdminEmail"].ToString();

            string requestID = "";
            string status = "";

            ETClient.SoapClient etClient = GetClient();

            TriggeredSendDefinition definition = new TriggeredSendDefinition();
            definition.CustomerKey = "Error_Logging";

            //subscriber to whom email will be sent
            Subscriber subscriber = new Subscriber();
            subscriber.EmailAddress = adminEmail;
            subscriber.SubscriberKey = adminEmail;

            TriggeredSend send = new TriggeredSend();

            send.TriggeredSendDefinition = definition;

            //If passing Full HTML_Body, pass value to HTML__Body (This attribute should exist in account)
            ETClient.Attribute attribute1 = new ETClient.Attribute();
            attribute1.Name = "Body";
            attribute1.Value = error;
           
            subscriber.Attributes = new ETClient.Attribute[] { attribute1 };
            send.Subscribers = new Subscriber[] { subscriber };

            APIObject[] sends = { send };
            String requestId = null;
            String overAllStatus = null;
            
            CreateResult[] results = etClient.Create(new CreateOptions(), sends, out requestId, out overAllStatus);
        }
    }
}
