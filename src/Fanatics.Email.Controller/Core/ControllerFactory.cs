﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Controller.Core
{
    public class ControllerFactory
    {
        private Auth _auth;
        private BusinessUnit _businessUnit;
        private EmailSendDefinition _emailSendDefinition;
        private Send _send;
        private Folder _folder;
        private HubExchange _hubExchange;
        private DataExtension _dataExtension;
        private Campaign _campaign;
        private Email _email;

        public Email Email
        {
            get
            {
                if (_email == null)
                    _email = new Email();
                return _email;
            }
        }
        
        public Auth Auth
        {
            get
            {
                if (_auth == null)
                    _auth = new Auth();
                return _auth;
            }
        }

        public BusinessUnit BusinessUnit
        {
            get
            {
                if (_businessUnit == null)
                    _businessUnit = new BusinessUnit();
                return _businessUnit;
            }
        }

        public EmailSendDefinition EmailSendDefinition
        {
            get
            {
                if (_emailSendDefinition == null)
                    _emailSendDefinition = new EmailSendDefinition();
                return _emailSendDefinition;
            }
        }

        public Send Send
        {
            get
            {
                if (_send == null)
                    _send = new Send();
                return _send;
            }
        }

        public Folder Folder
        {
            get
            {
                if (_folder == null)
                    _folder = new Folder();
                return _folder;
            }
        }

        public HubExchange HubExchange
        {
            get
            {
                if (_hubExchange == null)
                    _hubExchange = new HubExchange();
                return _hubExchange;
            }
        }

        public DataExtension DataExtension
        {
            get
            {
                if (_dataExtension == null)
                    _dataExtension = new DataExtension();
                return _dataExtension;
            }
        }

        public Campaign Campaign
        {
            get
            {
                if (_campaign == null)
                    _campaign = new Campaign();
                return _campaign;
            }
        }

    }
}
