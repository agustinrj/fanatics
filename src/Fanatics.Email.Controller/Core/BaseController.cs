﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Fanatics.Email.Model.HubExchange;
using System.Xml.Linq;
using System.ServiceModel.Channels;

namespace Fanatics.Email.Controller
{
    public class BaseController
    {
        public ETClient.SoapClient GetClient()
        {
            ETClient.SoapClient client = new ETClient.SoapClient();
            
            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["ETUserName"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["ETPassword"];
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings["ETEndPoint"]);
            
            return client;
        }

        public ETClient.SoapClient GetNewClient()
        {
            ETClient.SoapClient client = new ETClient.SoapClient();

            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NewETUserName"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NewETPassword"];
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings["ETEndPoint"]);

            return client;
        }
    }
}
