﻿using Fanatics.Email.Controller.ETClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Controller
{
    public class Folder:BaseController
    {
        public Model.Folder GetByMidContentTypeName(int mid,bool isNew,string contentType,string name)
        {
            Model.Folder folder = null;

            ETClient.SoapClient etClient = isNew ? GetNewClient() : GetClient();

            string requestID;
            string status;
            APIObject[] results;

            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "ContentType";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { contentType };

            SimpleFilterPart rf = new SimpleFilterPart();
            rf.Property = "Name";
            rf.SimpleOperator = SimpleOperators.equals;
            rf.Value = new string[] { name };

            ComplexFilterPart cfp = new ComplexFilterPart();
            cfp.LeftOperand = sfp;
            cfp.LogicalOperator = LogicalOperators.AND;
            cfp.RightOperand = rf;

            RetrieveRequest rr = new RetrieveRequest();

            rr.ObjectType = "DataFolder";
            rr.Properties = new string[] { "ID", "Name"};
            rr.Filter = cfp;

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            status = etClient.Retrieve(rr, out requestID, out results);

            if (results.Count() > 0)
            {
                DataFolder df = (DataFolder)results[0];
                folder = new Model.Folder();
                folder.ID = df.ID;
            }

            return folder;
        }

        public int GetCreateFolderID(int mid, string folderName, string folderType, int parentFolderID, bool isNewBU)
        {
            DataFolder datafolder = new DataFolder();

            ETClient.SoapClient etClient = new ETClient.SoapClient();
            etClient.ClientCredentials.UserName.UserName = isNewBU ? ConfigurationManager.AppSettings["NewETUserName"] : ConfigurationManager.AppSettings["ETUserName"];
            etClient.ClientCredentials.UserName.Password = isNewBU ? ConfigurationManager.AppSettings["NewETPassword"] : ConfigurationManager.AppSettings["ETPassword"];
            etClient.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings["ETEndPoint"]);
            
            String requestID;
            String status;
            APIObject[] results;

            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "Name";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { folderName };

            SimpleFilterPart rf = new SimpleFilterPart();
            rf.Property = "ParentFolder.ID";
            rf.SimpleOperator = SimpleOperators.equals;
            rf.Value = new string[] { parentFolderID.ToString() };

            ComplexFilterPart cfp = new ComplexFilterPart();
            cfp.LeftOperand = sfp;
            cfp.LogicalOperator = LogicalOperators.AND;
            cfp.RightOperand = rf;

            RetrieveRequest rr = new RetrieveRequest();

            rr.ObjectType = "DataFolder";
            rr.Properties = new string[] { "ID", "Name", "ParentFolder.ID", "ParentFolder.Name" };
            rr.Filter = cfp;

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            status = etClient.Retrieve(rr, out requestID, out results);

            //if folder doesn't exist, create it and return id.
            if (results.Count() > 0)
                datafolder = (DataFolder)results[0];
            else
            {
                String requestIDFolder;
                String statusFolder;
                CreateOptions createOptions = new CreateOptions();

                createOptions.Client = clientIDs[0];

                datafolder.Name = folderName;
                datafolder.Client = clientIDs[0];
                datafolder.Description = "";
                datafolder.AllowChildren = true;
                datafolder.AllowChildrenSpecified = true;
                datafolder.IsActive = true;
                datafolder.IsActiveSpecified = true;
                datafolder.IsEditable = true;
                datafolder.IsEditableSpecified = true;
                datafolder.ParentFolder = new DataFolder();
                datafolder.ParentFolder.Client = clientIDs[0];
                datafolder.ParentFolder.ID = parentFolderID; 
                datafolder.ParentFolder.IDSpecified = true;
                datafolder.ContentType = folderType;

                CreateResult[] cresults = etClient.Create(createOptions, new APIObject[] { datafolder }, out requestIDFolder, out statusFolder);
                datafolder.ID = cresults[0].NewID;
            }

            return datafolder.ID;
        }
    }
}
