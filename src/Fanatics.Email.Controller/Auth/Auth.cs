﻿using Fanatics.Email.Controller.ETClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Controller
{
    public class Auth:BaseController
    {
        /// <summary>
        /// Verifies user credentials against the ESP
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool Login(string userName, string password)
        {
            try
            {
                ETClient.SoapClient etClient = new ETClient.SoapClient();
                etClient.ClientCredentials.UserName.UserName = userName;
                etClient.ClientCredentials.UserName.Password = password;
                etClient.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings["ETEndPoint"]);

                int mid = 0;
                int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

                string requestID = "";
                string status = "";
                APIObject[] results;

                RetrieveRequest rr = new RetrieveRequest();
                rr.ObjectType = "AccountUser";

                rr.Properties =
                    new string[]
                        {
                           "UserID",
                           "Password"
                        };

                SimpleFilterPart filter = new SimpleFilterPart();
                filter.Property = "UserID";
                filter.SimpleOperator = SimpleOperators.equals;
                filter.Value = new string[] { userName };
                rr.Filter = filter;

                ClientID[] client = new ClientID[1];
                client[0] = new ClientID();
                client[0].ClientID1 = mid;
                client[0].ClientID1Specified = true;
                rr.ClientIDs = client;

                // Execute RetrieveRequest
                status = etClient.Retrieve(rr, out requestID, out results);

                return status.ToLower().Equals("ok");
            }
            catch (FaultException)
            {
                return false;
            }
        }


        /// <summary>
        /// Verifies user credentials against the ESP
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool LoginByTokenUserId(string token, string userId)
        {
            try
            {
                HubExchange hub = new HubExchange();
                var tokenContext = hub.GetTokenContext(token);
                return tokenContext.user.id == int.Parse(userId);
            }
            catch (FaultException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns an account by a given username.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public Model.Account GetByUserName(string userName) 
        {
            Model.Account account = null;

            ETClient.SoapClient etClient = new ETClient.SoapClient();
            etClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["ETUserName"];
            etClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["ETPassword"];
            etClient.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings["ETEndPoint"]);

            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            string requestID = "";
            string status = "";
            APIObject[] results;

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "AccountUser";

            rr.Properties =
                new string[]
                        {
                           "UserID",
                           "Password"
                        };

            SimpleFilterPart filter = new SimpleFilterPart();
            filter.Property = "UserID";
            filter.SimpleOperator = SimpleOperators.equals;
            filter.Value = new string[] { userName };
            rr.Filter = filter;

            ClientID[] client = new ClientID[1];
            client[0] = new ClientID();
            client[0].ClientID1 = mid;
            client[0].ClientID1Specified = true;
            rr.ClientIDs = client;

            // Execute RetrieveRequest
            status = etClient.Retrieve(rr, out requestID, out results);

            if (status.ToLower().Equals("ok") && results.Count() > 0)
            {
                var a = (AccountUser)results[0];
                account = new Model.Account() { UserName = a.UserID, Password = a.Password };
            }
            else
            {
                if (results.Count() == 0)
                {
                    ETClient.SoapClient newEtClient = GetNewClient();
                    status = newEtClient.Retrieve(rr, out requestID, out results);

                    if (status.ToLower().Equals("ok") && results.Count() > 0)
                    {
                        var a = (AccountUser)results[0];
                        account = new Model.Account() { UserName = a.UserID, Password = a.Password };
                    }
                    else
                        throw new Exception("User not found");
                }
            }

            return account;
        }

        /// <summary>
        /// Returns an account by a given username.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public Model.Account GetByAccountUserID(string accountUserID)
        {
            Model.Account account = null;

            ETClient.SoapClient etClient = GetClient();
         
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            string requestID = "";
            string status = "";
            APIObject[] results;

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "AccountUser";

            rr.Properties =
                new string[]
                        {
                           "UserID",
                           "Password"
                        };

            SimpleFilterPart filter = new SimpleFilterPart();
            filter.Property = "AccountUserID";
            filter.SimpleOperator = SimpleOperators.equals;
            filter.Value = new string[] { accountUserID };
            rr.Filter = filter;

            ClientID[] client = new ClientID[1];
            client[0] = new ClientID();
            client[0].ClientID1 = mid;
            client[0].ClientID1Specified = true;
            rr.ClientIDs = client;

            // Execute RetrieveRequest
            status = etClient.Retrieve(rr, out requestID, out results);

            if (status.ToLower().Equals("ok") && results.Count() > 0)
            {
                var a = (AccountUser)results[0];
                account = new Model.Account() { UserName = a.UserID, Password = a.Password };
            }
            else
            {
                if (results.Count() == 0)
                {
                    ETClient.SoapClient newEtClient = GetNewClient();
                    client[0].ClientID1 = 10888620;
                    rr.ClientIDs = client;
                    status = newEtClient.Retrieve(rr, out requestID, out results);

                    if (status.ToLower().Equals("ok") && results.Count() > 0)
                    {
                        var a = (AccountUser)results[0];
                        account = new Model.Account() { UserName = a.UserID, Password = a.Password };
                    }
                    else
                        throw new Exception("User not found");
                }
            }

            return account;
        }

        /// <summary>
        /// Temporary method, updates record with token in xadminauthx DE
        /// Intended to be deprecated once Dashboard is fully migrated
        /// </summary>
        /// <param name="token"></param>
        /// <param name="username"></param>
        public void StoreTokenForUser(string token, string username) 
        {
            ETClient.SoapClient etClient = GetClient();

            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            string requestID = "";
            string status = "";
            UpdateResult[] results;

            DataExtensionObject DEO = new DataExtensionObject();
            DEO.CustomerKey = "xadminauthx";

            APIProperty tokenProp = new APIProperty();
            tokenProp.Name = "oauthToken";
            tokenProp.Value = token;

            APIProperty usernameProp = new APIProperty();
            usernameProp.Name = "username";
            usernameProp.Value = username;

            DEO.Properties = new APIProperty[] { tokenProp,usernameProp};

            ClientID client = new ClientID();
            client.ClientID1 = mid;
            client.ClientID1Specified = true;
            
            UpdateOptions uo = new UpdateOptions();
            uo.Client = client;
            uo.SaveOptions = new SaveOption[1];
            uo.SaveOptions[0] = new SaveOption() { PropertyName = "*", SaveAction = SaveAction.UpdateAdd };
            
            results = etClient.Update(uo, new APIObject[] { DEO }, out requestID, out status);

            if (!status.ToLower().Equals("ok"))
                throw new Exception("Could not update xadminauthx DE");
        }
    }
}
