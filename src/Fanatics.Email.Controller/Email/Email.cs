﻿using Fanatics.Email.Controller.ETClient;
using Fanatics.Email.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Fanatics.Email.Controller
{
    public class Email
    {
       
        public ETClient.Email GetByCustomerKey(int mid, string customerKey, bool isNewBU)
        {
            ETClient.Email email = null;
            ETClient.SoapClient etClient = new ETClient.SoapClient();
            etClient.ClientCredentials.UserName.UserName = isNewBU ? ConfigurationManager.AppSettings["NewETUserName"] : ConfigurationManager.AppSettings["ETUserName"];
            etClient.ClientCredentials.UserName.Password = isNewBU ? ConfigurationManager.AppSettings["NewETPassword"] : ConfigurationManager.AppSettings["ETPassword"];
            etClient.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings["ETEndPoint"]);

            string requestID = "";
            string status = "";
            APIObject[] results;

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "Email";

            rr.Properties =
                new string[]
                        {
                           "Name",
                           "CustomerKey"
                        };

            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "CustomerKey";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { customerKey };

            rr.Filter = sfp;

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            status = etClient.Retrieve(rr, out requestID, out results);

            if (status.ToLower().Equals("ok"))
            {
                email = (ETClient.Email)results[0];
                email.Client = clientIDs[0];
            }

            return email;
        }

        public Fanatics.Email.Model.Email GetEmailByName(string name, int mid, bool isNewBU)
        {
            String requestID;
            APIObject[] results;
            ETClient.Email email = null;
           

            ETClient.SoapClient etClient = new ETClient.SoapClient();
            etClient.ClientCredentials.UserName.UserName = isNewBU ? ConfigurationManager.AppSettings["NewETUserName"]  : ConfigurationManager.AppSettings["ETUserName"];
            etClient.ClientCredentials.UserName.Password = isNewBU ? ConfigurationManager.AppSettings["NewETPassword"] : ConfigurationManager.AppSettings["ETPassword"];
            etClient.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings["ETEndPoint"]);

            if(mid == 0)
                int.TryParse(ConfigurationManager.AppSettings["ETClient"].ToString(), out mid);

            // Filter by the Folder/Category 
            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "Name";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { name };

            // Create the RetrieveRequest object

            RetrieveRequest request = new RetrieveRequest();
            request.ObjectType = "Email";
            request.Filter = sfp;
            request.Properties = new string[] { "CategoryID", "ID", "HTMLBody", "PreHeader", "Name", "CharacterSet", "CreatedDate", "ModifiedDate", "Subject", "HasDynamicSubjectLine" };

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            request.ClientIDs = clientIDs;

            // Execute the Retrieve
            string status = etClient.Retrieve(request, out requestID, out results);

            if (results.Length > 1)
                throw new Exception(string.Format("More than one email found with the name: {0}", name));

            if (results.Length > 0)
                email = (ETClient.Email)results[0];

            if (email != null)
                return new Fanatics.Email.Model.Email() { Name = email.Name, HTMLBody = email.HTMLBody, Subject = email.Subject, PreHeader = email.PreHeader, ID = email.ID };
            else
                return null;
        }

        public dynamic CreateFromCampaignRequest(EmailSendCreateRequest request)
        {
            Controller.Campaign campaignController = new Campaign();

            ETClient.SoapClient etClient = new ETClient.SoapClient();
            etClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["ETUserName"];
            etClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["ETPassword"];
            // TODO: remove this once everything is in new environment
            if (request.IsNewBU)
            {
                etClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NewETUserName"];
                etClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NewETPassword"];
            }

            etClient.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings["ETEndPoint"]);

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = request.MID;
            clientIDs[0].IDSpecified = true;

            Fanatics.Email.Model.Email email = null;

            // if it's an overwrite first we need to remove the SFMC objects associated with the campaign
            if (request.Overwrite)
                campaignController.DeleteSFMCObjects(request.MID, request.Campaign.Id, "email",request.IsNewBU);

            // try to find email, if it doesn't exist then create it. 
            email = GetEmailByName(request.Campaign.EmailName,request.MID,request.IsNewBU);

            if (email == null)
            {
                List<APIObject> objects = new List<APIObject>();
                List<APIObject> deObjects = new List<APIObject>();

                // get the template
                var template = GetEmailByName(request.Campaign.TemplateSFMCName,0,false);

                if(template == null)
                    throw new Exception(string.Format("template {0} not found", request.Campaign.TemplateSFMCName));

                // create the new email, replace the content with the template fields provided
                string emailBody = template.HTMLBody;

                foreach (var item in request.Campaign.TemplateConfiguration)
                    emailBody = emailBody.Replace(string.Format("[[{0}]]", item.ContentName), item.ContentValue);

                // replace cname and cnumber
                if (!string.IsNullOrEmpty(request.Campaign.CName))
                    emailBody = emailBody.Replace("[[CNAME]]", request.Campaign.CName);

                if (!string.IsNullOrEmpty(request.Campaign.CNumber))
                    emailBody = emailBody.Replace("[[CNUMBER]]", request.Campaign.CNumber);

                if (!string.IsNullOrEmpty(request.Campaign.PreheaderURL))
                    emailBody = emailBody.Replace("[[PREHEADERURL]]", request.Campaign.PreheaderURL);

                var r = new Regex(@"<contents>[\s\S]*?<\/contents>");
                var xml = r.Match(emailBody).Value;

                // remove all the xml that is part of the template from email body.
                emailBody = emailBody.Replace(xml, "");

                ETClient.Email newEmail = new ETClient.Email();
                var customerKey = Guid.NewGuid().ToString();
                newEmail.CustomerKey = customerKey;
                newEmail.Subject = request.Campaign.SubjectLine;
                newEmail.PreHeader = request.Campaign.Preheader;
                newEmail.Name = request.Campaign.EmailName;
                newEmail.HTMLBody = emailBody;
                newEmail.CharacterSet = "utf-8";
                newEmail.CategoryID = GetEmailFolderID(request);
                newEmail.CategoryIDSpecified = true;
                newEmail.IsHTMLPaste = true;
                newEmail.IsHTMLPasteSpecified = true;

                newEmail.Client = new ClientID();
                newEmail.Client.ID = request.MID;
                newEmail.Client.IDSpecified = true;

                objects.Add(newEmail);

                // create row in Campaigns_SFMCObjects DE
                deObjects.Add(campaignController.GetSFMCObjectRow(request.Campaign.Id, customerKey, "email"));

                // submit API call
                string cRequestID = "";
                string cStatus = "";
                CreateResult[] cResults = etClient.Create(new CreateOptions(), objects.ToArray(), out cRequestID, out cStatus);

                if (cStatus.ToLower() != "ok")
                    throw new Exception(cResults[0].StatusMessage);

                UpdateOptions updateOptions = new UpdateOptions();
                SaveOption saveOption = new SaveOption();

                saveOption.SaveAction = SaveAction.UpdateAdd;
                saveOption.PropertyName = "DataExtensionObject";
                updateOptions.SaveOptions = new SaveOption[] { saveOption };

                int mid = 0;
                int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

                ClientID clientID = new ClientID();
                clientID.ID = mid;
                clientID.IDSpecified = true;
                updateOptions.Client = clientID;

                // TODO: remove this once everything is in new environment
                ETClient.SoapClient etClientSource = new ETClient.SoapClient();
                etClientSource.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["ETUserName"];
                etClientSource.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["ETPassword"];
                
                UpdateResult[] uResults = etClientSource.Update(updateOptions, deObjects.ToArray(), out cRequestID, out cStatus);

                if (cStatus.ToLower() != "ok")
                    throw new Exception(uResults[0].StatusMessage);
            }
            else
                throw new Exception(string.Format("An email with the name {0} already exists on MID {1}", request.Campaign.EmailName, request.MID));

            return new { Status = "OK" };
        }

        private int GetEmailFolderID(EmailSendCreateRequest request)
        {
            Controller.Folder folderController = new Folder();

            var folderType = "email";

            // folder is Year - Month - day of deployment date
            var year = request.Campaign.DeploymentDate.Year.ToString();
            var month = request.Campaign.DeploymentDate.Month.ToString();
            var day = request.Campaign.DeploymentDate.Day.ToString();
                        
            var yearFolderID = folderController.GetCreateFolderID(request.MID, year, folderType, request.EmailsFolderId,request.IsNewBU);
            var monthFolderID = folderController.GetCreateFolderID(request.MID, month, folderType, yearFolderID, request.IsNewBU);
            var dayFolderID = folderController.GetCreateFolderID(request.MID, day, folderType, monthFolderID, request.IsNewBU);

            return dayFolderID;
        }
    }
}
