﻿using Fanatics.Email.Controller.ETClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Controller
{
    public class BusinessUnit:BaseController
    {
        /// <summary>
        /// Returns a list of business units for a specific account
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public List<Model.BusinessUnit> GetByAccount(Model.Account account)
        {
            List<Model.BusinessUnit> buList = new List<Model.BusinessUnit>();

            ETClient.SoapClient etClient = new ETClient.SoapClient();
            etClient.ClientCredentials.UserName.UserName = account.UserName;
            etClient.ClientCredentials.UserName.Password = account.Password;
            etClient.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings["ETEndPoint"]);

            string requestID = "";
            string status = "";
            APIObject[] results;

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "Account";
            rr.QueryAllAccounts = true;
            rr.QueryAllAccountsSpecified = true;

            rr.Properties =
                new string[]
                        {
                           "ID","Name"
                        };

            // Execute RetrieveRequest
            status = etClient.Retrieve(rr, out requestID, out results);

            if(status.ToLower().Equals("ok"))
            {
                foreach (Account a in results)
                    buList.Add(new Model.BusinessUnit() { ID = a.ID, Name = a.Name }); 
            }

            return buList;
        }

        /// <summary>
        /// Returns a list of business units for a specific account
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public List<Model.BusinessUnit> Get()
        {
            List<Model.BusinessUnit> buList = new List<Model.BusinessUnit>();

            buList.AddRange(GetBus(false));
            buList.AddRange(GetBus(true));

            return buList;
        }

        public List<Model.BusinessUnit> GetBus(bool isNew)
        {
            List<Model.BusinessUnit> buList = new List<Model.BusinessUnit>();

            ETClient.SoapClient etClient = isNew ? GetNewClient() : GetClient();

            string requestID = "";
            string status = "";
            APIObject[] results;

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "Account";
            rr.QueryAllAccounts = true;
            rr.QueryAllAccountsSpecified = true;

            rr.Properties =
                new string[]
                        {
                           "ID","Name"
                        };

            // Execute RetrieveRequest
            status = etClient.Retrieve(rr, out requestID, out results);

            if (status.ToLower().Equals("ok"))
            {
                foreach (Account a in results)
                    buList.Add(new Model.BusinessUnit() { ID = a.ID, Name = a.Name,IsNew = isNew });
            }

            return buList;
        }
    }
}
