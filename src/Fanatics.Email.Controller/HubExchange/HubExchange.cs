﻿using Fanatics.Email.Model.HubExchange;
using JWT;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;

namespace Fanatics.Email.Controller
{
    public class HubExchange
    {
        public dynamic Login(string jwt)
        {

            // Decoded request using JWT.cs in the project that leverages https://github.com/johnsheehan/jwt.
#if Release
                   String decodedJWT = JsonWebToken.Decode(jwt, "4kkih5v3jx5ijbekk021tcyi0tnsxxugjkzeja345xhqk3xpgcbvstihz0rxosk40jcfdf4nhe3lmhqyigjcjsmjyekcu2cii0qch3v1jidwto1uhkas2hbtces0zytlg2240dklfbxpbbnuzwtdqgasocjyvb1iupkgsode5mz51zsj2s5vwgudsjbb5r5uidhsi4fkimdcqihzqzqgam1hsyzmkezyzj4uz1ingjgxqyzavib5iidsvfdpluq");
#else
            String decodedJWT = JsonWebToken.Decode(jwt, "4kkih5v3jx5ijbekk021tcyi0tnsxxugjkzeja345xhqk3xpgcbvstihz0rxosk40jcfdf4nhe3lmhqyigjcjsmjyekcu2cii0qch3v1jidwto1uhkas2hbtces0zytlg2240dklfbxpbbnuzwtdqgasocjyvb1iupkgsode5mz51zsj2s5vwgudsjbb5r5uidhsi4fkimdcqihzqzqgam1hsyzmkezyzj4uz1ingjgxqyzavib5iidsvfdpluq");
#endif

            // Store encoded and decoded JWT in sesssion variables for step 1 display only.
            SessionManager.EncodedJWT = jwt.Trim();
            SessionManager.DecodedJWT = decodedJWT.Trim();

            // Parsed request into JSON object using Newtonsoft.Json.Linq.

            var response = System.Web.Helpers.Json.Decode(decodedJWT);

            SessionManager.InternalOauthToken = response.request.user.internalOauthToken;
            SessionManager.OAuthToken = response.request.user.oauthToken;
            SessionManager.RefreshToken = response.request.user.refreshToken;

            SessionManager.TokenRequest = DateTime.Now;

            var tokenContext = GetTokenContext(response.request.user.oauthToken);

            //retrieve token context for loggin purposes.
            if (tokenContext != null)
            {
                SessionManager.userId = Convert.ToString(tokenContext.user.id);
                SessionManager.MID = Convert.ToString(tokenContext.organization.id);
                
            }

            //Get EndPoint URL for SOAP API calls
            SessionManager.SOAPEndPoint = GetSoapEndPoint(SessionManager.OAuthToken);

            //Login User into Fanatics User Data Extension
            //Get User Name First

            Auth authController = new Auth();
            var account = authController.GetByAccountUserID(SessionManager.userId);

            if (account == null)
                throw new Exception("Account not found");
            else
            {
                //Log user name into data extension...
                //authController.StoreTokenForUser(SessionManager.OAuthToken,account.UserName);
                
                SessionManager.UserName = account.UserName;
            }

            return response;
        }

        /// <summary>
        /// Calls https://www.exacttargetapis.com/platform/v1/tokenContext/
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public dynamic GetTokenContext(String accessToken)
        {
            try
            {
                String strSOAPEndPoint = String.Empty;

                string strURL = "https://www.exacttargetapis.com/platform/v1/tokenContext/?access_token=" + accessToken.Trim();
                string strResponse = RESTPerform(strURL, null, "GET");

                //Log4NetLogger log2 = new Log4NetLogger();
                //log2.Debug(strResponse);

                return Json.Decode(strResponse);
            }
            catch (Exception ex)
            {
                //Log4NetLogger log = new Log4NetLogger();
                //log.Error("GetTokenContext Error", ex);
                throw ex;
            }
        }

        /// <summary>
        /// calls https://www.exacttargetapis.com/platform/v1/endpoints/soap
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public String GetSoapEndPoint(String accessToken)
        {
            try
            {
                String strSOAPEndPoint = String.Empty;

                // Set REST URL and make call documented at code.exacttarget.com/devcenter/fuel-api-family/platform/endpoints/retrieveendpointsbytype.
                string strURL = "https://www.exacttargetapis.com/platform/v1/endpoints/soap?access_token=" + accessToken.Trim();
                string strResponse = RESTPerform(strURL, null, "GET");

                var response = Json.Decode(strResponse);

                return response.url;
            }
            catch (Exception ex)
            {
                //Log4NetLogger log = new Log4NetLogger();
                //log.Error("GetSoapEndPoint Error", ex);
                throw ex;
            }
        }

        // Create a request using a URL that can receive a post. 
        public string RESTPerform(string strURL, string parameters, string method)
        {
            try
            {
                // Build the request.
                WebRequest request = WebRequest.Create(strURL.Trim());
                request.Method = method;
                request.ContentType = "application/json";

                if (parameters != null)
                {
                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        

                        //Log4NetLogger log = new Log4NetLogger();
                        //log.Debug(json);

                        streamWriter.Write(parameters);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                }

                // Get the response.
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();

                // Return the response.
                return responseFromServer.Trim();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Calls https://www.exacttargetapis.com/platform/v1/requestToken/
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public dynamic GetAccessToken(String accessToken, String internalOauthToken, String refreshToken, bool isLegacy)
        {
            try
            {
                StringBuilder jsonParameters = new StringBuilder();
                jsonParameters.Append("{");
                
                jsonParameters.AppendFormat("\"clientID\":\"{0}\"", ConfigurationManager.AppSettings["appClientID"]);
                jsonParameters.AppendFormat("\"clientSecret\":\"{0}\"", ConfigurationManager.AppSettings["appClientSecret"]);

                if (!string.IsNullOrEmpty(refreshToken))
                    jsonParameters.AppendFormat("\"refreshToken\":\"{0}\"", refreshToken);

                if (isLegacy)
                {
                    jsonParameters.Append("\"accessType\":\"offline\"");
                    jsonParameters.AppendFormat("\"scope\":\"{0}\"", "cas:" + internalOauthToken);
                }

                jsonParameters.Append("}");

                String strSOAPEndPoint = String.Empty;

                string strURL = "https://auth.exacttargetapis.com/v1/requestToken" + (isLegacy ? "?legacy=1" : "");
                string strResponse = RESTPerform(strURL, jsonParameters.ToString(), "POST");


                return Json.Decode(strResponse);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }              
    }
}
