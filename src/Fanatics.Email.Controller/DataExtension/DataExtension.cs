﻿using Fanatics.Email.Controller.ETClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Fanatics.Email.Controller
{
    public class DataExtension:BaseController
    {
        public Model.DataExtension GetByExternalKeyMID(string externalKey,bool isNew, int mid,SimpleFilterPart filter)
        {
            Regex intRegex = new Regex(@"^\d+$");
            Model.DataExtension de = new Model.DataExtension();
            de.ExternalKey = externalKey;

            string requestID = "";
            string status = "";
            APIObject[] results;

            ETClient.SoapClient etClient = isNew ? GetNewClient() : GetClient();

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = string.Format("DataExtensionObject[{0}]", externalKey);

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            var fields = GetFieldsByExternalKeyMID(externalKey,isNew, mid);

            List<string> props = new List<string>();

            foreach (Model.DataExtensionField field in fields)
            {
                if (field.IsPrimaryKey)
                    de.HasPrimaryKey = true;

                if (field.Name.ToLower() != "parametersused")
                    props.Add(field.Name);
            }
            
            rr.Properties = props.ToArray();

            if (filter != null)
                rr.Filter = filter;

            status = etClient.Retrieve(rr, out requestID, out results);
            if (!status.ToLower().Equals("ok") && !status.ToLower().Equals("moredataavailable"))
            {
                throw new Exception(status);
            }
            else
            {
                while (status.ToLower().Equals("moredataavailable") || status.ToLower().Equals("ok"))
                {
                    foreach (DataExtensionObject deObject in results)
                    {
                        IDictionary<String, object> row = new Dictionary<String, object>(StringComparer.OrdinalIgnoreCase );

                        foreach (var prop in deObject.Properties)
                        {
                            if (prop.Name.IndexOf("_CustomObjectKey") < 0)
                            {
                                if (intRegex.IsMatch(prop.Value))
                                    row.Add(prop.Name, int.Parse(prop.Value));
                                else
                                    row.Add(prop.Name, prop.Value);
                            }
                        }

                        if(de.HasPrimaryKey)
                            row.Add("Actions", "");
                        else
                            row.Add("Actions", "none");

                        de.Rows.Add(row);
                    }

                    if (status.ToLower().Equals("moredataavailable"))
                    {
                        rr.ContinueRequest = requestID;
                        status = etClient.Retrieve(rr, out requestID, out results);
                    }
                    else
                        status = "complete";
                }
            }

            return de;
        }

        public List<Model.DataExtensionField> GetFieldsByExternalKeyMID(string externalKey,bool isNew, int mid)
        {
            List<Model.DataExtensionField> fields = new List<Model.DataExtensionField>();

            string requestID = "";
            string status = "";
            APIObject[] results;

            ETClient.SoapClient etClient = isNew? GetNewClient() : GetClient();

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "DataExtensionField";

            rr.Properties =
                new string[]
                    {
                       "Name",
                       "FieldType",
                       "IsRequired",
                       "IsPrimaryKey",
                       "DefaultValue",
                       "MaxLength",
                       "Ordinal"
                    };

            SimpleFilterPart filter = new SimpleFilterPart();
            filter.Property = "DataExtension.CustomerKey";
            filter.SimpleOperator = SimpleOperators.equals;
            filter.Value = new string[] { externalKey };
            rr.Filter = filter;

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            status = etClient.Retrieve(rr, out requestID, out results);

            if (!status.ToLower().Equals("ok"))
            {
                throw new Exception(status);
            }
            else
            {
                 foreach (DataExtensionField field in results)
                 {
                     fields.Add(new Model.DataExtensionField()
                     {
                         Name = field.Name,
                         FieldType = field.FieldType.ToString(),
                         DefaultValue = field.DefaultValue,
                         IsPrimaryKey = field.IsPrimaryKey,
                         IsRequired = field.IsRequired,
                         MaxLength = field.MaxLength,
                         Ordinal = field.Ordinal
                     });
                 }
            }

            return fields;
        }

        public string Update(IDictionary<string, object> row, string externalKey, bool isNew,int mid)
        {
            bool updated = true;

            string requestID = "";
            string status = "";

            ETClient.SoapClient etClient = isNew ? GetNewClient() : GetClient();

            var fields = GetFieldsByExternalKeyMID(externalKey, isNew, mid);

            UpdateOptions updateOptions = new UpdateOptions();
            SaveOption saveOption = new SaveOption();

            saveOption.SaveAction = SaveAction.UpdateAdd;
            saveOption.PropertyName = "DataExtensionObject";
            updateOptions.SaveOptions = new SaveOption[]{ saveOption };

            ClientID clientID = new ClientID();
            clientID.ID = mid;
            clientID.IDSpecified = true;
            updateOptions.Client = clientID;

            DataExtensionObject deObject = new DataExtensionObject();
            List<APIProperty> keys = new List<APIProperty>();
            List<APIProperty> props = new List<APIProperty>();

            deObject.CustomerKey = externalKey;

            foreach (var field in fields)
            {
                if (row.ContainsKey(field.Name))
                {
                    APIProperty prop = new APIProperty();

                    if (row[field.Name] == "" && !field.IsRequired)
                        prop = new NullAPIProperty();
                    else
                        prop.Value = row[field.Name].ToString();

                    prop.Name = field.Name;

                    if (field.IsPrimaryKey)
                        keys.Add(prop);
                    
                        props.Add(prop);
                }
            }

            deObject.Keys = keys.ToArray();
            deObject.Properties = props.ToArray();

            UpdateResult[] updateResult = etClient.Update(updateOptions, new APIObject[]{deObject}, out requestID, out status);

            return status;
        }

        public string Delete(IDictionary<string, object> row, string externalKey, bool isNew,int mid)
        {
            bool deleted = true;

            string requestID = "";
            string status = "";

            ETClient.SoapClient etClient = isNew ? GetNewClient() : GetClient();

            var fields = GetFieldsByExternalKeyMID(externalKey,isNew, mid);

            DeleteOptions deleteOptions = new DeleteOptions();

            ClientID clientID = new ClientID();
            clientID.ID = mid;
            clientID.IDSpecified = true;
            deleteOptions.Client = clientID;

            DataExtensionObject deObject = new DataExtensionObject();
            List<APIProperty> keys = new List<APIProperty>();
            List<APIProperty> props = new List<APIProperty>();

            deObject.CustomerKey = externalKey;

            foreach (var field in fields)
            {
                if (row.ContainsKey(field.Name))
                {
                    APIProperty prop = new APIProperty();
                    prop.Name = field.Name;
                    prop.Value = row[field.Name].ToString();

                    if (field.IsPrimaryKey)
                        keys.Add(prop);
                }
            }

            deObject.Keys = keys.ToArray();

            DeleteResult[] deleteteResult = etClient.Delete(deleteOptions, new APIObject[] { deObject }, out requestID, out status);

            return status;
        }

        public List<dynamic> GetByCategoryID(int categoryID, bool isNewBU)
        {
            List<dynamic> dataExtensions = new List<dynamic>();

            string requestID = "";
            string status = "";
            APIObject[] results;

            ETClient.SoapClient etClient = isNewBU ? GetNewClient() : GetClient();

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "DataExtension";
            rr.Properties =
                new string[]
                    {
                        "ObjectID", "CustomerKey", "Name", "IsSendable", "SendableSubscriberField.Name"
                    };

            SimpleFilterPart filter = new SimpleFilterPart();
            filter.Property = "CategoryID";
            filter.SimpleOperator = SimpleOperators.equals;
            filter.Value = new string[] { categoryID.ToString() };
            rr.Filter = filter;
            rr.QueryAllAccounts = true;
            rr.QueryAllAccountsSpecified = true;
             status = etClient.Retrieve(rr, out requestID, out results);

             if (!status.ToLower().Equals("ok"))
             {
                 throw new Exception(status);
             }
             else
             {
                 foreach (var r in results)
                 {
                     var DE = (ETClient.DataExtension)r;
                    dataExtensions.Add(new { Name = DE.Name, CustomerKey = DE.CustomerKey });
                 }
             }

             return dataExtensions;
        }

        public Model.DataExtension GetByExternalKey(string externalKey, bool isNewBU)
        {
            Model.DataExtension retDE = null;

            string requestID = "";
            string status = "";
            APIObject[] results;

            ETClient.SoapClient etClient = isNewBU ? GetNewClient() :GetClient();

            RetrieveRequest rr = new RetrieveRequest();
            rr.QueryAllAccounts = true;
            rr.QueryAllAccountsSpecified = true;
            rr.ObjectType = "DataExtension";
            rr.Properties =
                new string[]
                    {
                        "ObjectID", "CustomerKey", "Name", "IsSendable", "SendableSubscriberField.Name"
                    };

            SimpleFilterPart filter = new SimpleFilterPart();
            filter.Property = "CustomerKey";
            filter.SimpleOperator = SimpleOperators.equals;
            filter.Value = new string[] { externalKey };
            rr.Filter = filter;
            rr.QueryAllAccounts = true;
            rr.QueryAllAccountsSpecified = true;

            status = etClient.Retrieve(rr, out requestID, out results);

            if (!status.ToLower().Equals("ok"))
            {
                throw new Exception(status);
            }
            else
            {
                if(results.Count() > 0)
                {
                    ETClient.DataExtension de = (ETClient.DataExtension)results[0];
                    retDE = new Model.DataExtension() { ExternalKey = externalKey, ObjectID = de.ObjectID };
                }
            }

            return retDE;
        }
    }
}
