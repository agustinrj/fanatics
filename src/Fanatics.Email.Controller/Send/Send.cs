﻿using Fanatics.Email.Controller.ETClient;
using Fanatics.Email.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Controller
{
    public class Send
    {
        public SoapClient GetClient(bool isNewBU)
        {
            ETClient.SoapClient etClient = new ETClient.SoapClient();
            if (isNewBU)
            {
                etClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NewETUserName"];
                etClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NewETPassword"];
            }
            else
            {
                etClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["ETUserName"];
                etClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["ETPassword"];
            }
            etClient.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings["ETEndPoint"]);

            return etClient;
        }

        public SendScheduleResult Schedule(int mid, string customerKey, string subjectLine, bool isNewBU)
        {
            int sendID = 0;

            // retrieve status of send, if pending or running do not schedule
            var send = GetPendingByMidCustomerKey(mid, customerKey,isNewBU);

            if (send != null && (send.Status.ToLower().Equals("scheduled") || send.Status.ToLower().Equals("sending")))
                throw new Exception("Send already scheduled");

            // retrieve send definition by customer key
            ETClient.SoapClient etClient = GetClient(isNewBU);

            string requestID = "";
            string status = "";
            string message = "";
            APIObject[] results;

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "EmailSendDefinition";

            rr.Properties =
                new string[]
                        {
                           "Name",
                           "CustomerKey",
                           "EmailSubject"
                        };

            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "CustomerKey";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { customerKey };

            rr.Filter = sfp;

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            status = etClient.Retrieve(rr, out requestID, out results);

            if (status.ToLower().Equals("ok"))
            {
                ETClient.EmailSendDefinition e = (ETClient.EmailSendDefinition)results[0];

                // if subject line has a value and is different from the original one, update send definition first
                if (!string.IsNullOrEmpty(subjectLine) && e.EmailSubject != subjectLine)
                {
                    e.EmailSubject = subjectLine;
                    
                    UpdateOptions uo = new UpdateOptions();
                    uo.Client = new ClientID();
                    uo.Client.ClientID1 = mid;
                    uo.Client.ClientID1Specified = true;
                    uo.Client.ID = mid;
                    uo.Client.IDSpecified = true;

                    e.Client = uo.Client;

                    UpdateResult[] r = etClient.Update(uo, new APIObject[] { e }, out requestID, out status);
                    if (!status.ToLower().Equals("ok"))
                        throw new Exception("Could not Update email send definition");
                }
                // schedule send definition 10 min from now
                ScheduleDefinition sd = new ScheduleDefinition();
                sd.StartDateTime = DateTime.Now.AddMinutes(10);
                sd.StartDateTimeSpecified = true;

                ScheduleOptions so = new ScheduleOptions();
                so.Client = new ClientID() { ClientID1 = mid, ClientID1Specified = true };

                ScheduleResult[] sResults = etClient.Schedule(so, "start", sd, new APIObject[] { e }, out status, out message, out requestID);

                sendID = int.Parse(sResults[0].Task.ID);
            }

            return new SendScheduleResult() { Message = "Send Scheduled", TaskID = sendID };
        }

        public string Test(int mid, string customerKey, string subjectLine, bool isNewBU)
        {
            // retrieve send definition by customer key
            ETClient.SoapClient etClient = GetClient(isNewBU);

            string requestID = "";
            string status = "";
            string message = "";
            APIObject[] results;

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "EmailSendDefinition";

            rr.Properties =
                new string[]
                        {
                           "Name",
                           "CustomerKey",
                           "EmailSubject"
                        };

            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "CustomerKey";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { customerKey };

            rr.Filter = sfp;

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            status = etClient.Retrieve(rr, out requestID, out results);

            if (status.ToLower().Equals("ok"))
            {
                ETClient.EmailSendDefinition e = (ETClient.EmailSendDefinition)results[0];

                // if subject line has a value, update send definition first
                if (!string.IsNullOrEmpty(subjectLine) && e.EmailSubject != subjectLine)
                {
                    e.EmailSubject = subjectLine;
                    UpdateOptions uo = new UpdateOptions();
                    uo.Client = new ClientID();
                    uo.Client.ClientID1 = mid;
                    uo.Client.ClientID1Specified = true;
                    uo.Client.ID = mid;
                    uo.Client.IDSpecified = true;

                    e.Client = uo.Client;

                    etClient.Update(uo, new APIObject[] { e }, out requestID, out status);
                    if (!status.ToLower().Equals("ok"))
                        throw new Exception("Could not Update email send definition");
                }
             
                PerformOptions po = new PerformOptions();
                po.Client = new ClientID() { ClientID1 = mid, ClientID1Specified = true };

                PerformResult[] sResults = etClient.Perform(po, "test",new APIObject[] { e }, out status, out message, out requestID);

                if (status.ToLower().Equals("ok"))
                    return "Test Sent";
                else
                    throw new Exception("Could not create test send");
            }
            else
                throw new Exception("Could not find send definition");
        }

        public string Cancel(int mid, int sendID, bool isNewBU)
        {
            // retrieve the send object
            ETClient.SoapClient etClient = GetClient(isNewBU);

            string requestID = "";
            string status = "";
            string message = "";
            APIObject[] results;

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "Send";

            rr.Properties =
                new string[]
                        {
                           "ID",
                           "EmailSendDefinition.CustomerKey",
                           "Status",
                           "PreviewURL"
                        };

            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "ID";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { sendID.ToString() };

            rr.Filter = sfp;

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            status = etClient.Retrieve(rr, out requestID, out results);

            if (status.ToLower().Equals("ok"))
            {
                ETClient.Send send = (ETClient.Send)results[0];

                PerformOptions performOptions = new PerformOptions();
                performOptions.Client = new ClientID() { ClientID1 = mid, ClientID1Specified = true };
                APIObject[] performObjects = new APIObject[] { send };
                PerformResult[] performResults;

                if (send.Status.ToLower() != "complete")
                    performResults = etClient.Perform(performOptions, "cancel", performObjects, out status, out message, out requestID);
            }

            if (status.ToLower().Equals("ok"))
                return "Send Aborted" ;
            else
                throw new Exception("Could not cancel send");
        }

        public string GetSendStatus(int mid, int sendID, bool isNewBU)
        {
            // retrieve the send object
            ETClient.SoapClient etClient = GetClient(isNewBU);

            string requestID = "";
            string status = "";
            string message = "";
            APIObject[] results;

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "Send";

            rr.Properties =
                new string[]
                        {
                           "ID",
                           "EmailSendDefinition.CustomerKey",
                           "Status",
                           "PreviewURL"
                        };

            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "ID";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { sendID.ToString() };

            rr.Filter = sfp;

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            status = etClient.Retrieve(rr, out requestID, out results);

            if (status.ToLower().Equals("ok"))
            {
                ETClient.Send send = (ETClient.Send)results[0];
                
                return send.Status;
            }

            return "";
        }

        public Model.Send GetPendingByMidCustomerKey(int mid, string customerKey, bool isNewBU)
        {
            Model.Send send = null;

            // retrieve the send object
            ETClient.SoapClient etClient = GetClient(isNewBU);

            string requestID = "";
            string status = "";

            APIObject[] results;

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "Send";

            rr.Properties =
                new string[]
                        {
                           "ID",
                           "EmailSendDefinition.CustomerKey",
                           "Status",
                           "NumberTargeted",
                           "NumberSent"
                        };

            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "EmailSendDefinition.CustomerKey";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { customerKey };


            SimpleFilterPart sfp2 = new SimpleFilterPart();
            sfp2.Property = "Status";
            sfp2.SimpleOperator = SimpleOperators.equals;
            sfp2.Value = new string[] { "Scheduled" };

            SimpleFilterPart sfp3 = new SimpleFilterPart();
            sfp3.Property = "Status";
            sfp3.SimpleOperator = SimpleOperators.equals;
            sfp3.Value = new string[] { "Sending" };

            ComplexFilterPart cfp2 = new ComplexFilterPart();
            cfp2.LeftOperand = sfp2;
            cfp2.LogicalOperator = LogicalOperators.OR;
            cfp2.RightOperand = sfp3;

            ComplexFilterPart cfp = new ComplexFilterPart();
            cfp.LeftOperand = sfp;
            cfp.LogicalOperator = LogicalOperators.AND;
            cfp.RightOperand = cfp2;

            rr.Filter = cfp;

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            status = etClient.Retrieve(rr, out requestID, out results);

            if (status.ToLower().Equals("ok") && results.Count() > 0)
            {
                ETClient.Send s = (ETClient.Send)results[0];
                send = new Model.Send() { ID = s.ID, Status = s.Status, NumberTargeted = s.NumberTargeted };
            }

            return send;
        }
    }
}
