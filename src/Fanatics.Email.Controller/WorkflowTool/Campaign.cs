﻿using Fanatics.Email.Controller.ETClient;
using Fanatics.Email.Model;
using Fanatics.Email.Model.WorkflowTool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Fanatics.Email.Controller
{
    public class Campaign:BaseController
    {
        public List<Fanatics.Email.Model.Campaign> Get(string campaignsDEKey, int mid, int pageIndex, int pageSize, string sortDirection, string sortBy, string filterBy, string searchBy, string startDate, string endDate)
        {
            List<Fanatics.Email.Model.Campaign> campaigns = GetActive(campaignsDEKey, mid);
            
            List<Fanatics.Email.Model.Campaign> results = campaigns;

            // Filter 
            if (filterBy != "all")
            {
                results = campaigns.Where(p => p.BusinessUnit == filterBy).ToList();
                campaigns = results;
            }

            // Filter by Date
            if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
                results = campaigns.Where(p => p.DeploymentDate.Date >= DateTime.Parse(startDate).Date && p.DeploymentDate.Date <= DateTime.Parse(endDate).Date).ToList();
            else
                results = campaigns.Where(p => p.DeploymentDate >= DateTime.Today && p.DeploymentDate < DateTime.Today.AddDays(14)).ToList();

            // Search
            if (!string.IsNullOrEmpty(searchBy))
            {
                results = campaigns.Where(p => p.BusinessUnit.ToLower().Contains(searchBy.ToLower()) ||
                    p.EmailName.ToLower().Contains(searchBy.ToLower()) ||
                    p.JobIDs.ToString().Contains(searchBy.ToLower()) ||
                    p.OpsOwner.ToLower().Contains(searchBy.ToLower()) ||
                    p.SubjectLine.ToLower().Contains(searchBy.ToLower()) ||
                    p.Status.ToLower().Contains(searchBy.ToLower())
                    ).ToList();
            }

            // Sorting
            if (!string.IsNullOrEmpty(sortBy))
            {
                var campaignType = typeof(Fanatics.Email.Model.Campaign);
                var campaignPropInfo = campaignType.GetProperty(sortBy);
                if (sortDirection == "asc")
                    results = results.OrderBy(p => campaignPropInfo.GetValue(p, null)).ToList();
                else
                    results = results.OrderByDescending(p => campaignPropInfo.GetValue(p, null)).ToList();

            }
            
            return results;
        }

        public List<Fanatics.Email.Model.Campaign> GetActive(string externalKey, int mid)
        {
            List<Fanatics.Email.Model.Campaign> campaigns = new List<Model.Campaign>();

            // Get Campaigns DE Data
            SimpleFilterPart filter = new SimpleFilterPart();
            filter.Property = "Active";
            filter.SimpleOperator = SimpleOperators.notEquals;
            filter.Value = new string[] { "False" };

            Model.DataExtension campaignsDE = GetCampaignByExternalKeyMID(externalKey, mid, filter);

            foreach (var row in campaignsDE.Rows)
            {
                Fanatics.Email.Model.Campaign campaign = new Model.Campaign();

                object Theme, OpsOwner, SubjectLine, Preheader, PreheaderURL, Name, DeploymentDate, Status, JobIDs, CampaignID, QAApprovalDate, AssetsDueDate, BusinessUnit, OpsOwnerEmail, CNumber, CName;

                if (row.TryGetValue("Theme", out Theme))
                    campaign.Theme = Theme.ToString();

                if (row.TryGetValue("BusinessUnit", out BusinessUnit))
                    campaign.BusinessUnit = BusinessUnit.ToString();

                if (row.TryGetValue("OpsOwner", out OpsOwner))
                    campaign.OpsOwner = OpsOwner.ToString();

                if (row.TryGetValue("OpsOwnerEmail", out OpsOwnerEmail))
                    campaign.OpsOwnerEmail = OpsOwnerEmail.ToString();

                if (row.TryGetValue("SubjectLine", out SubjectLine))
                    campaign.SubjectLine = SubjectLine.ToString();

                if (row.TryGetValue("Preheader", out Preheader))
                    campaign.Preheader = Preheader.ToString();

                if (row.TryGetValue("PreheaderURL", out PreheaderURL))
                    campaign.PreheaderURL = PreheaderURL.ToString();

                if (row.TryGetValue("JobIDs", out JobIDs))
                    campaign.JobIDs = JobIDs.ToString();

                if (row.TryGetValue("CampaignID", out CampaignID))
                    campaign.Id = CampaignID.ToString();

                if (row.TryGetValue("Name", out Name))
                    campaign.EmailName = Name.ToString();

                if (row.TryGetValue("Status", out Status))
                    campaign.Status = Status.ToString();

                if (row.TryGetValue("DeploymentDate", out DeploymentDate))
                    campaign.DeploymentDate = String.IsNullOrEmpty(DeploymentDate.ToString()) ? DateTime.Now : DateTime.Parse(DeploymentDate.ToString());

                if (row.TryGetValue("AssetsDueDate", out AssetsDueDate))
                    campaign.AssetsDueDate = String.IsNullOrEmpty(AssetsDueDate.ToString()) ? DateTime.Now : DateTime.Parse(AssetsDueDate.ToString());

                if (row.TryGetValue("QAApprovalDate", out QAApprovalDate))
                    campaign.QAApprovalDate = String.IsNullOrEmpty(QAApprovalDate.ToString()) ? DateTime.Now : DateTime.Parse(QAApprovalDate.ToString());

                DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                Calendar cal = dfi.Calendar;

                campaign.DayOfWeek = campaign.DeploymentDate.DayOfWeek.ToString();
                campaign.WeekNo = cal.GetWeekOfYear(campaign.DeploymentDate, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);

                campaigns.Add(campaign);
            }

            return campaigns;
        }

        public Model.DataExtension GetCampaignByExternalKeyMID(string externalKey, int mid, FilterPart filter)
        {
            Regex intRegex = new Regex(@"^\d+$");
            Model.DataExtension de = new Model.DataExtension();
            de.ExternalKey = externalKey;

            string requestID = "";
            string status = "";
            APIObject[] results;

            ETClient.SoapClient etClient = GetClient();

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = string.Format("DataExtensionObject[{0}]", externalKey);

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            rr.Properties = new string[14]{ "Theme", "BusinessUnit", "OpsOwner", "OpsOwnerEmail", "SubjectLine", "Preheader","PreheaderURL", "JobIDs", "CampaignID", "Name", "Status", "DeploymentDate", "AssetsDueDate", "QAApprovalDate"};
                
             if (filter != null)
                rr.Filter = filter;

            status = etClient.Retrieve(rr, out requestID, out results);
            if (!status.ToLower().Equals("ok") && !status.ToLower().Equals("moredataavailable"))
            {
                throw new Exception(status);
            }
            else
            {
                while (status.ToLower().Equals("moredataavailable") || status.ToLower().Equals("ok"))
                {
                    foreach (DataExtensionObject deObject in results)
                    {
                        IDictionary<String, object> row = new Dictionary<String, object>(StringComparer.OrdinalIgnoreCase);

                        foreach (var prop in deObject.Properties)
                        {
                            if (prop.Name.IndexOf("_CustomObjectKey") < 0)
                            {
                                if (intRegex.IsMatch(prop.Value))
                                    row.Add(prop.Name, int.Parse(prop.Value));
                                else
                                    row.Add(prop.Name, prop.Value);
                            }
                        }

                        if (de.HasPrimaryKey)
                            row.Add("Actions", "");
                        else
                            row.Add("Actions", "none");

                        de.Rows.Add(row);
                    }

                    if (status.ToLower().Equals("moredataavailable"))
                    {
                        rr.ContinueRequest = requestID;
                        status = etClient.Retrieve(rr, out requestID, out results);
                    }
                    else
                        status = "complete";
                }
            }

            return de;
        }

        public dynamic ChangeStatus(int mid, string campaignID, string status)
        {
            bool updated = true;

            string requestID = "";
            string resultStatus = "";

            ETClient.SoapClient etClient = GetClient();

            UpdateOptions updateOptions = new UpdateOptions();
            SaveOption saveOption = new SaveOption();

            saveOption.SaveAction = SaveAction.UpdateAdd;
            saveOption.PropertyName = "DataExtensionObject";
            updateOptions.SaveOptions = new SaveOption[] { saveOption };

            ClientID clientID = new ClientID();
            clientID.ID = mid;
            clientID.IDSpecified = true;
            updateOptions.Client = clientID;

            List<APIObject> objects = new List<APIObject>();

            var campaignsDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignsDE"].ToString();

            DataExtensionObject deCampaigns = new DataExtensionObject();
            deCampaigns.CustomerKey = campaignsDE;

            List<APIProperty> keys = new List<APIProperty>();
            List<APIProperty> props = new List<APIProperty>();

            // main campaign object
            keys.Add(new APIProperty() { Name = "CampaignID", Value = campaignID });
            props.Add(new APIProperty() { Name = "Status", Value = status });

            deCampaigns.Properties = props.ToArray();
            deCampaigns.Keys = keys.ToArray();

            objects.Add(deCampaigns);

            UpdateResult[] updateResult = etClient.Update(updateOptions, objects.ToArray(), out requestID, out resultStatus);

            // Save Status change
            var campaign = GetDetails(mid, campaignID);
            List<CampaignNotification> notifications = GetNotificationsByCampaign(campaign,status, "Status");
            
            SaveNotification(mid, notifications);

            return new { Status = resultStatus };
        }

        public dynamic ChangeOwner(int mid, string campaignID, string owner,string ownerEmail)
        {
            bool updated = true;

            string requestID = "";
            string resultStatus = "";

            ETClient.SoapClient etClient = GetClient();

            UpdateOptions updateOptions = new UpdateOptions();
            SaveOption saveOption = new SaveOption();

            saveOption.SaveAction = SaveAction.UpdateAdd;
            saveOption.PropertyName = "DataExtensionObject";
            updateOptions.SaveOptions = new SaveOption[] { saveOption };

            ClientID clientID = new ClientID();
            clientID.ID = mid;
            clientID.IDSpecified = true;
            updateOptions.Client = clientID;

            List<APIObject> objects = new List<APIObject>();

            var campaignsDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignsDE"].ToString();

            DataExtensionObject deCampaigns = new DataExtensionObject();
            deCampaigns.CustomerKey = campaignsDE;

            List<APIProperty> keys = new List<APIProperty>();
            List<APIProperty> props = new List<APIProperty>();

            // main campaign object
            keys.Add(new APIProperty() { Name = "CampaignID", Value = campaignID });
            props.Add(new APIProperty() { Name = "OpsOwner", Value = owner });
            props.Add(new APIProperty() { Name = "OpsOwnerEmail", Value = ownerEmail });

            deCampaigns.Properties = props.ToArray();
            deCampaigns.Keys = keys.ToArray();

            objects.Add(deCampaigns);

            UpdateResult[] updateResult = etClient.Update(updateOptions, objects.ToArray(), out requestID, out resultStatus);

            // Save Status change
            var campaign = GetDetails(mid, campaignID);
            List<CampaignNotification> notifications = GetNotificationsByCampaign(campaign, "DetailsChanged", "Ops Owner");

            SaveNotification(mid, notifications);

            return new { Status = resultStatus };
        }

        public dynamic ChangeJobIDs(int mid, string campaignID, string jobids)
        {
            bool updated = true;

            string requestID = "";
            string resultStatus = "";

            ETClient.SoapClient etClient = GetClient();

            UpdateOptions updateOptions = new UpdateOptions();
            SaveOption saveOption = new SaveOption();

            saveOption.SaveAction = SaveAction.UpdateAdd;
            saveOption.PropertyName = "DataExtensionObject";
            updateOptions.SaveOptions = new SaveOption[] { saveOption };

            ClientID clientID = new ClientID();
            clientID.ID = mid;
            clientID.IDSpecified = true;
            updateOptions.Client = clientID;

            List<APIObject> objects = new List<APIObject>();

            var campaignsDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignsDE"].ToString();

            DataExtensionObject deCampaigns = new DataExtensionObject();
            deCampaigns.CustomerKey = campaignsDE;

            List<APIProperty> keys = new List<APIProperty>();
            List<APIProperty> props = new List<APIProperty>();

            // main campaign object
            keys.Add(new APIProperty() { Name = "CampaignID", Value = campaignID });
            props.Add(new APIProperty() { Name = "JobIDs", Value = jobids });
            
            deCampaigns.Properties = props.ToArray();
            deCampaigns.Keys = keys.ToArray();

            objects.Add(deCampaigns);

            UpdateResult[] updateResult = etClient.Update(updateOptions, objects.ToArray(), out requestID, out resultStatus);

            // Save Status change
            var campaign = GetDetails(mid, campaignID);
            List<CampaignNotification> notifications = GetNotificationsByCampaign(campaign, "DetailsChanged","Job IDs");

            SaveNotification(mid, notifications);

            return new { Status = resultStatus };
        }

        private List<CampaignNotification> GetNotificationsByCampaign(Model.Campaign campaign,string status,string fieldsChanged)
        {
            List<CampaignNotification> notifications = new List<CampaignNotification>();
            CampaignNotification n;

            // set notification status which will trigger a different email on automation
            string notificationStatus = (status != "At Risk" || status != "Development") ? "DetailsChanged" : status;
            
            // add notifications for each owner
            foreach (var owner in campaign.CampaignOwners)
            {
                n = new CampaignNotification();
                n.CampaignID = campaign.Id;
                n.CampaignName = campaign.EmailName;
                n.EmailAddress = owner.UserEmail;
                n.UserName = owner.UserName;
                n.NotificationType = status;
                n.NotificationBody = fieldsChanged;
                n.IsAdmin = false;
                notifications.Add(n);
            }

            // add notifications for the ops owner
            if (!string.IsNullOrEmpty(campaign.OpsOwner))
            {
                n = new CampaignNotification();
                n.CampaignID = campaign.Id;
                n.CampaignName = campaign.EmailName;
                n.EmailAddress = campaign.OpsOwnerEmail;
                n.UserName = campaign.OpsOwner;
                n.NotificationType = status;
                n.NotificationBody = fieldsChanged;
                n.IsAdmin = false;
                notifications.Add(n);
            }

            // add notification to the admin
            // if admin is owner do not add so we don't send duplicate rows
            var adminEmail = ConfigurationManager.AppSettings["WorkflowTool.AdminEmail"].ToString();
            if (!string.IsNullOrEmpty(adminEmail) && campaign.CampaignOwners.Where(p => p.UserEmail == adminEmail).ToList().Count == 0)
            {
                n = new CampaignNotification();
                n.CampaignID = campaign.Id;
                n.CampaignName = campaign.EmailName;
                n.EmailAddress = adminEmail;
                n.UserName = "Admin";
                n.NotificationType = status;
                n.NotificationBody = fieldsChanged;
                n.IsAdmin = false;
                notifications.Add(n);
            }

            return notifications;
        }

        private string GetModifiedFields(Model.Campaign campaign, Model.Campaign existingCampaign)
        {
            List<string> modifiedFields = new List<string>();

            if (campaign.AssetsLocation != existingCampaign.AssetsLocation)
                modifiedFields.Add("Assets Location");

            if (campaign.Audiences.Count != existingCampaign.Audiences.Count)
                modifiedFields.Add("Audiences");

            if (campaign.BusinessUnit != existingCampaign.BusinessUnit)
                modifiedFields.Add("Business Unit");

            if (campaign.CampaignOwners.Count != existingCampaign.CampaignOwners.Count)
                modifiedFields.Add("Campaign Owners");

            if (campaign.CName != existingCampaign.CName)
                modifiedFields.Add("CName");

            if (campaign.CNumber != existingCampaign.CNumber)
                modifiedFields.Add("CNumber");

            if (campaign.DeploymentDate != existingCampaign.DeploymentDate)
                modifiedFields.Add("Deployment Date");

            if (campaign.Disclaimer != existingCampaign.Disclaimer)
                modifiedFields.Add("Disclaimer");

            if (campaign.EmailName != existingCampaign.EmailName)
                modifiedFields.Add("Email Name");

            if (campaign.Feedback.Count != existingCampaign.Feedback.Count)
                modifiedFields.Add("Feedback");

            if (campaign.JobIDs != existingCampaign.JobIDs)
                modifiedFields.Add("JobIDs");

            if (campaign.Notes != existingCampaign.Notes)
                modifiedFields.Add("Notes");

            if (campaign.OpsOwner != existingCampaign.OpsOwner)
                modifiedFields.Add("Ops Owner");

            if (campaign.Preheader != existingCampaign.Preheader)
                modifiedFields.Add("Preheader");

            if (campaign.Promos.Count != existingCampaign.Promos.Count)
                modifiedFields.Add("Promos");

            if (campaign.SubjectLine != existingCampaign.SubjectLine)
                modifiedFields.Add("SubjectLine");

            if (campaign.Template != existingCampaign.Template)
                modifiedFields.Add("Template");

            if (campaign.TemplateConfiguration.Count != existingCampaign.TemplateConfiguration.Count)
                modifiedFields.Add("Template Configuration");

            if (campaign.Theme != existingCampaign.Theme)
                modifiedFields.Add("Theme");

            if (campaign.Toggles.Count != existingCampaign.Toggles.Count)
                modifiedFields.Add("Toggles");

            return string.Join(",", modifiedFields);
        }

        public dynamic Copy(int mid,string campaignID, string campaignName)
        {
            bool updated = true;

            string requestID = "";
            string status = "";

            ETClient.SoapClient etClient = GetClient();

            UpdateOptions updateOptions = new UpdateOptions();
            SaveOption saveOption = new SaveOption();

            saveOption.SaveAction = SaveAction.UpdateAdd;
            saveOption.PropertyName = "DataExtensionObject";
            updateOptions.SaveOptions = new SaveOption[] { saveOption };

            ClientID clientID = new ClientID();
            clientID.ID = mid;
            clientID.IDSpecified = true;
            updateOptions.Client = clientID;

            List<APIObject> objects = new List<APIObject>();

            var campaignsDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignsDE"].ToString();
            var ownersDE = ConfigurationManager.AppSettings["WorkflowTool.OwnersDE"].ToString();
            var promosDE = ConfigurationManager.AppSettings["WorkflowTool.PromosDE"].ToString();
            var templatesDE = ConfigurationManager.AppSettings["WorkflowTool.TemplatesDE"].ToString();
            var togglesDE = ConfigurationManager.AppSettings["WorkflowTool.TogglesDE"].ToString();
            var campaignAudiencesDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignAudiencesDE"].ToString();

            if (NameExists(mid, campaignName))
                return new { Status = "error", Message = "duplicate campaign" };

            var campaign = GetDetails(mid, campaignID);

            campaign.Id = "";
            campaign.EmailName = campaignName;

            return Save(mid, campaign);
        }

        public dynamic DeleteRelatedObjects(int mid, Model.Campaign campaign)
        {
            string requestID = "";
            string status = "";

            List<APIObject> objects = new List<APIObject>();

            ETClient.SoapClient etClient = GetClient();

            DeleteOptions deleteOptions = new DeleteOptions();
            SaveOption saveOption = new SaveOption();

            saveOption.SaveAction = SaveAction.UpdateOnly;
            saveOption.PropertyName = "DataExtensionObject";
            deleteOptions.SaveOptions = new SaveOption[] { saveOption };

            ClientID clientID = new ClientID();
            clientID.ID = mid;
            clientID.IDSpecified = true;
            deleteOptions.Client = clientID;

            var ownersDE = ConfigurationManager.AppSettings["WorkflowTool.OwnersDE"].ToString();
            var promosDE = ConfigurationManager.AppSettings["WorkflowTool.PromosDE"].ToString();
            var templatesDE = ConfigurationManager.AppSettings["WorkflowTool.TemplatesDE"].ToString();
            var togglesDE = ConfigurationManager.AppSettings["WorkflowTool.TogglesDE"].ToString();
            var campaignAudiencesDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignAudiencesDE"].ToString();
            
            List<APIProperty> keys = new List<APIProperty>();
            List<APIProperty> props = new List<APIProperty>();

            // owners
            foreach (var owner in campaign.CampaignOwners)
            {
                keys = new List<APIProperty>();
                props = new List<APIProperty>();

                DataExtensionObject deOwners = new DataExtensionObject();
                deOwners.CustomerKey = ownersDE;

                keys.Add(new APIProperty() { Name = "CampaignID", Value = campaign.Id });
                keys.Add(new APIProperty() { Name = "UserName", Value = owner.UserName });
                props.Add(new APIProperty() { Name = "UserEmail", Value = owner.UserEmail });
                props.Add(new APIProperty() { Name = "ModifiedDate", Value = DateTime.Now.ToString() });

                deOwners.Properties = props.ToArray();
                deOwners.Keys = keys.ToArray();

                objects.Add(deOwners);
            }

            // promos
            foreach (var promo in campaign.Promos)
            {
                keys = new List<APIProperty>();
                props = new List<APIProperty>();

                DataExtensionObject dePromos = new DataExtensionObject();
                dePromos.CustomerKey = promosDE;

                keys.Add(new APIProperty() { Name = "CampaignID", Value = campaign.Id });
                keys.Add(new APIProperty() { Name = "PromoName", Value = promo.PromoName });
                props.Add(new APIProperty() { Name = "StartDate", Value = promo.StartDate.ToString() });
                props.Add(new APIProperty() { Name = "EndDate", Value = promo.EndDate.ToString() });
                props.Add(new APIProperty() { Name = "PromoCode", Value = promo.PromoCode });

                dePromos.Properties = props.ToArray();
                dePromos.Keys = keys.ToArray();

                objects.Add(dePromos);
            }

            // template info
            foreach (var spot in campaign.TemplateConfiguration)
            {
                keys = new List<APIProperty>();
                props = new List<APIProperty>();

                DataExtensionObject deTemplates = new DataExtensionObject();
                deTemplates.CustomerKey = templatesDE;

                keys.Add(new APIProperty() { Name = "CampaignID", Value = campaign.Id });
                keys.Add(new APIProperty() { Name = "Section", Value = spot.Section });
                keys.Add(new APIProperty() { Name = "ContentName", Value = spot.ContentName });
                props.Add(new APIProperty() { Name = "ContentValue", Value = spot.ContentValue });

                deTemplates.Properties = props.ToArray();
                deTemplates.Keys = keys.ToArray();

                objects.Add(deTemplates);
            }

            // toggles
            foreach (var toggle in campaign.Toggles)
            {
                keys = new List<APIProperty>();
                props = new List<APIProperty>();

                DataExtensionObject deToggles = new DataExtensionObject();
                deToggles.CustomerKey = togglesDE;

                keys.Add(new APIProperty() { Name = "CampaignID", Value = campaign.Id });
                keys.Add(new APIProperty() { Name = "Toggle", Value = toggle });
                props.Add(new APIProperty() { Name = "ModifiedDate", Value = DateTime.Now.ToString() });

                deToggles.Properties = props.ToArray();
                deToggles.Keys = keys.ToArray();

                objects.Add(deToggles);
            }

            // audiences 
            foreach (var audience in campaign.Audiences)
            {
                keys = new List<APIProperty>();
                props = new List<APIProperty>();

                DataExtensionObject deCampaignAudiences = new DataExtensionObject();
                deCampaignAudiences.CustomerKey = campaignAudiencesDE;

                keys.Add(new APIProperty() { Name = "CampaignID", Value = campaign.Id });
                keys.Add(new APIProperty() { Name = "Audience", Value = audience.AudienceType });
                keys.Add(new APIProperty() { Name = "Value", Value = audience.Value == null ? "" : audience.Value });
                props.Add(new APIProperty() { Name = "ModifiedDate", Value = DateTime.Now.ToString() });

                deCampaignAudiences.Properties = props.ToArray();
                deCampaignAudiences.Keys = keys.ToArray();

                objects.Add(deCampaignAudiences);
            }

            DeleteResult[] updateResult = etClient.Delete(deleteOptions, objects.ToArray(), out requestID, out status);

            return new { Status = status };
        }

        public dynamic Delete(int mid, string campaignID,string lastModifiedBy)
        {
            string requestID = "";
            string status = "";
            var campaignsDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignsDE"].ToString();

            List<APIObject> objects = new List<APIObject>();

            ETClient.SoapClient etClient = GetClient();

            UpdateOptions updateOptions = new UpdateOptions();
            SaveOption saveOption = new SaveOption();

            saveOption.SaveAction = SaveAction.UpdateOnly;
            saveOption.PropertyName = "DataExtensionObject";
            updateOptions.SaveOptions = new SaveOption[] { saveOption };

            ClientID clientID = new ClientID();
            clientID.ID = mid;
            clientID.IDSpecified = true;
            updateOptions.Client = clientID;

            DataExtensionObject deCampaigns = new DataExtensionObject();
            deCampaigns.CustomerKey = campaignsDE;

            List<APIProperty> keys = new List<APIProperty>();
            List<APIProperty> props = new List<APIProperty>();

            keys.Add(new APIProperty() { Name = "CampaignID", Value = campaignID });
            props.Add(new APIProperty() { Name = "Active", Value = "False" });
            props.Add(new APIProperty() { Name = "LastModifiedDate", Value = DateTime.Now.ToString() });
            props.Add(new APIProperty() { Name = "LastModifiedBy", Value = lastModifiedBy });

            deCampaigns.Properties = props.ToArray();
            deCampaigns.Keys = keys.ToArray();

            objects.Add(deCampaigns);

            UpdateResult[] updateResult = etClient.Update(updateOptions, objects.ToArray(), out requestID, out status);

            return new { Status = status};
        }

        public dynamic Save(int mid, Model.Campaign campaign)
        {
            bool updated = true;

            string requestID = "";
            string status = "";

            ETClient.SoapClient etClient = GetClient();

            UpdateOptions updateOptions = new UpdateOptions();
            SaveOption saveOption = new SaveOption();

            saveOption.SaveAction = SaveAction.UpdateAdd;
            saveOption.PropertyName = "DataExtensionObject";
            updateOptions.SaveOptions = new SaveOption[] { saveOption };

            ClientID clientID = new ClientID();
            clientID.ID = mid;
            clientID.IDSpecified = true;
            updateOptions.Client = clientID;
           string campaignStatus = "DetailsChanged";
            string fieldsChanged = "";
            if (string.IsNullOrEmpty(campaign.Id))
            {
                if (NameExists(mid, campaign.EmailName))
                    return new { Status = "error", Message = "duplicate campaign" };

                campaign.Id = Guid.NewGuid().ToString();

                campaignStatus = "New";
            }
            else
            {
                // if campaign exists, delete existing objects from SFMC
                var existingCampaign = GetDetails(mid, campaign.Id);

                // if status is detailsChanged then we need to bring which fields changed.
                if (existingCampaign != null)
                    fieldsChanged = GetModifiedFields(campaign, existingCampaign);

                DeleteRelatedObjects(mid, existingCampaign);
            }

            // If user picked a deployment Date that is before the Assets QA Date or the Assets Due Date
            // Change the status to at risk
            if (campaign.DeploymentDate < campaign.QAApprovalDate || campaign.DeploymentDate < campaign.AssetsDueDate)
                campaignStatus = "At Risk";

            campaign.Status = campaignStatus;

            List<APIObject> objects = ParseCampaignObject(campaign);

            UpdateResult[] updateResult = etClient.Update(updateOptions, objects.ToArray(), out requestID, out status);

            // Save notifactions
            
           List<CampaignNotification> notifications = GetNotificationsByCampaign(campaign, campaignStatus, fieldsChanged);

            SaveNotification(mid, notifications);

            return new { Status = status, CampaignID = campaign.Id };
        }

        /// <summary>
        /// Imports a list of Campaigns
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="campaign"></param>
        /// <returns></returns>
        public ImportResult Import(int mid, List<Model.EmailSendCreateRequest> requests)
        {
            Controller.Email emailController = new Email();
            Controller.EmailSendDefinition esdController = new EmailSendDefinition();

            bool updated = true;
            
            List<string> errors = new List<string>();
            int processedCount = 0;
            string requestID = "";
            string status = "";

            ETClient.SoapClient etClient = GetClient();

            UpdateOptions updateOptions = new UpdateOptions();
            SaveOption saveOption = new SaveOption();

            saveOption.SaveAction = SaveAction.UpdateAdd;
            saveOption.PropertyName = "DataExtensionObject";
            updateOptions.SaveOptions = new SaveOption[] { saveOption };

            ClientID clientID = new ClientID();
            clientID.ID = mid;
            clientID.IDSpecified = true;
            updateOptions.Client = clientID;

            List<APIObject> objects = new List<APIObject>();
            List<CampaignNotification> notifications = new List<CampaignNotification>();

            string campaignStatus = "New";

            foreach (var request in requests)
            {
                var campaign = request.Campaign;
                campaign.Id = Guid.NewGuid().ToString();

                if (campaign.DeploymentDate > campaign.QAApprovalDate || campaign.DeploymentDate > campaign.AssetsDueDate)
                    campaignStatus = "At Risk";

                campaign.Status = campaignStatus;

                var campaignObject = ParseCampaignObject(campaign);
                objects.AddRange(campaignObject);

                notifications.AddRange(GetNotificationsByCampaign(campaign, campaignStatus,""));

                if(request.ProcesssRequest)
                {
                    var errored = false;
                    if(request.EmailsFolderId > 0)
                    {
                        try
                        {
                            emailController.CreateFromCampaignRequest(request);
                        }
                        catch (Exception ex)
                        {
                            errored = true;
                            errors.Add(string.Format("Error creating Email for Campaign {0} : {1}",campaign.EmailName ,ex.Message));
                        }
                    }

                    if(request.UISendsFolderId > 0 && !errored)
                    {
                        try
                        {
                            esdController.CreateFromCampaignRequest(request);
                        }
                        catch (Exception ex)
                        {
                            errors.Add(string.Format("Error creating UI Send for Campaign {0} : {1}", campaign.EmailName, ex.Message));
                        }
                    }
                }
            }
            
            UpdateResult[] updateResults = etClient.Update(updateOptions, objects.ToArray(), out requestID, out status);

            foreach (UpdateResult result in updateResults)
            {
                if (result.StatusCode.ToLower() != "ok")
                    errors.Add(string.Format("API error : {0}",result.StatusMessage));
                else
                {
                    if (result.Object.GetType() == typeof(DataExtensionObject))
                    {
                        var customerKey = ((DataExtensionObject)result.Object).CustomerKey;
                        if (customerKey.ToLower().Equals(ConfigurationManager.AppSettings["WorkflowTool.CampaignsDE"].ToString().ToLower()))
                            processedCount++;
                    }
                }
            }

            // Save notifactions
            SaveNotification(mid, notifications);

            return new ImportResult (){ Status = status, Errors = errors, ProcessedCount = processedCount };
        }

        /// <summary>
        /// Converts a Campaign model into a collection of API Objects to upload into SFMC.
        /// </summary>
        /// <param name="campaign"></param>
        /// <returns></returns>
        public List<APIObject> ParseCampaignObject(Model.Campaign campaign)
        {
            List<APIObject> objects = new List<APIObject>();

            var campaignsDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignsDE"].ToString();
            var ownersDE = ConfigurationManager.AppSettings["WorkflowTool.OwnersDE"].ToString();
            var promosDE = ConfigurationManager.AppSettings["WorkflowTool.PromosDE"].ToString();
            var templatesDE = ConfigurationManager.AppSettings["WorkflowTool.TemplatesDE"].ToString();
            var togglesDE = ConfigurationManager.AppSettings["WorkflowTool.TogglesDE"].ToString();
            var campaignAudiencesDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignAudiencesDE"].ToString();

           
            DataExtensionObject deCampaigns = new DataExtensionObject();
            deCampaigns.CustomerKey = campaignsDE;

            List<APIProperty> keys = new List<APIProperty>();
            List<APIProperty> props = new List<APIProperty>();

            // main campaign object
            keys.Add(new APIProperty() { Name = "CampaignID", Value = campaign.Id });
            props.Add(new APIProperty() { Name = "Theme", Value = campaign.Theme });
            props.Add(new APIProperty() { Name = "OpsOwner", Value = campaign.OpsOwner });
            props.Add(new APIProperty() { Name = "OpsOwnerEmail", Value = campaign.OpsOwnerEmail });
            props.Add(new APIProperty() { Name = "SubjectLine", Value = campaign.SubjectLine });
            props.Add(new APIProperty() { Name = "Preheader", Value = campaign.Preheader });
            props.Add(new APIProperty() { Name = "PreheaderURL", Value = campaign.PreheaderURL });
            props.Add(new APIProperty() { Name = "LastModifiedDate", Value = DateTime.Now.ToString() });
            props.Add(new APIProperty() { Name = "AssetsLocation", Value = campaign.AssetsLocation });
            props.Add(new APIProperty() { Name = "Name", Value = campaign.EmailName });
            props.Add(new APIProperty() { Name = "CNumber", Value = campaign.CNumber });
            props.Add(new APIProperty() { Name = "CName", Value = campaign.CName });
            props.Add(new APIProperty() { Name = "DeploymentDate", Value = campaign.DeploymentDate.ToString() });
            props.Add(new APIProperty() { Name = "CreatedBy", Value = campaign.CreatedBy });
            props.Add(new APIProperty() { Name = "Status", Value = campaign.Status });
            props.Add(new APIProperty() { Name = "Disclaimer", Value = campaign.Disclaimer });
            props.Add(new APIProperty() { Name = "JobIDs", Value = campaign.JobIDs });
            props.Add(new APIProperty() { Name = "LastModifiedBy", Value = campaign.LastModifiedBy });
            props.Add(new APIProperty() { Name = "CreatedDate", Value = DateTime.Now.ToString() });
            props.Add(new APIProperty() { Name = "Notes", Value = campaign.Notes });
            props.Add(new APIProperty() { Name = "BusinessUnit", Value = campaign.BusinessUnit });
            props.Add(new APIProperty() { Name = "MessageType", Value = campaign.MessageType });
            props.Add(new APIProperty() { Name = "Template", Value = campaign.Template });
            props.Add(new APIProperty() { Name = "QAApprovalDate", Value = campaign.QAApprovalDate.ToString() });
            props.Add(new APIProperty() { Name = "AssetsDueDate", Value = campaign.AssetsDueDate.ToString() });

            deCampaigns.Properties = props.ToArray();
            deCampaigns.Keys = keys.ToArray();

            objects.Add(deCampaigns);

            // owners
            foreach (var owner in campaign.CampaignOwners)
            {
                keys = new List<APIProperty>();
                props = new List<APIProperty>();

                DataExtensionObject deOwners = new DataExtensionObject();
                deOwners.CustomerKey = ownersDE;

                keys.Add(new APIProperty() { Name = "CampaignID", Value = campaign.Id });
                keys.Add(new APIProperty() { Name = "UserName", Value = owner.UserName });
                props.Add(new APIProperty() { Name = "UserEmail", Value = owner.UserEmail });
                props.Add(new APIProperty() { Name = "ModifiedDate", Value = DateTime.Now.ToString() });

                deOwners.Properties = props.ToArray();
                deOwners.Keys = keys.ToArray();

                objects.Add(deOwners);
            }

            // promos
            foreach (var promo in campaign.Promos)
            {
                keys = new List<APIProperty>();
                props = new List<APIProperty>();

                DataExtensionObject dePromos = new DataExtensionObject();
                dePromos.CustomerKey = promosDE;

                keys.Add(new APIProperty() { Name = "CampaignID", Value = campaign.Id });
                keys.Add(new APIProperty() { Name = "PromoName", Value = promo.PromoName });
                props.Add(new APIProperty() { Name = "StartDate", Value = promo.StartDate.ToString() });
                props.Add(new APIProperty() { Name = "EndDate", Value = promo.EndDate.ToString() });
                props.Add(new APIProperty() { Name = "PromoCode", Value = promo.PromoCode });

                dePromos.Properties = props.ToArray();
                dePromos.Keys = keys.ToArray();

                objects.Add(dePromos);
            }

            // template info
            foreach (var spot in campaign.TemplateConfiguration)
            {
                keys = new List<APIProperty>();
                props = new List<APIProperty>();

                DataExtensionObject deTemplates = new DataExtensionObject();
                deTemplates.CustomerKey = templatesDE;

                keys.Add(new APIProperty() { Name = "CampaignID", Value = campaign.Id });
                keys.Add(new APIProperty() { Name = "Section", Value = spot.Section });
                keys.Add(new APIProperty() { Name = "ContentName", Value = spot.ContentName });
                props.Add(new APIProperty() { Name = "ContentValue", Value = spot.ContentValue });

                deTemplates.Properties = props.ToArray();
                deTemplates.Keys = keys.ToArray();

                objects.Add(deTemplates);
            }

            // toggles
            foreach (var toggle in campaign.Toggles)
            {
                keys = new List<APIProperty>();
                props = new List<APIProperty>();

                DataExtensionObject deToggles = new DataExtensionObject();
                deToggles.CustomerKey = togglesDE;

                keys.Add(new APIProperty() { Name = "CampaignID", Value = campaign.Id });
                keys.Add(new APIProperty() { Name = "Toggle", Value = toggle });
                props.Add(new APIProperty() { Name = "ModifiedDate", Value = DateTime.Now.ToString() });

                deToggles.Properties = props.ToArray();
                deToggles.Keys = keys.ToArray();

                objects.Add(deToggles);
            }

            // audiences 
            foreach (var audience in campaign.Audiences)
            {
                keys = new List<APIProperty>();
                props = new List<APIProperty>();

                DataExtensionObject deCampaignAudiences = new DataExtensionObject();
                deCampaignAudiences.CustomerKey = campaignAudiencesDE;

                keys.Add(new APIProperty() { Name = "CampaignID", Value = campaign.Id });
                keys.Add(new APIProperty() { Name = "Audience", Value = audience.AudienceType });
                keys.Add(new APIProperty() { Name = "Value", Value = audience.Value == null ? "" : audience.Value });
                props.Add(new APIProperty() { Name = "ModifiedDate", Value = DateTime.Now.ToString() });

                deCampaignAudiences.Properties = props.ToArray();
                deCampaignAudiences.Keys = keys.ToArray();

                objects.Add(deCampaignAudiences);
            }
            return objects;
        }

        /// <summary>
        /// Verify if a campaign with the same name exists
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="campaignID"></param>
        /// <returns></returns>
        public bool NameExists(int mid, string campaignName)
        {
            Fanatics.Email.Model.Campaign campaign = new Model.Campaign();
            string requestID = "";
            string status = "";
            APIObject[] results;

            var campaignsDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignsDE"].ToString();
    
            ETClient.SoapClient etClient = GetClient();

            RetrieveRequest rr = new RetrieveRequest();

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "Name";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { campaignName };

            SimpleFilterPart sfp2 = new SimpleFilterPart();
            sfp2.Property = "Active";
            sfp2.SimpleOperator = SimpleOperators.notEquals;
            sfp2.Value = new string[] { "False" };

            ComplexFilterPart filter = new ComplexFilterPart();
            filter.LeftOperand = sfp;
            filter.RightOperand = sfp2;
            filter.LogicalOperator = LogicalOperators.AND;

            rr.Filter = filter;

            // campaign
            rr.ObjectType = string.Format("DataExtensionObject[{0}]", campaignsDE);
            rr.Properties = new string[] { "Name"};

            etClient.Retrieve(rr, out requestID, out results);
            return (results.Count() > 0);
        }

        public Fanatics.Email.Model.Campaign GetDetails(int mid, string campaignID)
        {
            Fanatics.Email.Model.Campaign campaign = new Model.Campaign();
            string requestID = "";
            string status = "";
            APIObject[] results;

            var campaignsDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignsDE"].ToString();
            var ownersDE = ConfigurationManager.AppSettings["WorkflowTool.OwnersDE"].ToString();
            var promosDE = ConfigurationManager.AppSettings["WorkflowTool.PromosDE"].ToString();
            var templatesDE = ConfigurationManager.AppSettings["WorkflowTool.TemplatesDE"].ToString();
            var togglesDE = ConfigurationManager.AppSettings["WorkflowTool.TogglesDE"].ToString();
            var campaignAudiencesDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignAudiencesDE"].ToString();
            var campaignFeedbackDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignFeedbackDE"].ToString();
           var audienceDE = ConfigurationManager.AppSettings["WorkflowTool.AudienceDE"].ToString();

            ETClient.SoapClient etClient = GetClient();

            RetrieveRequest rr = new RetrieveRequest();
            
            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "CampaignID";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { campaignID };
            rr.Filter = sfp;

            // campaign
            rr.ObjectType = string.Format("DataExtensionObject[{0}]", campaignsDE);
            rr.Properties = new string[] { "CampaignID",
                "Theme",
                "OpsOwner",
                "OpsOwnerEmail",
                "SubjectLine",
                "Preheader",
                "PreheaderURL",
                "LastModifiedDate",
                "AssetsLocation",
                "Name",
                "CName",
                "CNumber",
                "DeploymentDate", 
                "CreatedBy",
                "Status",
                "Disclaimer",
                "JobIDs",
                "LastModifiedBy", 
                "CreatedDate",
                "Notes",
                "BusinessUnit",
                "MessageType",
                "Template",
                "AssetsDueDate",
                "QAApprovalDate"};

            etClient.Retrieve(rr, out requestID, out results);
            if (results.Count() > 0)
            {
                var de = (DataExtensionObject)results[0];

                campaign.Id = campaignID;
                campaign.Theme = de.Properties.ToList().Where(p => p.Name == "Theme").FirstOrDefault().Value;
                campaign.OpsOwner = de.Properties.ToList().Where(p => p.Name == "OpsOwner").FirstOrDefault().Value;
                campaign.OpsOwnerEmail = de.Properties.ToList().Where(p => p.Name == "OpsOwnerEmail").FirstOrDefault().Value;
                campaign.SubjectLine = de.Properties.ToList().Where(p => p.Name == "SubjectLine").FirstOrDefault().Value;
                campaign.Preheader = de.Properties.ToList().Where(p => p.Name == "Preheader").FirstOrDefault().Value;
                campaign.PreheaderURL = de.Properties.ToList().Where(p => p.Name == "PreheaderURL").FirstOrDefault().Value;
                campaign.DeploymentDate = DateTime.Parse(de.Properties.ToList().Where(p => p.Name == "DeploymentDate").FirstOrDefault().Value);
                campaign.AssetsLocation = de.Properties.ToList().Where(p => p.Name == "AssetsLocation").FirstOrDefault().Value;
                campaign.EmailName = de.Properties.ToList().Where(p => p.Name == "Name").FirstOrDefault().Value;
                campaign.CName = de.Properties.ToList().Where(p => p.Name == "CName").FirstOrDefault().Value;
                campaign.CNumber = de.Properties.ToList().Where(p => p.Name == "CNumber").FirstOrDefault().Value;
                campaign.CreatedBy = de.Properties.ToList().Where(p => p.Name == "CreatedBy").FirstOrDefault().Value;
                campaign.Status = de.Properties.ToList().Where(p => p.Name == "Status").FirstOrDefault().Value;
                campaign.Disclaimer = de.Properties.ToList().Where(p => p.Name == "Disclaimer").FirstOrDefault().Value;
                campaign.JobIDs =de.Properties.ToList().Where(p => p.Name == "JobIDs").FirstOrDefault().Value;
                campaign.LastModifiedBy = de.Properties.ToList().Where(p => p.Name == "LastModifiedBy").FirstOrDefault().Value;
                campaign.Notes = de.Properties.ToList().Where(p => p.Name == "Notes").FirstOrDefault().Value;
                campaign.BusinessUnit = de.Properties.ToList().Where(p => p.Name == "BusinessUnit").FirstOrDefault().Value;
                campaign.MessageType = de.Properties.ToList().Where(p => p.Name == "MessageType").FirstOrDefault().Value;
                campaign.Template = de.Properties.ToList().Where(p => p.Name == "Template").FirstOrDefault().Value;
                campaign.AssetsDueDate = DateTime.Parse(de.Properties.ToList().Where(p => p.Name == "AssetsDueDate").FirstOrDefault().Value);
                campaign.QAApprovalDate = DateTime.Parse(de.Properties.ToList().Where(p => p.Name == "QAApprovalDate").FirstOrDefault().Value);
            }

            // owners
             rr.ObjectType = string.Format("DataExtensionObject[{0}]", ownersDE);
             rr.Properties = new string[] { "UserName", "UserEmail"};
             etClient.Retrieve(rr, out requestID, out results);

             if (results.Count() > 0)
             {
                 for (int i = 0; i < results.Count(); i++)
                 {
                     var de = (DataExtensionObject)results[i];
                     campaign.CampaignOwners.Add(
                         new CampaignOwner()
                         {
                             UserName = de.Properties.ToList().Where(p => p.Name == "UserName").FirstOrDefault().Value,
                             UserEmail = de.Properties.ToList().Where(p => p.Name == "UserEmail").FirstOrDefault().Value
                         });
                 }
             }

            // promos
             rr.ObjectType = string.Format("DataExtensionObject[{0}]", promosDE);
             rr.Properties = new string[] { "PromoName", "PromoCode","StartDate","EndDate" };
             etClient.Retrieve(rr, out requestID, out results);

             if (results.Count() > 0)
             {
                 for (int i = 0; i < results.Count(); i++)
                 {
                     var de = (DataExtensionObject)results[i];
                     var promo = new Promo();

                     promo.PromoName = de.Properties.ToList().Where(p => p.Name == "PromoName").FirstOrDefault().Value;
                     promo.PromoCode = de.Properties.ToList().Where(p => p.Name == "PromoCode").FirstOrDefault().Value;
                     promo.StartDate = DateTime.Parse(de.Properties.ToList().Where(p => p.Name == "StartDate").FirstOrDefault().Value);
                     promo.EndDate = DateTime.Parse(de.Properties.ToList().Where(p => p.Name == "EndDate").FirstOrDefault().Value);

                     campaign.Promos.Add(promo);
                 }
             }

            // template data
             rr.ObjectType = string.Format("DataExtensionObject[{0}]", templatesDE);
             rr.Properties = new string[] { "Section", "ContentName", "ContentValue" };
             etClient.Retrieve(rr, out requestID, out results);

             if (results.Count() > 0)
             {
                 for (int i = 0; i < results.Count(); i++)
                 {
                     var de = (DataExtensionObject)results[i];
                     var template = new TemplateConfiguration();

                     template.Section = de.Properties.ToList().Where(p => p.Name == "Section").FirstOrDefault().Value;
                     template.ContentName = de.Properties.ToList().Where(p => p.Name == "ContentName").FirstOrDefault().Value;
                     template.ContentValue = de.Properties.ToList().Where(p => p.Name == "ContentValue").FirstOrDefault().Value;
                     
                     campaign.TemplateConfiguration.Add(template);
                 }
             }

            // toggles
             rr.ObjectType = string.Format("DataExtensionObject[{0}]", togglesDE);
             rr.Properties = new string[] { "Toggle", };
             etClient.Retrieve(rr, out requestID, out results);

             if (results.Count() > 0)
             {
                 for (int i = 0; i < results.Count(); i++)
                 {
                     var de = (DataExtensionObject)results[i];
                     campaign.Toggles.Add(de.Properties.ToList().Where(p => p.Name == "Toggle").FirstOrDefault().Value);
                 }
             }

            // audiences
             rr.ObjectType = string.Format("DataExtensionObject[{0}]", campaignAudiencesDE);
             rr.Properties = new string[] { "Audience", "Value" };
             etClient.Retrieve(rr, out requestID, out results);

             if (results.Count() > 0)
             {
                 for (int i = 0; i < results.Count(); i++)
                 {
                     var de = (DataExtensionObject)results[i];
                     var audience = new Model.Audience();

                     audience.AudienceType = de.Properties.ToList().Where(p => p.Name == "Audience").FirstOrDefault().Value;
                     audience.Value = de.Properties.ToList().Where(p => p.Name == "Value").FirstOrDefault().Value;
                     campaign.Audiences.Add(audience);
                 }
             }

            // feedback
            rr.ObjectType = string.Format("DataExtensionObject[{0}]", campaignFeedbackDE);
            rr.Properties = new string[] { "CreatedBy", "CreatedDate","Feedback" };
            etClient.Retrieve(rr, out requestID, out results);

            if (results.Count() > 0)
            {
                for (int i = 0; i < results.Count(); i++)
                {
                    var de = (DataExtensionObject)results[i];
                    var feedback = new Model.CampaignFeedback();

                    feedback.CreatedBy = de.Properties.ToList().Where(p => p.Name == "CreatedBy").FirstOrDefault().Value;
                    feedback.CreatedDate = DateTime.Parse(de.Properties.ToList().Where(p => p.Name == "CreatedDate").FirstOrDefault().Value);
                    feedback.Feedback = de.Properties.ToList().Where(p => p.Name == "Feedback").FirstOrDefault().Value;
                    campaign.Feedback.Add(feedback);
                }

                // sort feedback on asc order
                campaign.Feedback = campaign.Feedback.OrderBy(f => f.CreatedDate).ToList();
            }

            return campaign;
        }

        public dynamic AddFeedback(int mid, string campaignID, string createdBy ,string feedback)
        {
            string requestID, status;

            ETClient.SoapClient etClient = GetClient();

            CreateOptions createOptions = new CreateOptions();
            SaveOption saveOption = new SaveOption();

            saveOption.SaveAction = SaveAction.AddOnly;
            saveOption.PropertyName = "DataExtensionObject";
            createOptions.SaveOptions = new SaveOption[] { saveOption };

            ClientID clientID = new ClientID();
            clientID.ID = mid;
            clientID.IDSpecified = true;
            createOptions.Client = clientID;

            List<APIObject> objects = new List<APIObject>();

            var feedbackDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignFeedbackDE"].ToString();
            
            DataExtensionObject deFeedback = new DataExtensionObject();
            deFeedback.CustomerKey = feedbackDE;

            List<APIProperty> keys = new List<APIProperty>();
            List<APIProperty> props = new List<APIProperty>();

            var createdDate = DateTime.Now;

            props.Add(new APIProperty() { Name = "CreatedBy", Value = createdBy});
            props.Add(new APIProperty() { Name = "Feedback", Value = feedback});
            props.Add(new APIProperty() { Name = "CreatedDate", Value = createdDate.ToString()});
            props.Add(new APIProperty() { Name = "CampaignID", Value = campaignID});


            deFeedback.Properties = props.ToArray();
            deFeedback.Keys = keys.ToArray();

            objects.Add(deFeedback);
          

            CreateResult[] result = etClient.Create(createOptions, objects.ToArray(), out requestID, out status);

            CampaignFeedback feedbackObject = new CampaignFeedback() { CreatedBy = createdBy, CreatedDate = createdDate, Feedback = feedback };

            return new { Status = status, Feedback = feedbackObject };
        }

        /// <summary>
        /// Gets a DE row to be inserted when creating an object in SFMC that is associated with a campaign.
        /// </summary>
        /// <param name="campaignID"></param>
        /// <param name="customerKey"></param>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public APIObject GetSFMCObjectRow(string campaignID, string customerKey, string objectType)
        {
           var sfmcObjectsDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignSFMCObjectsDE"].ToString();

            DataExtensionObject deSFMCObjects = new DataExtensionObject();
            deSFMCObjects.CustomerKey = sfmcObjectsDE;

            List<APIProperty> keys = new List<APIProperty>();
            List<APIProperty> props = new List<APIProperty>();

            keys.Add(new APIProperty() { Name = "CampaignID", Value = campaignID });
            keys.Add(new APIProperty() { Name = "CustomerKey", Value = customerKey });
            keys.Add(new APIProperty() { Name = "ObjectType", Value = objectType });
            props.Add(new APIProperty() { Name = "Active", Value = "True" });

            deSFMCObjects.Properties = props.ToArray();
            deSFMCObjects.Keys = keys.ToArray();

            return deSFMCObjects;
        }

        public DataExtensionObject[] GetSFMCObjects(int mid, string campaignID, string objectType)
        {
            var sfmcObjectsDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignSFMCObjectsDE"].ToString();
            string requestID = "";
            string status = "";
            APIObject[] results;
            List<DataExtensionObject> deObjects = new List<DataExtensionObject>(); ;

            SimpleFilterPart filter1 = new SimpleFilterPart();
            filter1.Property = "CampaignID";
            filter1.Value = new string[] { campaignID }; ;
            filter1.SimpleOperator = SimpleOperators.equals;

            SimpleFilterPart filter2 = new SimpleFilterPart();
            filter2.Property = "ObjectType";
            filter2.Value = new string[] { objectType}; ;
            filter2.SimpleOperator = SimpleOperators.equals;

            ComplexFilterPart filter = new ComplexFilterPart();

            filter.LeftOperand = filter1;
            filter.RightOperand = filter2;
            filter.LogicalOperator = LogicalOperators.AND;

            ETClient.SoapClient etClient = GetClient();

            RetrieveRequest rr = new RetrieveRequest();

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;
            
            rr.Filter = filter;

            // campaign
            rr.ObjectType = string.Format("DataExtensionObject[{0}]", sfmcObjectsDE);
            rr.Properties = new string[] { 
                "CampaignID",
                "CustomerKey",
                "ObjectType"};

            status = etClient.Retrieve(rr, out requestID, out results);

            if (status.ToLower() != "ok")
                throw new Exception("Could not retrieve rows");
            else
            {
                foreach (DataExtensionObject item in results)
                {
                    DataExtensionObject de = new DataExtensionObject();
                    de.CustomerKey = sfmcObjectsDE;
                    List<APIProperty> keys = new List<APIProperty>();
                    keys.Add(new APIProperty { Name = "CampaignID", Value = item.Properties[0].Value });
                    keys.Add(new APIProperty { Name = "CustomerKey", Value = item.Properties[1].Value });
                    keys.Add(new APIProperty { Name = "ObjectType", Value = item.Properties[2].Value });
                    de.Keys = keys.ToArray();
                    deObjects.Add(de);
                }
            }

            return deObjects.ToArray();
        }

        /// <summary>
        /// returns wheter the campaignID has objects from SFMC already associated to it.
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="campaignID"></param>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public bool HasSFMCObjects(int mid, string campaignID, string objectType)
        {
            return GetSFMCObjects(mid, campaignID, objectType).Count() > 0;
        }

        /// <summary>
        /// Removes SFMC objects associated with a campaign
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="campaignID"></param>
        /// <param name="objectType"></param>
        public void DeleteSFMCObjects(int mid, string campaignID, string objectType, bool isNewBU)
        {
            int etClientID = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out etClientID);

            var objects = GetSFMCObjects(etClientID, campaignID, objectType);

            if (objects.Count() > 0)
            {
                string requestID = "";
                string status = "";

                ETClient.SoapClient etClient = isNewBU ? GetNewClient() : GetClient();

                Controller.Email emailController = new Controller.Email();
                Controller.EmailSendDefinition esdController = new Controller.EmailSendDefinition();

                ClientID[] clientIDs = new ClientID[1];
                clientIDs[0] = new ClientID();
                clientIDs[0].ID = etClientID;
                clientIDs[0].IDSpecified = true;

                DeleteOptions deleteOptions = new DeleteOptions();
                deleteOptions.Client = clientIDs[0];

                // Delete rows from the Data extension.
                // TODO: remove this line once everything is set to new environment.
                ETClient.SoapClient etClientSource = GetClient();
                DeleteResult[] deleteResults = etClientSource.Delete(deleteOptions, objects, out requestID, out status);

                if (status.ToLower() != "ok")
                    throw new Exception("Could not delete rows");

                // delete the actual objects
                deleteOptions.Client.ID = mid;
                deleteOptions.Client.ClientID1 = mid;
                deleteOptions.Client.ClientID1Specified = true;
                
                List<APIObject> sfmcObjects = new List<APIObject>();

                foreach (DataExtensionObject deObjects in objects)
                {
                    var customerKey = deObjects.Keys[1].Value;
                    switch (objectType)
                    {
                        case "uisend":
                            sfmcObjects.Add(esdController.GetByCustomerKey(mid, customerKey,isNewBU));
                            break;
                        case "email":
                            sfmcObjects.Add(emailController.GetByCustomerKey(mid, customerKey,isNewBU));
                            break;
                    }
                }

                deleteResults = etClient.Delete(deleteOptions, sfmcObjects.ToArray(), out requestID, out status);

                if (status.ToLower() != "ok")
                    throw new Exception("Could not delete objects");
            }
        }

        /// <summary>
        /// stores notification row for later automated email
        /// </summary>
        /// <param name="?"></param>
        public void SaveNotification(int mid, List<CampaignNotification> notifications)
        {
            string requestID, status;

            ETClient.SoapClient etClient = GetClient();

            CreateOptions createOptions = new CreateOptions();
            SaveOption saveOption = new SaveOption();

            saveOption.SaveAction = SaveAction.AddOnly;
            saveOption.PropertyName = "DataExtensionObject";
            createOptions.SaveOptions = new SaveOption[] { saveOption };

            ClientID clientID = new ClientID();
            clientID.ID = mid;
            clientID.IDSpecified = true;
            createOptions.Client = clientID;

            List<APIObject> objects = new List<APIObject>();

            var notificationsDE = ConfigurationManager.AppSettings["WorkflowTool.CampaignNotificationsDE"].ToString();

            foreach (var notification in notifications)
            {
                DataExtensionObject deNotifications = new DataExtensionObject();
                deNotifications.CustomerKey = notificationsDE;

                List<APIProperty> keys = new List<APIProperty>();
                List<APIProperty> props = new List<APIProperty>();

                props.Add(new APIProperty() { Name = "EmailAddress", Value = notification.EmailAddress });
                props.Add(new APIProperty() { Name = "CampaignID", Value = notification.CampaignID });
                props.Add(new APIProperty() { Name = "CampaignName", Value = notification.CampaignName });
                props.Add(new APIProperty() { Name = "IsAdmin", Value = notification.IsAdmin.ToString() });
                props.Add(new APIProperty() { Name = "NotificationType", Value = notification.NotificationType });
                props.Add(new APIProperty() { Name = "NotificationBody", Value = notification.NotificationBody });
                props.Add(new APIProperty() { Name = "UserName", Value = notification.UserName });

                deNotifications.Properties = props.ToArray();
                deNotifications.Keys = keys.ToArray();

                objects.Add(deNotifications);
            }

            CreateResult[] result = etClient.Create(createOptions, objects.ToArray(), out requestID, out status);
        }
    }
}
