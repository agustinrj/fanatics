﻿using Fanatics.Email.Controller.ETClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Controller.WorkflowTool
{
    public class User:BaseController
    {
        public void SendForgotPasswordEmail(string subject, string body, string email)
        {
            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            string requestID = "";
            string status = "";
            
            ETClient.SoapClient etClient = GetClient();

            TriggeredSendDefinition definition = new TriggeredSendDefinition();
            definition.CustomerKey = "Forgot_Password";
           
            //subscriber to whom email will be sent
            Subscriber subscriber = new Subscriber();
            subscriber.EmailAddress = email;
            subscriber.SubscriberKey = email;

            TriggeredSend send = new TriggeredSend();

            send.TriggeredSendDefinition = definition;

            //If passing Full HTML_Body, pass value to HTML__Body (This attribute should exist in account)
            ETClient.Attribute attribute1 = new ETClient.Attribute();
            attribute1.Name = "Body";
            attribute1.Value = body;
            ETClient.Attribute attribute2 = new ETClient.Attribute();
            attribute2.Name = "Subject";
            attribute2.Value = subject;
          
            subscriber.Attributes = new ETClient.Attribute[] { attribute1,attribute2 };
            send.Subscribers = new Subscriber[] { subscriber }; 

            APIObject[] sends = { send };
            String requestId = null;
            String overAllStatus = null;
            CreateOptions test = new CreateOptions();
            //If you want to send email Asynchronous, Business Rule should be enabled.
            test.RequestType = RequestType.Asynchronous;
            CreateResult[] results = etClient.Create(new CreateOptions(), sends, out requestId, out overAllStatus); 
        }

        public void email()
        {
            int mid = 10470860; // this attribute determines the account ID (child account).
            CreateOptions createOptions = new CreateOptions();

            ClientID clientID = new ClientID();
            clientID.ID = mid;
            clientID.IDSpecified = true;
            createOptions.Client = clientID;

            string requestID = "";
            string status = "";

            ETClient.SoapClient client = new ETClient.SoapClient();

            client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["ETUserName"];
            client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["ETPassword"];
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings["ETEndPoint"]);

            TriggeredSendDefinition definition = new TriggeredSendDefinition();
            definition.CustomerKey = "Forgot_Password"; // This is the triggered email Key
            

            //subscriber to whom email will be sent
            Subscriber subscriber = new Subscriber();
            subscriber.EmailAddress = "test@bh.exxacttarget.com";
            subscriber.SubscriberKey = "test@bh.exxacttarget.com";

            TriggeredSend send = new TriggeredSend();

            send.TriggeredSendDefinition = definition;
            
            //If passing Full HTML_Body, pass value to HTML__Body (This attribute should exist in account)
            ETClient.Attribute attribute1 = new ETClient.Attribute();
            attribute1.Name = "Body";
            attribute1.Value = "email body";
            ETClient.Attribute attribute2 = new ETClient.Attribute();
            attribute2.Name = "Subject";
            attribute2.Value = "subject of email";

            subscriber.Attributes = new ETClient.Attribute[] { attribute1, attribute2 };
            send.Subscribers = new Subscriber[] { subscriber };

            APIObject[] sends = { send };
            String requestId = null;
            String overAllStatus = null;

            CreateResult[] results = client.Create(createOptions, sends, out requestId, out overAllStatus);
        }
    }
}
