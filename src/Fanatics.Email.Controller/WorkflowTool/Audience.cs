﻿using Fanatics.Email.Controller.ETClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Controller
{
    public class Audience:BaseController
    {
        public List<Fanatics.Email.Model.Audience> GetByBusinessUnit(string audienceDEKey, int mid, string buName, bool isNewBU)
        {
            List<Fanatics.Email.Model.Audience> audiences = new List<Fanatics.Email.Model.Audience>();
            Controller.DataExtension deController = new DataExtension();
            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "BusinessUnit";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { buName};
            Model.DataExtension audienceDE = deController.GetByExternalKeyMID(audienceDEKey, false, mid, sfp);

            foreach (var row in audienceDE.Rows)
            {
                Fanatics.Email.Model.Audience audience = new Fanatics.Email.Model.Audience();

                object AudienceType, AudienceName, AudienceValue, FolderID,AudienceGroup;

                if (row.TryGetValue("AudienceType", out AudienceType))
                    audience.AudienceType = AudienceType.ToString();

                if (row.TryGetValue("FolderID", out FolderID))
                    audience.FolderID = FolderID == "" ? 0 : (int)FolderID;

                if (row.TryGetValue("AudienceName", out AudienceName))
                    audience.Name = AudienceName.ToString();

                if (row.TryGetValue("AudienceValue", out AudienceValue))
                    audience.Value = AudienceValue.ToString();

                if (row.TryGetValue("AudienceGroup", out AudienceGroup))
                    audience.AudienceGroup = AudienceGroup.ToString();

                // Add DE Names if Audience type is "DEAudience"
                if (FolderID != "" && (AudienceType.ToString().ToLower().Equals("deaudience")|| AudienceType.ToString().ToLower().Equals("supression")))
                    audiences.AddRange(GetAudiencesByFolderIDAudienceType((int)FolderID, audience.AudienceType,isNewBU,audience.AudienceGroup));
                else
                    audiences.Add(audience);
            }

            return audiences;
        }

        private List<Model.Audience> GetAudiencesByFolderIDAudienceType(int folderID,string audienceType, bool isNewBU,string audienceGroup)
        {
            List<Fanatics.Email.Model.Audience> audiences = new List<Fanatics.Email.Model.Audience>();

            Controller.DataExtension deController = new DataExtension();
            var DEs = deController.GetByCategoryID(folderID, isNewBU);

            foreach (var name in DEs)
            {
                Fanatics.Email.Model.Audience audience = new Fanatics.Email.Model.Audience()
                {
                    Name = name.Name,
                    Value = name.CustomerKey,
                    AudienceType = audienceType,
                    AudienceGroup = audienceGroup
                };
                audiences.Add(audience);
            }

            return audiences;
        }
    }
}
