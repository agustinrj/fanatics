﻿using Fanatics.Email.Controller.ETClient;
using Fanatics.Email.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fanatics.Email.Controller
{
    public class EmailSendDefinition
    {
        public SoapClient GetClient(bool isNewBU)
        {
            ETClient.SoapClient etClient = new ETClient.SoapClient();
            if (isNewBU)
            {
                etClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NewETUserName"];
                etClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NewETPassword"];
            }
            else
            {
                etClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["ETUserName"];
                etClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["ETPassword"];
            }
            etClient.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings["ETEndPoint"]);

            return etClient;
        }

        public List<Model.EmailSendDefinition> GetNamesByMidFolderId(int mid,int folderId, bool isNewBU)
        {
            List<Model.EmailSendDefinition> emails = new List<Model.EmailSendDefinition>();

            ETClient.SoapClient etClient = GetClient(isNewBU);

            string requestID = "";
            string status = "";
            APIObject[] results;

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "EmailSendDefinition";

            rr.Properties =
                new string[]
                        {
                           "Name",
                           "CustomerKey"
                        };

            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "CategoryID";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { folderId.ToString() };

            rr.Filter = sfp;

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            status = etClient.Retrieve(rr, out requestID, out results);

            if (status.ToLower().Equals("ok"))
            {
                foreach (ETClient.EmailSendDefinition e in results)
                    emails.Add(new Model.EmailSendDefinition() { Name = e.Name,CustomerKey = e.CustomerKey});
            }

            return emails;
        }

        public ETClient.EmailSendDefinition GetByCustomerKey(int mid, string customerKey, bool isNewBU)
        {
            ETClient.EmailSendDefinition esd = null;
            ETClient.SoapClient etClient = GetClient(isNewBU);

            string requestID = "";
            string status = "";
            APIObject[] results;

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "EmailSendDefinition";

            rr.Properties =
                new string[]
                        {
                           "ObjectID",
                           "CustomerKey"
                        };

            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "CustomerKey";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { customerKey };

            rr.Filter = sfp;

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            status = etClient.Retrieve(rr, out requestID, out results);

            if (status.ToLower().Equals("ok"))
            {
                esd = (ETClient.EmailSendDefinition)results[0];
                esd.Client = clientIDs[0];
            }

            return esd;
        }

        public Model.EmailSendDefinition GetByMidCustomerKey(int mid,string customerKey, bool isNewBU)
        {
            Model.EmailSendDefinition email = null;

            ETClient.SoapClient etClient = GetClient(isNewBU);

            string requestID = "";
            string status = "";
            APIObject[] results;

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "EmailSendDefinition";

            rr.Properties =
                new string[]
                        {
                           "Name",
                           "Email.ID",
                           "SendDefinitionList",
                           "EmailSubject",
                           "DeduplicateByEmail"
                        };

            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "CustomerKey";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { customerKey };

            rr.Filter = sfp;

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified= true;
            rr.ClientIDs = clientIDs;

            status = etClient.Retrieve(rr, out requestID, out results);

            if (status.ToLower().Equals("ok"))
            {
                if (results.Count() == 0)
                    return null;

                ETClient.EmailSendDefinition e = (ETClient.EmailSendDefinition)results[0];
                email = new Model.EmailSendDefinition();
                email.Name = e.Name;
                email.SubjectLine = e.EmailSubject;
                email.CustomerKey = customerKey;
                email.DeduplicateByEmail = e.DeduplicateByEmail;

                // Get Email Name
                rr.ObjectType = "Email";
                rr.QueryAllAccounts = true;
                rr.QueryAllAccountsSpecified = true;

                rr.Properties =
                    new string[]
                    {
                        "Name"
                    };

                sfp = new SimpleFilterPart();
                sfp.Property = "ID";
                sfp.SimpleOperator = SimpleOperators.equals;
                sfp.Value = new string[] { e.Email.ID.ToString() };

                rr.Filter = sfp;

                status = etClient.Retrieve(rr, out requestID, out results);
                if (status.ToLower().Equals("ok") && results.Count() > 0)
                    email.EmailName = ((Fanatics.Email.Controller.ETClient.Email)results[0]).Name;

                // Get Data Extension Name
                if (e.SendDefinitionList.Count() > 0)
                {
                    foreach (var list in e.SendDefinitionList)
                    {
                        if (list.DataSourceTypeID == DataSourceTypeEnum.CustomObject)
                        {
                            rr.ObjectType = "DataExtension";
                            rr.QueryAllAccounts = true;
                            rr.QueryAllAccountsSpecified = true;

                            rr.Properties =
                                new string[]
                                {
                                   "Name","ObjectID"
                                };

                            sfp = new SimpleFilterPart();
                            sfp.Property = "ObjectID";
                            sfp.SimpleOperator = SimpleOperators.equals;
                            sfp.Value = new string[] { list.CustomObjectID };

                            rr.Filter = sfp;

                            status = etClient.Retrieve(rr, out requestID, out results);
                            if (status.ToLower().Equals("ok") && results.Count() > 0)
                            {
                                if (list.SendDefinitionListType == SendDefinitionListTypeEnum.ExclusionList)
                                    email.ExclusionAudience.Add(new Model.EmailSendDefinitionList() { Name = ((ETClient.DataExtension)results[0]).Name });
                                else
                                {
                                    if (!list.IsTestObject)
                                        email.SendAudience.Add(new Model.EmailSendDefinitionList() { Name = ((ETClient.DataExtension)results[0]).Name });
                                }
                            }
                        }
                    }
                }

                // get the status of the send
                Send sendController = new Send();
                email.Send = sendController.GetPendingByMidCustomerKey(mid, customerKey,isNewBU);
            }

            return email;
        }

        /// <summary>
        /// Creates one or multiple UI Sends based on a request object.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public dynamic CreateFromCampaignRequest(EmailSendCreateRequest request)
        {
            Controller.Campaign campaignController = new Campaign();

            ETClient.SoapClient etClient = new ETClient.SoapClient();

            // TODO: remove usage of new ET UserName this once everything is in new environment
            etClient.ClientCredentials.UserName.UserName = request.IsNewBU ? ConfigurationManager.AppSettings["NewETUserName"] : ConfigurationManager.AppSettings["ETUserName"];
            etClient.ClientCredentials.UserName.Password = request.IsNewBU ? ConfigurationManager.AppSettings["NewETPassword"] : ConfigurationManager.AppSettings["ETPassword"];
            etClient.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings["ETEndPoint"]);
            
            List<APIObject> objects = new List<APIObject>();
            List<APIObject> deObjects = new List<APIObject>();

            // if it's an overwrite first we need to remove the SFMC objects associated with the campaign
            if (request.Overwrite)
                campaignController.DeleteSFMCObjects(request.MID, request.Campaign.Id, "uisend",request.IsNewBU);

            // if it was requested to create one UI per audiece we will name each UI following this naming convention:
            // CNUMBER_CNAME_AUDIENCENAME (excluding _Email from audience name)
            // exclusions will remain the same
            if (request.CreateUIPerAudience)
            {
                ETClient.EmailSendDefinition firstESD = null;

                var dataExtensions = request.Campaign.Audiences.Where(a => a.AudienceType.ToLower().Equals("deaudience")).ToList();
                foreach (var audience in dataExtensions)
                {
                    string esdName = string.Format("{0}_{1}_{2}", request.Campaign.CNumber, request.Campaign.CName, audience.Name.Replace("_EMAIL", ""));

                    if (objects.Count() == 0)
                    {
                        firstESD = GetEmailSendDefinitionObject(request, esdName, audience.Value, null, null, 0);
                        objects.Add(firstESD);
                        deObjects.Add(campaignController.GetSFMCObjectRow(request.Campaign.Id, firstESD.CustomerKey, "uisend"));
                    }
                    else
                    {
                        var esd = GetEmailSendDefinitionObject(request, esdName, audience.Value, firstESD.Email, firstESD.SendClassification, firstESD.CategoryID);
                        objects.Add(esd);
                        deObjects.Add(campaignController.GetSFMCObjectRow(request.Campaign.Id, esd.CustomerKey, "uisend"));
                    }
                }
            }
            else
            {
                var esd = GetEmailSendDefinitionObject(request, request.Campaign.EmailName, null, null, null, 0);
                objects.Add(esd);
                deObjects.Add(campaignController.GetSFMCObjectRow(request.Campaign.Id, esd.CustomerKey, "uisend"));
            }

            string cRequestID = "";
            string cStatus = "";

            CreateResult[] cResults = etClient.Create(new CreateOptions(), objects.ToArray(), out cRequestID, out cStatus);
            
            if (cStatus.ToLower() != "ok")
                throw new Exception(cResults[0].StatusMessage);

            UpdateOptions updateOptions = new UpdateOptions();
            SaveOption saveOption = new SaveOption();

            saveOption.SaveAction = SaveAction.UpdateAdd;
            saveOption.PropertyName = "DataExtensionObject";
            updateOptions.SaveOptions = new SaveOption[] { saveOption };

            int mid = 0;
            int.TryParse(ConfigurationManager.AppSettings["ETClient"], out mid);

            ClientID clientID = new ClientID();
            clientID.ID = mid;
            clientID.IDSpecified = true;
            updateOptions.Client = clientID;

            // TODO: remove this once everything is in new environment
            ETClient.SoapClient etClientSource = new ETClient.SoapClient();
            etClientSource.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["ETUserName"];
            etClientSource.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["ETPassword"];
            UpdateResult[] uResults = etClientSource.Update(updateOptions, deObjects.ToArray(), out cRequestID, out cStatus);

            if (cStatus.ToLower() != "ok")
                throw new Exception(uResults[0].StatusMessage);

            return new { Status = "OK" };
        }

        private ETClient.EmailSendDefinition GetEmailSendDefinitionObject(EmailSendCreateRequest request, string name, string specificAudience, ETClient.Email emailObject, SendClassification sendClassification, int categoryID)
        {
            Controller.Email emailController = new Email();
            Controller.DataExtension deController = new DataExtension();

            ETClient.EmailSendDefinition esd = new ETClient.EmailSendDefinition();
            esd.Name = name;
            esd.CustomerKey = Guid.NewGuid().ToString();
            esd.Description = request.Campaign.EmailName + " created with the Workflow Tool";

            // Get or create the email object
            int emailID = 0;
            if (emailObject == null)
            {
                var email = emailController.GetEmailByName(request.Campaign.EmailName,request.MID,request.IsNewBU);

                if(email == null)
                    throw new Exception(string.Format("The email {0} was not found, please create this Email to continue", request.Campaign.EmailName));

                emailID = email.ID;
            }
            else
                emailID = emailObject.ID;

            ETClient.Email em = new ETClient.Email();
            em.ID = emailID;
            em.IDSpecified = true;

            esd.Email = em;
            esd.EmailSubject = request.Campaign.SubjectLine;
            esd.PreHeader = request.Campaign.Preheader;

            // Set default commercial send classification. 
            if (sendClassification == null)
                esd.SendClassification = GetSendClassification(request.MID,request.IsNewBU);
            else
                esd.SendClassification = sendClassification;

            esd.DeduplicateByEmail = true;
            esd.DeduplicateByEmailSpecified = true;

            // Audiences
            List<SendDefinitionList> sendDefinitionList = new List<SendDefinitionList>();

            List<Model.Audience> dataExtensions = new List<Model.Audience>();

            if (string.IsNullOrEmpty(specificAudience))
                dataExtensions = request.Campaign.Audiences.Where(a => a.AudienceType.ToLower().Equals("deaudience")).ToList();
            else
                dataExtensions = request.Campaign.Audiences.Where(a => a.AudienceType.ToLower().Equals("deaudience") && a.Value.Equals(specificAudience)).ToList();

            foreach (var audience in dataExtensions)
            {
                Model.DataExtension de = deController.GetByExternalKey(audience.Value, request.IsNewBU);
                if (de != null)
                {
                    SendDefinitionList sdl = new SendDefinitionList();
                    sdl.Name = de.ExternalKey;
                    sdl.CustomerKey = de.ExternalKey;
                    sdl.SendDefinitionListType = SendDefinitionListTypeEnum.SourceList;
                    sdl.SendDefinitionListTypeSpecified = true;
                    sdl.DataSourceTypeID = DataSourceTypeEnum.CustomObject;
                    sdl.DataSourceTypeIDSpecified = true;
                    sdl.CustomObjectID = de.ObjectID;
                    sendDefinitionList.Add(sdl);
                }
            }

            // Supressions
            var supressions = request.Campaign.Audiences.Where(a => a.AudienceType.ToLower().Equals("supression"));

            foreach (var supression in supressions)
            {
                Model.DataExtension de = deController.GetByExternalKey(supression.Value, request.IsNewBU);
                if (de != null)
                {
                    SendDefinitionList sdl = new SendDefinitionList();
                    sdl.Name = de.ExternalKey;
                    sdl.CustomerKey = de.ExternalKey;
                    sdl.SendDefinitionListType = SendDefinitionListTypeEnum.ExclusionList;
                    sdl.SendDefinitionListTypeSpecified = true;
                    sdl.DataSourceTypeID = DataSourceTypeEnum.CustomObject;
                    sdl.DataSourceTypeIDSpecified = true;
                    sdl.CustomObjectID = de.ObjectID;
                    sendDefinitionList.Add(sdl);
                }
            }

            esd.SendDefinitionList = sendDefinitionList.ToArray();

            // folder
            if (categoryID == 0)
                esd.CategoryID = GetEmailSendFolderID(request);
            else
                esd.CategoryID = categoryID;

            esd.CategoryIDSpecified = true;
            
            esd.Client = new ClientID();
            esd.Client.ID = request.MID;
            esd.Client.IDSpecified = true;

            return esd;
        }

        private SendClassification GetSendClassification(int mid,bool isNewBU)
        {
            SendClassification sc = null;

            ETClient.SoapClient etClient = new ETClient.SoapClient();
            etClient.ClientCredentials.UserName.UserName = isNewBU ? ConfigurationManager.AppSettings["NewETUserName"] : ConfigurationManager.AppSettings["ETUserName"];
            etClient.ClientCredentials.UserName.Password = isNewBU ? ConfigurationManager.AppSettings["NewETPassword"] : ConfigurationManager.AppSettings["ETPassword"];
            etClient.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings["ETEndPoint"]);

            string requestID = "";
            string status = "";
            APIObject[] results;

            RetrieveRequest rr = new RetrieveRequest();
            rr.ObjectType = "SendClassification";

            rr.Properties =
                new string[]
                        {
                           "ObjectID",
                           "CustomerKey"
                        };

            SimpleFilterPart sfp = new SimpleFilterPart();
            sfp.Property = "CustomerKey";
            sfp.SimpleOperator = SimpleOperators.equals;
            sfp.Value = new string[] { "Default Commercial" };

            rr.Filter = sfp;

            ClientID[] clientIDs = new ClientID[1];
            clientIDs[0] = new ClientID();
            clientIDs[0].ID = mid;
            clientIDs[0].IDSpecified = true;
            rr.ClientIDs = clientIDs;

            status = etClient.Retrieve(rr, out requestID, out results);

            if (status.ToLower().Equals("ok"))
            {
                if (results.Count() > 0)
                    sc = (SendClassification)results[0];
            }
            else
                throw new Exception("Default Commercial Send Classification not found");
                
            return sc;
        }

        private int GetEmailSendFolderID(EmailSendCreateRequest request)
        {
            Controller.Folder folderController = new Folder();

            var folderType = "userinitiatedsends";

            // folder is Year - Month - day of deployment date
            var year = request.Campaign.DeploymentDate.Year.ToString();
            var month = request.Campaign.DeploymentDate.Month.ToString();
            var day = request.Campaign.DeploymentDate.Day.ToString();

            var yearFolderID = 0;

            if(request.CreateQuickDeploy)
                yearFolderID = folderController.GetCreateFolderID(request.MID, year, folderType, request.QuickDeployFolderId, request.IsNewBU);
            else
                yearFolderID = folderController.GetCreateFolderID(request.MID, year, folderType, request.UISendsFolderId, request.IsNewBU);

            var monthFolderID = folderController.GetCreateFolderID(request.MID, month, folderType, yearFolderID, request.IsNewBU);
            var dayFolderID = folderController.GetCreateFolderID(request.MID, day, folderType, monthFolderID, request.IsNewBU);

            return dayFolderID;
        }
    }
}
